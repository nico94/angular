import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ReporteContrato';
  data: any[] = [{
    DiaReneg: null,
    MesRenegLetra: null,
    AnoReneg: null,
    Nombre: null,
    ApPaterno: null,
    ApMaterno: null,
    Rut: null,
    Dv: null,
    EstCivil: null,
    Profesion: null,
    DiaVenta: null,
    MesVentaLetra: null,
    AnoVenta: null,
    ValTotUfNum: null,
    ValTotUfLetra: null,
    ValTotPesosNum: null,
    ValTotPesosLetra: null,
    ValContadoPesosNum: null,
    ValContadoPesosLetra: null,
    NumRecibo: null,
    SldoPesosNum: null,
    SldoPesosLetra:null,
    CuotasPagNvo:null,
    ValCuotaPagNvo:null,
    DiaPagNvo:null,
    MesPagNvoLetra:null,
    AnoPagNvo:null,
  }];

  constructor(
    private activatedRoute: ActivatedRoute
  ){
    this.activatedRoute.queryParams.subscribe(data =>{
      this.data[0].DiaReneg = data['DiaReneg'];
      this.data[0].MesRenegLetra = data['MesRenegLetra'];
      this.data[0].AnoReneg = data['AnoReneg'];
      this.data[0].Nombre = data['Nombre'];
      this.data[0].ApPaterno = data['ApPaterno'];
      this.data[0].ApMaterno = data['ApMaterno'];
      this.data[0].Rut = data['Rut'];
      this.data[0].EstCivil = data['EstCivil'];
      this.data[0].Profesion = data['Profesion'];
      this.data[0].DiaVenta = data['DiaVenta'];
      this.data[0].MesVentaLetra = data['MesVentaLetra'];
      this.data[0].AnoVenta = data['AnoVenta'];
      this.data[0].ValTotUfNum = data['ValTotUfNum'];
      this.data[0].ValTotUfLetra = data['ValTotUfLetra'];
      this.data[0].ValTotPesosNum = data['ValTotPesosNum'];
      this.data[0].ValTotPesosLetra = data['ValTotPesosLetra'];
      this.data[0].ValContadoPesosNum = data['ValContadoPesosNum'];
      this.data[0].ValContadoPesosLetra = data['ValContadoPesosLetra'];
      this.data[0].NumRecibo = data['NumRecibo'];
      this.data[0].SldoPesosNum = data['SldoPesosNum'];
      this.data[0].SldoPesosLetr = data['SldoPesosLetr'];
      this.data[0].CuotasPagNv = data['CuotasPagNv'];
      this.data[0].ValCuotaPagNv = data['ValCuotaPagNv'];
      this.data[0].DiaPagNv = data['DiaPagNv'];
      this.data[0].MesPagNvoLetr = data['MesPagNvoLetr'];
      this.data[0].AnoPagNv = data['AnoPagNv'];
    });
  }

  ngOnInit(){
    window.print();
  }
}