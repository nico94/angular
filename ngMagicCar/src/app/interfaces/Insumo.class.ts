export class Insumo {
    constructor(public id: number, public nombre: string) { }
}

export interface IInsumo {
    total: number;
    resultados: IInsumo[];
}