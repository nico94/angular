import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';

// Servicios
import { RecepcionvehiculosService } from '../../../service/recepcionvehiculos.service';

// Interface
import { IListaMotivo } from '../../../interfaces/ListaMotivo';

// Matrial Angular
import { MatDialog, MatSnackBar } from '@angular/material';

// Componentes
import { ModalRecepcionvehiculoComponent } from '../../modals/modal-recepcionvehiculo/modal-recepcionvehiculo.component';
import { ModalInfoadicionalComponent } from '../../modals/modal-infoadicional/modal-infoadicional.component';
import { Router } from '@angular/router';
import { ModalDocvehiculoComponent } from '../../modals/modal-docvehiculo/modal-docvehiculo.component';
import { min } from 'rxjs/operators';
import { ModalAsignarpatenteComponent } from '../../modals/modal-asignarpatente/modal-asignarpatente.component';

@Component({
  selector: 'app-crear-recepcio',
  templateUrl: './crear-recepcio.component.html',
  styleUrls: ['./crear-recepcio.component.css']
})
export class CrearRecepcioComponent implements OnInit, OnDestroy {

  private _Subscription: Subscription;
  

  showOtro = false;
  selectedValue: string;

  MotivoForm = new FormControl();
  
  _NUMEROS: FormGroup;
  PATENTE_: FormGroup;

  ListMotivo: IListaMotivo[] = [];

  componentehijo="componente hijo";

  validaciones:any;
  // Variables
  _patenteNoValida = true;
  @Input() _patente: string;
  _motivos;
  _listaPatenteDatos: any[] = [];
  _prioridad: string;
  _listaKilometrajes: any[] = [];
  _seleccion_kilometraje = '';
  _kilometraje: number;
  _otro = '';
  _nivel_combustible = 1;
  _prof_del_der = 0;
  _prof_del_izq = 0;
  _prof_tras_der = 0;
  _listadoDocVehi: any[] = [];
  _prof_tras_izq = 0;
  _deshabilitar_km = false;
  _habilitar_botones = false; // habilita botones de registro de estado y documento
  _estado_registrado = false; // estado cuando el registro de estado fue realizado
  _documento_registrado = false; // estado cuando el registro de documentos fue realizado
  _id_rec_vehiculo;
  _valorDP: any = '';
  datos: any;
  btnAsignarpatente = true;

  public form: FormGroup;

  getErrorMessage() {
    return this.validaciones.hasError('required') ? 'Ingresar Kilometraje' :


      this.validaciones.hasError('validaciones') ? 'Dato no valido' :
        '';
  }
  // Dialog Variables
  title: string;
  name: string;
  picker: any = '';
  date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
  _fecha_recepcion = this.date.value
  constructor(private _router: Router, private wsRecepcion: RecepcionvehiculosService, private dialog: MatDialog, private snackBar: MatSnackBar, private formBuilder: FormBuilder) {

    this.validaciones = new FormControl(['', Validators.required, Validators.minLength(3)]);

    this.PATENTE_ = formBuilder.group({
      patente_: ['', Validators.required],
    });
    this._NUMEROS = formBuilder.group({

      numero1: [1, Validators.min(1)],
      numero2: [1, Validators.min(1)],
      numero3: [1, Validators.min(1)],
      numero4: [1, Validators.min(1)],
    });
    console.log(this._NUMEROS);

    
  }

  ngOnInit() {
    this.listadoMotivo();
    this.form = this.formBuilder.group({
      numero1: 0
    });
    // this.cargarlistadoDocVehiculo();
  }

  ngOnDestroy() {
    // this._Subscription.unsubscribe();
  }

  changePatente() {
    if (this._patente.length < 6) {
      this._patenteNoValida = true;
      this.btnAsignarpatente = true      
    } else {
      this._patenteNoValida = false;
      this.btnAsignarpatente = true
    }
    if (this._patente.length === 6) {
      this.verificarPatente();
      
    }
  }

  agregarPatente(){
    this.openDialogcrear();
  }

  //  ACA COMIENZA EL RULO
  // treamos la ultima recepcion del vehiculo
  verificarPatente() {
    this._Subscription = this.wsRecepcion.consultarPatente(this._patente)
      .subscribe((data: any) => {        
        console.log(data);
        if (data.ListadoClientePatenteResult.lista.length === 0) {

          if (data.ListadoClientePatenteResult.newList.length === 0) {
            // this.snackBar.open('Patente no registrada', null, { duration: 3000 });
            this._patenteNoValida = true;
            this.btnAsignarpatente = false;
            
            return;
          } else {
            console.log('new list');
            this._listaPatenteDatos = data['ListadoClientePatenteResult']['newList'];
            this.listarKilmetraje();
            this._patenteNoValida = false;

          }
        } else {
          console.log('list');
          this._listaPatenteDatos = data['ListadoClientePatenteResult']['lista'];
          this.listarKilmetraje();
        }

      }, (error) => {
      });
  }

  // Lista los motivos para la recepcion
  listadoMotivo() {
    this._Subscription = this.wsRecepcion.getListadoMotivo()
      .subscribe((data: any) => {
        data['ListadoMotivoResult']['lista'].forEach(element => {
          this.ListMotivo.push({
            value: element.id_tipoactividad,
            descripcion: element.des_TipoAct
          });
        });
      }, (error) => {
      });
  }

  // lista kilometrajes que le corresponden al vehiculo
  listarKilmetraje() {

    this._Subscription = this.wsRecepcion.ListarKilometraje(this._listaPatenteDatos)
      .subscribe((data: any) => {
        data.ListadoTareaKilometrajeResult.lista.forEach(element => {
          this._listaKilometrajes.push(element.num_kmMayor_snapTareaActVehi);
        });

      }, (error) => {
      });

  }

  // Validacion campos completos
  validar() {
    //  console.log( this._patente , this._motivos.length < 0 , this._prioridad , this._fecha_recepcion , this._kilometraje ,
    //     this._prof_del_der > 0 , this._prof_del_izq > 0 , this._prof_tras_der > 0 , this._prof_tras_izq > 0);

    if (this._patente ||
      this._motivos.length > 0 ||
      this._prioridad ||
      this._fecha_recepcion ||
      this._kilometraje > 0
    ) {
      return false;
    }
    return true;
  }

  // si el motivo es PREVENTIVO(id 14) habilitamos la opcion de kilometrajes
  select_motivos(value) {
    console.log(this._motivos);
    this._valorDP = value.indexOf(15);

    if (value.indexOf(14) >= 0) {
      this._deshabilitar_km = false;
    } else {
      this._deshabilitar_km = true;
      this._seleccion_kilometraje = '';
    }
    if (this._valorDP >= 0) {
      this._valorDP = true;
      console.log(this._valorDP);
      localStorage.setItem("D&P", this._valorDP);
    } else {
      localStorage.removeItem("D&P");
    }
  }


  // continuamos a la siguiente pagina solo si se engresaron la documentacion y el estado
  continuar() {
    if (!this._habilitar_botones) {
      this.snackBar.open('Falta guardar la recepción', null, { duration: 3000 });
    }

    if (!this._estado_registrado) {
      this.snackBar.open('Debe ingresar estado de vehículo', null, { duration: 3000 });
      return;
    }

    if (!this._documento_registrado) {
      this.snackBar.open('Debe ingresar documentos del vehículo', null, { duration: 3000 });
      return;
    }

    this._router.navigate(['/recepcion/img_recepcion', this._patente]);

  }
  // this._NUMEROS.invalid === true ||

  // se guarda la recepcion y el kilometraje seleccionado de la lista
  // pero no se guarda el registro del estado del vehiculo ni tampoco la documentacion
  guardarEstadoActual() {
    if (this._kilometraje == 0  && this.PATENTE_.invalid === true) {
      this.snackBar.open('No se pueden guardar los datos', null, { duration: 3000 });
      return false;
      
    } else{
      const data = {

        dat_fecharecepcion_encrecvehiculo: this._fecha_recepcion,
        num_prioridad_encrecvehiculo: parseInt(this._prioridad, 10),
        num_kilometraje_encrecvehiculo: (this._kilometraje, 10),
        num_kmant_encrecvehiculo: this._seleccion_kilometraje,
        num_nivelcombustible_encrecvehiculo: this._nivel_combustible,
        num_profneumDder_encrecvehiculo: this._prof_del_der,
        num_profneumDizq_encrecvehiculo: this._prof_del_izq.toString(),
        num_profneumTder_encrecvehiculo: this._prof_tras_der.toString(),
        num_profneumTizq_encrecvehiculo: this._prof_tras_izq.toString(),
        des_observacion_encrevehiculo: '',
        id_Patente_snapClienteVehiculo: this._patente,
        cod_cliente_cliente: this._listaPatenteDatos[0].cod_cliente_cliente,
        cod_fichapersonal_fichapersonal: 47,
        listaMotivos: this.MotivoForm.value,
        bit_espropietario_encrecvehiculo: this.showOtro,
        des_chofer_encrecvehiculo: this._otro
      };
      let obj_motivo = [];

      this.MotivoForm.value.forEach(element => {
        obj_motivo.push({ id_tipoactividad: element });
      });
      // cambiamos el formato de la fecha para que el backend las lea
      // const nueva_fecha = this.correccion_fecha();
      // data.dat_fecharecepcion_encrecvehiculo = nueva_fecha;
      data.listaMotivos = obj_motivo;


      if (this._deshabilitar_km) {
        data.num_kmant_encrecvehiculo = '0';
      }
      this.wsRecepcion.GuardarRecepcionVehiculo(data).subscribe((result: any) => {
        console.log('guardar data completa', result);
        if (!result.GuardarRecepcionVehiculoResult.realizado) {
          this.snackBar.open('Error al guardar la información', null, { duration: 3000 });
          return;
        }
        this._habilitar_botones = true;
        this.rescatarid_recvehiculo();

      }, error => console.log(error));
    }
    // if (!this.validar()) {
    //   alert('Llene todos los datos antes de guardar');
    //   return;
    // }
    console.log(this._motivos);



  }

  rescatarid_recvehiculo() {
    this.wsRecepcion.RescatarIdRecVeiculo(this._patente).subscribe((result: any) => {
      console.log('respuesta kilometraje', result);
      if (!result.RetornarIDResult.realizado) {
        this.snackBar.open('Error al rescatar datos del vehículo', null, { duration: 3000 });
        return;
      }
      this._id_rec_vehiculo = result.RetornarIDResult.lista[0].id_codigo_recvehiculo;
      this._listaPatenteDatos[0].id_codigo_recvehiculo = this._id_rec_vehiculo;

      if (!this._deshabilitar_km) {
        console.log('enviar kilimetraje');
        this.GuardarKilometraje();
      }
    }, error => console.log(error));
  }

  GuardarKilometraje() {
    console.log(this._seleccion_kilometraje);
    this._listaPatenteDatos[0].id_codigo_recvehiculo = this._id_rec_vehiculo;
    this.wsRecepcion.GuardarKilometraje(this._listaPatenteDatos[0], this._seleccion_kilometraje).subscribe((result: any) => {
      console.log('respuesta kilometraje', result);
      if (!result.GuardarTareasMantenimientosProgramadasResult.resultado) {
        this.snackBar.open('Error al guardar kilometraje', null, { duration: 3000 });
        return;
      }
      this._habilitar_botones = true;
    }, error => console.log(error));
  }

  correccion_fecha() {

    const fecha = new Date(this._fecha_recepcion).toLocaleDateString().split('-');
    let dia = fecha[0];
    let mes = fecha[1];
    let year = fecha[2];
    if (parseInt(fecha[0], 10) < 10) {
      dia = '0' + fecha[0];
    }

    if (parseInt(fecha[1], 10) < 10) {
      mes = '0' + fecha[1];
    }


    return mes + '/' + dia + '/' + year;

  }

  openDialogEstado() {

    const dialogRef = this.dialog.open(ModalRecepcionvehiculoComponent, {
      disableClose: true,
      width: '1000px',
      height: 'auto',
      data: { data: this._listaPatenteDatos }
    });

    dialogRef.afterClosed().subscribe(result => {
      
      console.log(result);
      if (result.result) {
        this._estado_registrado = true;
      }
    });
  }

  openDialogcrear() {
    const dialogRef = this.dialog.open(ModalAsignarpatenteComponent, {
      disableClose: true,
      width: '1000px',
      height: 'auto',
      data: { patente: this._patente }
    });
    dialogRef.afterClosed().subscribe((data) =>{
      console.log(data['patente']);      
      this._patente = data['patente'];
      if(this._patente){
        this.verificarPatente();
        this.btnAsignarpatente = true;
      }else{
        this.snackBar.open('Error al cargar patente', null, { duration: 3000 });
      }
    });

  }

  openDialogInfo() {
    const dialogRef = this.dialog.open(ModalInfoadicionalComponent, {
      disableClose: true,
      width: '1000px',
      height: 'auto',
      data: { data: this._listaPatenteDatos, patente: this._patente }
    });
  }

  openDialogDoc() {
    const dialog = this.dialog.open(ModalDocvehiculoComponent, {
      disableClose: true,
      width: '1000px',
      height: 'auto',
      data: { data: this._listaPatenteDatos, patente: this._patente }



    });

    dialog.afterClosed().subscribe(result => {
      console.log(result);
      if (result.result) {
        this._documento_registrado = true;
      }
    });
  }



}

