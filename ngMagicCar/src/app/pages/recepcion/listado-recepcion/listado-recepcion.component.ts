import { Component, OnInit , OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { MatDialog } from '@angular/material';

import { Subscription } from 'rxjs';
import { RecepcionvehiculosService } from '../../../service/recepcionvehiculos.service';

import { LoginService } from '../../../service/login.service';
import { ModalCambiarestadoComponent } from '../../modals/modal-cambiarestado/modal-cambiarestado.component';


@Component({
  selector: 'app-listado-recepcion',
  templateUrl: './listado-recepcion.component.html',
  styleUrls: ['./listado-recepcion.component.css']
})
export class ListadoRecepcionComponent implements OnInit, OnDestroy {

  private key: string;
  private _Subscription: Subscription;
  DataSource: any[] = [];
  public cargando = true;
  meses  = [
    { mes: 'Enero', id: 1, },
    { mes: 'Febrero', id: 2},
    { mes: 'Marzo', id: 3, },
    { mes: 'Abril', id: 4, },
    { mes: 'Mayo', id: 5, },
    { mes: 'Junio', id: 6, },
    { mes: 'Julio', id: 7},
    {mes: 'Agosto', id: 8, },
    {mes: 'Septiembre', id: 9 },
    {mes: 'Octubre', id: 10 },
    {mes: 'Noviembre', id: 11 },
    {mes: 'Diciembre', id: 12 }
  ];
  m = 1;
  years = [ 2019, 2020, 2021, 2022 ];
  y = 2019;
  listaRecepciones = [];

  constructor(private _router: Router, private wsLogin: LoginService,
              private wsRecepcion: RecepcionvehiculosService,
              public dialog: MatDialog) {
    this.key = localStorage.getItem('Session');
    const fecha = new Date();
    this.m = fecha.getMonth() + 1;
    this.y = fecha.getFullYear();
    this.cargarLista();
    if (this.key !== 'true') {
      this._router.navigate(['/Login']);
    }
  }

  ngOnInit() {

  }
  ngOnDestroy() {
    this._Subscription.unsubscribe();
  }

  buscar() {
    this.cargarLista();
  }


  ingresarRecepcion() {
    this._router.navigate(['/recepcion/crear_recepcion']);
  }

  editarEstado(item) {

    const position = this.DataSource.findIndex( (element: any) =>  element.id_codigo_recvehiculo === item.id_codigo_recvehiculo );
    const dialigCambiarEstado = this.dialog.open(ModalCambiarestadoComponent,
      {
        width: '500px',
        height: 'auto',
        data: { id : item.id_codigo_recvehiculo}
      });
      dialigCambiarEstado.afterClosed().subscribe(result => {
        console.log(result);
        if ( result.cerrar ) {
          return ;
        }
        if ( result.anulado ) {
          if (position >= 0) {
            this.DataSource[position]['des_descripcion_estadoordencompra'] = 'ANULADO';
          }
        }
      });
  }

  cargarLista() {
    this._Subscription = this.wsRecepcion.getListado( this.y.toString() , this.m.toString() )
      .subscribe((data: any) => {
        // console.log(JSON.stringify(data));
        console.log(data);

        this.DataSource = data['ListadoRecepcionResult']['lista'];
        this.listaRecepciones = data['ListadoRecepcionResult']['lista']
          this.cargando = false;
      }, (error) => {
          this.cargando = false;
      });
  }
}
