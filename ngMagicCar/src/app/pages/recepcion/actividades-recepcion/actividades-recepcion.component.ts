import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalEliminaractividadComponent } from '../../modals/modal-eliminaractividad/modal-eliminaractividad.component';
import { RecepcionvehiculosService } from '../../../service/recepcionvehiculos.service';
import { MatDialog } from '@angular/material';
import { ModalAgregaractividadComponent } from '../../modals/modal-agregaractividad/modal-agregaractividad.component';

@Component({
  selector: 'app-actividades-recepcion',
  templateUrl: './actividades-recepcion.component.html',
  styleUrls: ['./actividades-recepcion.component.css']
})
export class ActividadesRecepcionComponent implements OnInit {

  lista_actividades = [];
  _id_codigo_recvehiculo: number;
  _patente = '';
  _data_completa: any = [];
  suma_total: any = 0;
  constructor(private _router: Router,
     private dialog: MatDialog,
     private route: ActivatedRoute,
     private wsRecepcion: RecepcionvehiculosService) {

    this.route.paramMap.subscribe( ({params}: any) => {
      console.log(params.id);
      this._patente = params.id;
      this.verificarPatente();
    });


   }

  ngOnInit() {
  }

 verificarPatente() {
  this.wsRecepcion.consultarPatente(this._patente)
     .subscribe((data: any) => {       
      //  console.log("LA DATA ES LA SIGUIENTE :",data.ListadoClientePatenteResult.lista);
      //  console.log("LA DATA ES LA SIGUIENTE :",data.ListadoClientePatenteResult.newList[0]['id_codigo_recvehiculo']);
      
      //  console.log("LOS DATOS DE LA NEW LISTA SON :",data.ListadoClientePatenteResult.newList);
      

       if (data.ListadoClientePatenteResult.lista.length === 0) {

         if (data.ListadoClientePatenteResult.newList.length === 0 ) {
           alert('Patente no existe en los registros');
           return;
         } else {
          //  console.log('new list');
           this._data_completa = data['ListadoClientePatenteResult']['newList'];
           this.rescatarid_recvehiculo();
         }
       } else {
        // console.log('list');
        this._data_completa = data['ListadoClientePatenteResult']['lista'];
        this.rescatarid_recvehiculo();
       }

     }, (error) => {
   });
 }



  listarActividades() {

    this.wsRecepcion.ListarActividades(this._data_completa[0].id_codigo_recvehiculo).subscribe( (result) => {
      console.log(result);
      if (result.ListadoActividadAsignadaResult.MensajeError) {
        alert(result.ListadoActividadAsignadaResult.MensajeError);
        return;
      }
      this.lista_actividades = result.ListadoActividadAsignadaResult.lista;
      this.sumaTotales();
      }
    , error => console.log(error) );
  }

  finalizar() {
    this._router.navigate(['/recepcion/listado_recepcion']);
  }

  sumaTotales() {
    let suma = 0;
    this.lista_actividades.forEach( element => {
      suma += element.num_valor_snapTareaActVehi;
    });

    this.suma_total = suma;
    this.format();
  }

  format() {

    let num = this.suma_total.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
    num = num.split('').reverse().join('').replace(/^[\.]/, '');
    this.suma_total = num;

  }

  openDialogBorrar(index) {
   const actividad = this.lista_actividades[index];

    const dialogRef = this.dialog.open(ModalEliminaractividadComponent, {
      width: '500px',
      height: 'auto',
      data : { act : actividad }
    });

    dialogRef.afterClosed().subscribe( result => {

      if (result.result) {
        this.lista_actividades.splice(index, 1 );
        this.sumaTotales();
      }
    });


  }

  rescatarid_recvehiculo() {
    this.wsRecepcion.RescatarIdRecVeiculo(this._patente).subscribe( (result: any ) => {
      console.log("EL RESULTADO ES :",result);
      if (!result.RetornarIDResult.realizado) {
        alert('Error al recatar dato');
        return;
      }
      this._data_completa[0].id_codigo_recvehiculo = result.RetornarIDResult.lista[0].id_codigo_recvehiculo;
      this.listarActividades();
    }, error => console.log(error));
  }

  agregar_actividad() {
    const dialogo = this.dialog.open(ModalAgregaractividadComponent, {
      width: '500px',
      height: 'auto',
      data: { data : this._data_completa }
    });

    dialogo.afterClosed().subscribe( result => {
      console.log(result);
      if (result.result) {
        this.lista_actividades.push(result.data);
        console.log(this.lista_actividades);
        this.sumaTotales();
      }

    });
  }
}
