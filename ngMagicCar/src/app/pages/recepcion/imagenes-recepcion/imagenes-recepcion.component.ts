import { Component, OnInit, ElementRef, ViewChild, ɵConsole } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RecepcionvehiculosService } from '../../../service/recepcionvehiculos.service';
import { DragScrollComponent } from 'ngx-drag-scroll/lib';

@Component({
  selector: 'app-imagenes-recepcion',
  templateUrl: './imagenes-recepcion.component.html',
  styleUrls: ['./imagenes-recepcion.component.css']
})
export class ImagenesRecepcionComponent implements OnInit {

  @ViewChild("video")
  public video: ElementRef;

  @ViewChild("canvas")
  public canvas: ElementRef;

  @ViewChild('nav', { read: DragScrollComponent }) ds: DragScrollComponent;

  public captures: Array<any>;

  public contador = 0;

  observaciones: String = '';
  _id_codigo_vehiculo: any = [];
  _patente = '';
  _data_completa = [];
  _subiendo = false;
  constructor(private _router: Router, private router: ActivatedRoute, private wsRecepcion: RecepcionvehiculosService) {


    this.captures = [];
    this.router.paramMap.subscribe( ({params}: any) => {
      console.log(params.id);
      this._patente = params.id;     

      this.verificarPatente();

      
    });
   }


  ngOnInit() {  }

  moveLeft() {
    this.ds.moveLeft();
  }

  moveRight() {
    this.ds.moveRight();
  }

  public ngAfterViewInit() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
        this.video.nativeElement.srcObject = (stream);
        this.video.nativeElement.play();
      });
    }
  }


  verificarPatente() {
   this.wsRecepcion.consultarPatente(this._patente)
      .subscribe((data: any) => {
        console.log(data.ListadoClientePatenteResult.lista);
        console.log(data.ListadoClientePatenteResult.newList);
        if (data.ListadoClientePatenteResult.lista.length === 0) {

          if (data.ListadoClientePatenteResult.newList.length === 0 ) {
            alert('Patente no existe en los registros');
            return;
          } else {
            console.log('new list');
            this._id_codigo_vehiculo = data['ListadoClientePatenteResult']['newList'][0].id_codigo_recvehiculo;
            this._data_completa = data['ListadoClientePatenteResult']['newList'];
            this.rescatarid_recvehiculo();
          }
        } else {
          console.log('list');
          this._id_codigo_vehiculo = data['ListadoClientePatenteResult']['lista'][0].id_codigo_recvehiculo;
          this._data_completa = data['ListadoClientePatenteResult']['lista'];
          
          this.rescatarid_recvehiculo();
        }

      }, (error) => {
    });
  }

  rescatarid_recvehiculo() {
    this.wsRecepcion.RescatarIdRecVeiculo(this._patente).subscribe( (result: any ) => {
      if (!result.RetornarIDResult.realizado) {
        alert('Error al recatar dato');
        return;
      }
      this._id_codigo_vehiculo = result.RetornarIDResult.lista[0].id_codigo_recvehiculo;
      console.log("EL CODIGO DEL VEHICULO ES :",this._id_codigo_vehiculo);

    }, error => console.log(error));
  }

  eliminarImagen(idx) {
    this.captures.splice(idx, 1 );
  }
  /*reemplaza el video por la imagen tomada y lo agrega al arerglo*/
  public capture() {
    let context = this.canvas.nativeElement.getContext('2d').drawImage(this.video.nativeElement, 0, 0, 640, 480);
    this.video.nativeElement.style.display = 'none';
    this.canvas.nativeElement.style.display = 'block';
  }

  guardar() {
    if (this.canvas.nativeElement.style.display === 'none') {
      console.log('none video');
      return;
    }
    const info = {
      blob_fotografia_fotografia : this.canvas.nativeElement.toDataURL('image/png'),
      des_observacion_fotografia : this.observaciones,
      id_codigo_recvehiculo: this._id_codigo_vehiculo,
    };
    this.captures.push(info);
  }

  /*reemplaza la imagen del video por la imagen tomada en modal*/
  btn_subirFotografia() {
    this.video.nativeElement.style.display = 'block';
    this.canvas.nativeElement.style.display = 'none';
    this.observaciones = null;
    this.contador = 0;
  }

  onKey(event) {
    this.contador = event.target.value.length;
  }

  continuar() {
    this._subiendo = true;
    this.wsRecepcion.GuardarFotoVehiculo(this.captures).subscribe( result => {
      console.log(result);
      if (result.GuardarFotosVehiculoResult.realizado) {
        //ACA VAMOS A MANDAR EL PARAMETRO DEL ID RECEPCION
        this._id_codigo_vehiculo
        this._router.navigate(['/recepcion/actividades_recepcion', this._patente]);
        console.log("LOS DATOS QUE SE MANDAN :",this._router);
      } else {
        alert('Error');
        this._subiendo = false;
      }
    }, error => { console.log(error); });
  }
}
