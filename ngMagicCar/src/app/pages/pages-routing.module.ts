import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

// Componentes
import { CrearRecepcioComponent } from './recepcion/crear-recepcio/crear-recepcio.component';
import { CrearOtComponent } from './ordentrabajo/crear-ot/crear-ot.component';
import { ListadoOtComponent } from './ordentrabajo/listado-ot/listado-ot.component';
import { ListadoRecepcionComponent } from './recepcion/listado-recepcion/listado-recepcion.component';
import { ImagenesRecepcionComponent } from './recepcion/imagenes-recepcion/imagenes-recepcion.component';
import { ActividadesRecepcionComponent } from './recepcion/actividades-recepcion/actividades-recepcion.component';

const routes: Routes = [
    {
        path: 'crear_recepcion',
        component: CrearRecepcioComponent
    },
    {
        path: 'listado_recepcion',
        component: ListadoRecepcionComponent
    },
    {
        path: 'crear_ot',
        component: CrearOtComponent,
    },
    {
        path: 'listado_ot',
        component: ListadoOtComponent
    },
    {
        path: 'img_recepcion/:id',
        component: ImagenesRecepcionComponent
    },
    {
        path: 'actividades_recepcion/:id',
        component: ActividadesRecepcionComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PagesRoutingModule { }
