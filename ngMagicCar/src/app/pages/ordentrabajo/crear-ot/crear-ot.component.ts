import { Component, OnInit, OnDestroy } from '@angular/core';
import { OrdentrabajoService } from '../../../service/ordentrabajo.service';
import { MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { ModalResponsabletareaComponent } from '../../modals/modal-responsabletarea/modal-responsabletarea.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

export interface OrdenTrabajo {
  id_Patente_snapClienteVehiculo: string;
  Tipo: string;
  bit_activo_snapClienteVehiculo: string;
  cod_marca_marca: number;
  cod_modelo_modelo: number;
  des_marca_marca: string;
  des_modelo_modelo: string;
  id_codigo_recvehiculo: string;
}

export interface Actividad {
  Estado: string;
  bit_programada_detTareaManVehi: boolean;
  cod_OT_OTVehi: number;
  cod_fichapersonal_fichapersonal: number;
  cod_estado_ot: number;
  dat_fecInicio_DTMantVehiculo: string;
  dat_fecTerminoReal_DTMantVehiculo: string;
  dat_fecasignada_DTMantVehiculo: string;
  dat_fectermino_DTMantVehiculo: string;
  des_nombrecompleto: string;
  des_rut_fichapersonal: string;
  des_tarea: string;
  id_Patente_snapClienteVehiculo: string;
  id_codigo_recvehiculo: number;
  id_tarea: number;
  num_valor_snapTareaActVehi: number;
}

@Component({
  selector: 'app-crear-ot',
  templateUrl: './crear-ot.component.html',
  styleUrls: ['./crear-ot.component.css']
})

export class CrearOtComponent implements OnInit, OnDestroy {
  private actividades: any[] = [];
  private estadosOT: any[] = [];
  public encontrado: boolean;
  public mostrarActividades: boolean;
  private datosBusqueda: OrdenTrabajo[] = [];
  private dataSource = new MatTableDataSource(null);
  private dataSourceActividad = new MatTableDataSource(null);
  private displayedColumns: string[] = ['codigo', 'patente', 'tipo', 'modelo'];
  private displayedColumnsAct: string[] = ['descripcion', 'valor', 'responsable', 'estado', 'programada'];
  private fechaOT: string;
  public cargando: boolean = true;
  private patenteGlob: string;
  private cod_recepcionVehiculo: number;
  private cod_OT: number;
  public cod_estado: number;
  public esCreacion: boolean;
  private sub: Subscription;

  constructor(
    private dialog: MatDialog,
    private _ordenService: OrdentrabajoService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private _router: Router
  ) {
    localStorage.setItem('cod_usuario', '47');
    this.dataSourceActividad = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.esCreacion = true;
    this.cargarEstadosOT();
    this.sub = this.route.queryParams
      .subscribe(params => {
        this.cod_OT = params['cod_ot'];
        this.patenteGlob = params['patente'];
        this.fechaOT = params['fecha_ot'];
        this.cod_estado = params['estado_ot'];
        this.cod_recepcionVehiculo = params['cod_recepcion'];
        if (this.cod_OT > 0 && this.cod_OT != undefined) {
          this.esCreacion = false;
        } else {
          this.esCreacion = true;
        }
      });
    this.cargarListaPendientes();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  cargarEstadosOT() {
    this._ordenService.listadoEstadosOT()
      .subscribe((data) => {
        // console.log('ESTADOS OT : ' + JSON.stringify(data));
        let resultado = data['ListadoEstadoOTResult']['lista'];
        if (resultado != null) {
          if (resultado.length > 0) {
            this.estadosOT = resultado;
          }
        }
      }, (error) => {
        this.snackBar.open('Error al cargar estados de ot', null, { duration: 3000 });
      });
  }

  obtenerEstadoOT(data) {
    this.cod_estado = 0;
    this.cod_estado = data;
    console.log('ESTADO OT SELECCION ' + this.cod_estado);
  }

  cargarListaPendientes() {
    if (this.esCreacion) {
      this._ordenService.listadoPendientes()
        .subscribe((data) => {
          let resultado = data['ListadoOTRecepcionPatenteResult']['lista'];
          if (resultado != null) {
            if (resultado.length > 0) {
              if (resultado[0]['id_codigo_recvehiculo'] != 0) {
                this.dataSource = new MatTableDataSource(resultado);
                this.encontrado = true;
              } else {
                this.snackBar.open('Sin recepciones pendientes ⚠️', null, { duration: 5000 });
              }
            } else {
              this.encontrado = false;
            }
          }
          this.cargando = false;
        }, (error) => {
          this.cargando = false;
          this.snackBar.open('Error al cargar lista de recepciones', null, { duration: 3000 });
        });
    } else {
      this.cargando = false;
      this.mostrarActividades = true;
      this.cargarActividades(this.patenteGlob, this.cod_recepcionVehiculo);
    }
  }

  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }

  seleccion(row: any) {
    console.log('ROW ' + JSON.stringify(row));
    let patente = row['id_Patente_snapClienteVehiculo'].trim();
    let codigoRecepcion = parseInt(row['id_codigo_recvehiculo'], 10);
    let cod_responsable = parseInt(localStorage.getItem('cod_usuario'), 10);
    if (patente != null && codigoRecepcion > 0 && cod_responsable != null) {
      this.encontrado = false;
      this.mostrarActividades = true;
      this.patenteGlob = patente;
      this.cod_recepcionVehiculo = codigoRecepcion;
      this._ordenService.crearActividadesPatente(patente, this.cod_recepcionVehiculo, cod_responsable)
        .subscribe((data) => {
          console.log('RESPONSE CREAR ' + data);
          let realizado = data['CrearActividadesPatenteRecepcionResult']['realizado'];
          if (realizado) {
            let nro_OT = data['CrearActividadesPatenteRecepcionResult']['cod_OT_OTVehi'];
            this.cod_OT = nro_OT;
            this.snackBar.open('Se han creado actividades', null, { duration: 3000 });
            this.cargarActividades(patente, codigoRecepcion);
          } else {
            this.cod_OT = 0;
            this.snackBar.open('Actividades no creadas', null, { duration: 3000 });
          }
        }, (error) => {
          this.snackBar.open('Error al crear actividades', null, { duration: 3000 });
        });
    } else {
      this.snackBar.open('Recepcion o patente no definidos', null, { duration: 3000 });
    }
  }

  finalizar() {
    console.log('ESTADO ' + this.cod_estado + ' COD_RECEPCION ' + this.cod_recepcionVehiculo + ' COD_OT ' + this.cod_OT);
    if (this.cod_estado > 0) {
      if (this.cod_OT > 0) {
        if (this.cod_recepcionVehiculo > 0) {
          this._ordenService.finalizarOT(this.cod_recepcionVehiculo, this.cod_OT, this.cod_estado)
            .subscribe((data) => {
              // console.log('FINALIZA LA OT ' + JSON.stringify(data));
              let realizado = data['CambiarEstadoOTPatenteRecepcionResult']['realizado'];
              let error = data['CambiarEstadoOTPatenteRecepcionResult']['mensajeError'];
              if (realizado) {
                this.snackBar.open('Orden guardada correctamente', null, { duration: 3000 });
                this._router.navigate(['/ordentrabajo/listado_ot']);
              } else {
                this.snackBar.open('Error al guardar Orden ' + error, null, { duration: 4000 });
              }
            }, (error) => {
              this.snackBar.open('Error al guardar la Orden', null, { duration: 4000 });
            });

        } else {
          this.snackBar.open('Error en código de recepción', null, { duration: 3000 });
        }
      } else {
        this.snackBar.open('Error en código de OT', null, { duration: 3000 });
      }
    } else {
      this.snackBar.open('Debe seleccionar el estado de la Orden de trabajo', null, { duration: 4000 });
    }
  }

  cargarActividades(patente: string, codigoRecepcion: number) {
    this._ordenService.listadoActividadesPatente(patente, codigoRecepcion)
      .subscribe((data) => {
        console.log('LISTADO ACTIVIDADES ' + JSON.stringify(data));
        let resultado = data['ListadoActividadesPatenteRecepcionResult']['lista'];
        if (resultado.length > 0) {
          if (resultado[0]['id_tarea'] != 0) {
            let programada = '';
            resultado.forEach((item, index) => {
              let bit_nuevo = resultado[index]['bit_programada_detTareaManVehi'];
              if (bit_nuevo) {
                programada = 'Si';
              } else {
                programada = 'No';
              }
              resultado[index]['bit_programada_detTareaManVehi'] = programada;
            });
            this.dataSourceActividad = new MatTableDataSource(resultado);
          } else {
            this.snackBar.open('Sin actividades ⚠️', null, { duration: 3000 });
          }
        }
        // console.log('Lista de tareas: ' + JSON.stringify(data));
      }, (error) => {
        this.snackBar.open('Error al cargar actividades', null, { duration: 3000 });
      });
  }

  seleccionActividad(tarea: any) {
    const dialogRef = this.dialog.open(ModalResponsabletareaComponent, {
      disableClose: true,
      width: '1000px',
      height: 'auto',
      data: { cod_ot: this.cod_OT, cod_recepcion: this.cod_recepcionVehiculo, patente: this.patenteGlob, tarea: tarea }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        this.cargarActividades(result['patente'], result['cod_recepcion']);
      });
  }
}
