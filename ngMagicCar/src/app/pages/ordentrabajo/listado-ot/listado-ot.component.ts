import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { OrdentrabajoService } from "../../../service/ordentrabajo.service";
import { MatSnackBar } from '@angular/material';
import chileanRut from 'chilean-rut';
import * as _moment from 'moment';

@Component({
  selector: 'app-listado-ot',
  templateUrl: './listado-ot.component.html',
  styleUrls: ['./listado-ot.component.css']
})

export class ListadoOtComponent implements OnInit, OnDestroy {
  private _Subscription: Subscription;
  private ordenesTrabajo: any[] = [];
  private key: string;
  public anos: any[] = [];
  public cargando: boolean = true;

  constructor(
    private _router: Router,
    private _otservice: OrdentrabajoService,
    private snackBar: MatSnackBar
  ) {
    this.key = localStorage.getItem('Session');
    if (this.key !== 'true') {
      this._router.navigate(['/Login']);
    }
  }

  ngOnInit() {
    this.cargarOrdenes(0, 0);
    this.cargarFechasOT();
  }

  cargarFechasOT() {
    this._otservice.listadoAnoMesOt()
      .subscribe((date) => {
        let fechas = date['ListadoMesAnOTResult']['lista'];
        if (fechas.length > 0) {
          if (fechas[0]['Mes'] > 0) {
            this.anos = fechas;
          }
        }
      });
  }

  cargaFiltroFecha(ano: string) {
    this.ordenesTrabajo = [];
    let mes = ano['Mes'];
    let year = ano['Ano'];
    if (mes > 0 && year > 0) {
      this.cargarOrdenes(year, mes);
    }
  }

  ngOnDestroy(): void {
    this._Subscription.unsubscribe();
  }

  ingresarOT() {
    this._router.navigate(['/ordentrabajo/crear_ot']);
  }

  editarOT(item) {
    console.log('OT SELECCION ' + JSON.stringify(item));
    this._router.navigate(['/ordentrabajo/crear_ot'], {
      queryParams: {
        cod_ot: item['cod_OT_OTVehi'],
        fecha_ot: item['dat_fecha_OTVehi'],
        estado_ot: item['cod_estadoservicio_estadoservicio'],      
        patente: item['id_Patente_snapClienteVehiculo'],
        cod_recepcion: item['id_codigo_recvehiculo']
      }
    });
  }

  cargarOrdenes(ano: number, mes: number) {
    this._Subscription = this._otservice.listadoOT(ano, mes)
      .subscribe((res: any) => {
        console.log('ORDENES '+JSON.stringify(res));
        var resultado = res['ListadoOTMesAnoResult']['lista'];
        if (resultado != null) {
          resultado.forEach((item, index) => {
            let fecha = null;
            fecha = resultado[index]['dat_fecha_OTVehi'];
            let fechaFormateada = null;
            if (fecha) {
              fechaFormateada = new Date(parseInt(fecha.replace(/(^.*\()|([+-].*$)/g, ''))).toISOString();
            } else {
              fechaFormateada = '';
            }
            if (fechaFormateada) {
              resultado[index]['dat_fecha_OTVehi'] = fechaFormateada;
            } else {
              resultado[index]['dat_fecha_OTVehi'] = '';
            }
            let rutCliente = resultado[index]['cod_cliente_cliente'];
            let rutFormateado = null;
            if (rutCliente != null) {
              if (rutCliente.length == 8 || rutCliente.length == 9) {
                let dv = rutCliente.substring(8);
                rutFormateado = chileanRut.format(rutCliente, dv);
              }
            }
            resultado[index]['cod_cliente_cliente'] = rutFormateado;
            if (resultado[0]['cod_OT_OTVehi'] != 0) {
              this.ordenesTrabajo = resultado;
            }else{
              this.snackBar.open('Sin ordenes de trabajo', null, { duration: 3000 });
            }
          });
        } else {
          this.snackBar.open('Sin ordenes de trabajo', null, { duration: 3000 });
        }
        this.cargando = false;
      }, (error) => {
        // console.log('Error cargando ots ' + JSON.stringify(error));
        this.snackBar.open('Error al cargar ordenes de trabajo', null, { duration: 3000 });
      });
  }
}