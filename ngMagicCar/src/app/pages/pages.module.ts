import { ActividadesRecepcionComponent } from './recepcion/actividades-recepcion/actividades-recepcion.component';
import { ModalInfoadicionalComponent } from './modals/modal-infoadicional/modal-infoadicional.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';

import { CrearRecepcioComponent } from '../pages/recepcion/crear-recepcio/crear-recepcio.component';

// ANGULAR MATERIAL
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatToolbarModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatTableModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatIconModule,
  MatOptionModule,
  MatSelectModule,
  MatRadioModule,
  MatSlideToggleModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatMenuModule,
  MatSnackBarModule,
  MatNativeDateModule,
  MatSidenavModule,
  MatCardModule,
  MatProgressBar,
  MatProgressBarModule,
  MatAutocompleteModule,

} from '@angular/material';

// Components
import { CrearOtComponent } from './ordentrabajo/crear-ot/crear-ot.component';
import { ListadoOtComponent } from './ordentrabajo/listado-ot/listado-ot.component';
import { ModalRecepcionvehiculoComponent } from './modals/modal-recepcionvehiculo/modal-recepcionvehiculo.component';
import { ModalResponsabletareaComponent } from './modals/modal-responsabletarea/modal-responsabletarea.component';
import { ListadoRecepcionComponent } from './recepcion/listado-recepcion/listado-recepcion.component';
import { ImagenesRecepcionComponent } from './recepcion/imagenes-recepcion/imagenes-recepcion.component';
import { ModalEliminaractividadComponent } from './modals/modal-eliminaractividad/modal-eliminaractividad.component';
import { ModalDocvehiculoComponent } from './modals/modal-docvehiculo/modal-docvehiculo.component';
import { ModalAgregaractividadComponent } from './modals/modal-agregaractividad/modal-agregaractividad.component';
import { DragScrollModule } from 'ngx-drag-scroll/lib';
import { ModalCambiarestadoComponent } from './modals/modal-cambiarestado/modal-cambiarestado.component';
import { ModalAsignarpatenteComponent } from './modals/modal-asignarpatente/modal-asignarpatente.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PagesRoutingModule,
    ReactiveFormsModule,
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    FormsModule,
    MatIconModule,
    MatOptionModule,
    MatSelectModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMenuModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatCardModule,
    DragScrollModule,
    MatProgressBarModule,
    MatAutocompleteModule
  ],
  declarations: [
    CrearRecepcioComponent,
    CrearOtComponent,
    ListadoOtComponent,
    ModalRecepcionvehiculoComponent,
    ModalResponsabletareaComponent,
    ListadoRecepcionComponent,
    ModalInfoadicionalComponent,
    ImagenesRecepcionComponent,
    ActividadesRecepcionComponent,
    ModalEliminaractividadComponent,
    ModalDocvehiculoComponent,
    ModalAgregaractividadComponent,
    ModalCambiarestadoComponent,
    ModalAsignarpatenteComponent,
  ],
  entryComponents: [
    ModalRecepcionvehiculoComponent,
    ModalResponsabletareaComponent,
    ModalInfoadicionalComponent,
    ModalEliminaractividadComponent,
    ModalDocvehiculoComponent,
    ModalAgregaractividadComponent,
    ModalCambiarestadoComponent,
    ModalAsignarpatenteComponent,
  ]
})
export class PagesModule { }
