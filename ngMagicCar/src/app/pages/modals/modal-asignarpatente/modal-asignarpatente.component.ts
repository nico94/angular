import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Inject } from '@angular/core';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { RecepcionvehiculosService } from '../../../service/recepcionvehiculos.service';
import { Subscription } from 'rxjs';
import chileanRut from 'chilean-rut';
import { elementStyleProp } from '@angular/core/src/render3';



@Component({
  selector: 'app-modal-asignarpatente',
  templateUrl: './modal-asignarpatente.component.html',
  styleUrls: ['./modal-asignarpatente.component.css']
})
export class ModalAsignarpatenteComponent implements OnInit, OnDestroy {


  //DATOS WS
  _Subscription: Subscription;

  //DATOS AGGREGAR PATENTE
  private codMarca: number;
  private codModelo: number;
  private codVersion: number;
  private codColor: number;
  patenteNueva: string;
  marcas: any[] = [];
  modelos: any[] = [];
  version: any[] = [];
  color: any[] = [];
  rutCliente: string;
  ano: number;

  //DATOS CLIENTES  
  codComuna: number;
  nombreClinte: string;
  telefonoCliente: string;
  direccionCliente: string;
  anoCliente: string;
  comunaCliente: any[] = [];
  codigoVendedorCliente = localStorage.getItem("cod_usuario"); // SACAR DEL LOCAL STORAGE 

  mostrar = false;
  mostrar2 = false;

  @ViewChild('rutInput') rutReference: ElementRef;

  constructor(
    public dialogRef: MatDialogRef<ModalAsignarpatenteComponent>,
    private wsRecepcion: RecepcionvehiculosService,
    private snackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public patente: any
  ) {
    console.log(patente.patente);
    this.patenteNueva = patente.patente;

  }

  ngOnInit() {
    this.obtenerListadoMarca();
    this.obtenerColor();
    this.validarCliente();
    this.ListadoComuna();

  }

  ngOnDestroy() {
    this._Subscription.unsubscribe();
  }

  onNoClick(): void {
    this.dialogRef.close({ patente: this.patenteNueva }

    );
  }

  Volverpantalla() {

    // --Falta definir--
    this.mostrar2 = true;
    this.mostrar = false;
  }
  changeRutcliente(value) {
    if (this.rutCliente.length < 4) {
      console.log("RUT MALO");
    }
    if (this.rutCliente.length === 9) {

    } else {
      console.log("RUT REGISTRADO");
    }
  }


  ListadoComuna() {
    this._Subscription = this.wsRecepcion.ListadoComuna()
      .subscribe((data: any) => {
        this.comunaCliente = data['ListadoComunaResult']['lista'];

      }, (error) => {
        console.error(error);
      })
  }

  validarCliente() {

    this._Subscription = this.wsRecepcion.validarCliente(this.rutCliente)
      .subscribe((data) => {
        let cliente = data;
        console.log(cliente)

        if (cliente.ValidarClienteResult.existe === true && this.rutCliente.length >=8) {
          // this.rutCliente = '';
          this.snackBar.open('Rut registrado', null, { duration: 3000 });

        } else {
          this.mostrar2 = true;          
          
          this.snackBar.open('Rut no registrado', null, { duration: 2000 });
        }
      }, (error) => {
        console.error(error);
      });
  }

  obtenerColor() {
    this._Subscription = this.wsRecepcion.obtenerListaColor()
      .subscribe((data: any) => {
        this.color = data['ListadoColorResult']['lista'];

      }, (error) => {
        console.error(error);
      });
  }
  obtenerListadoMarca() {
    this._Subscription = this.wsRecepcion.obtenerListadoMarca()
      .subscribe((data: any) => {
        let lista = data['ListadoMarcaResult']['lista'];
        this.marcas = lista;
      }, (error) => {
        console.error(error);
      });
  }

  obtenerModelo(data) {
    this.modelos = [];
    this.wsRecepcion.obtenerListadoModeloPorMarca(data)
      .subscribe((data) => {
        this.modelos = data['ListadoModeloPorMarcaResult']['lista'];
        if (this.modelos.length == 0) {
          this.snackBar.open('No posee modelos', null, { duration: 3000 });
          this.version = [];
        }
      }, (error) => {
        this.snackBar.open('Error cargando modelo', null, { duration: 3000 });
        console.error(error);
      });
  }
  obtenerVersion(data) {
    this.version = [];
    if (data > 0) {
      this.codModelo = data;
      this.wsRecepcion.obtenerVersionPorModelo(data)
        .subscribe((data) => {
          console.log(data)

          this.version = data['ListadoVersionPorModeloResult']['lista'];
          if (this.version.length == 0) {
            this.snackBar.open('No posee Version', null, { duration: 3000 });
          }
        }, (error) => {
          this.snackBar.open('Error cargando la version', null, { duration: 3000 });
          console.error(error);
        });
    } else {
      this.codModelo = 0;
    }
  }
  obtenerVersionPorModelo(data) {
    if (data > 0) {
      this.codVersion = data;
    } else {
      this.codVersion = 0;
    }
  }
  formatoRut() {
    if (this.rutCliente) {
      if (this.rutCliente.length === 9) {
        var dv = this.rutCliente.substring(8);
        this.rutCliente = chileanRut.format(this.rutCliente, dv);
        this.validarRut(this.rutCliente);
      } else if (this.rutCliente.length === 8) {
        var dv = this.rutCliente.substring(7);
        this.rutCliente = chileanRut.format(this.rutCliente, dv);
        this.validarRut(this.rutCliente);
      } else {
        this.snackBar.open('El largo del rut no corresponde', null, { duration: 800 })
        this.rutCliente = "";
        this.mostrar = false;
      }
    }
  }
  consultaRutWs(rut: string) {
    this.mostrar = false;
    this._Subscription = this.wsRecepcion.validarCliente(rut)
      .subscribe((data) => {
        let cliente = data;
        console.log(cliente)
        if (cliente.ValidarClienteResult.existe === true) {
          this.mostrar = false;
          this.mostrar = false;
          this.snackBar.open('Rut registrado', null, { duration: 3000 });
        } else {
          this.snackBar.open('Rut no registrado', null, { duration: 3000 });
          this.mostrar = true;
          this.mostrar2 = false;
        }
      }, (error) => {
        console.error(error);
      });
  }
  validarRut(rut: string) {
    if (!chileanRut.validate(rut)) {
      this.snackBar.open('Rut inválido', null, { duration: 800 })
      this.rutCliente = "";
    } else {
      console.log('SIN FORMATO ' + chileanRut.unformat(rut));
      this.consultaRutWs(chileanRut.unformat(rut));
    }
  }

  GuardarNuevoCliente() {
    const data = {
      cod_cliente_cliente: this.rutCliente,
      des_telefono_cliente: this.telefonoCliente,
      des_nombre_cliente: this.nombreClinte,
      des_direccion_cliente: this.direccionCliente,
      cod_comuna_comuna: this.codComuna,
      cod_vendedor_fichapersonal: this.codigoVendedorCliente
    }
    this.wsRecepcion.GuardarNuevoCliente(data)
      .subscribe((result: any) => {
        if (this.nombreClinte != null && this.telefonoCliente != null &&
          this.direccionCliente != null) {

          console.log(result);
          this.snackBar.open('Cliente Agregado Correctamente', null, { duration: 4000 })
          this.Volverpantalla()
        } else {
          this.snackBar.open('Falta Ingresar Datos', null, { duration: 4000 })
        }

      }, error => console.log(error)
      )
  }

  guardarPatente() {
    if (this.rutCliente != null && this.ano != null) {
      const data = {
        patente: this.patenteNueva,
        cod_marca_marca: this.codMarca,
        cod_modelo_modelo: this.codModelo,
        cod_version_version: this.codVersion,
        cod_color_color: this.codColor,
        cod_cliente_cliente: this.rutCliente,
        num_year_vehiculo: this.ano
      }
      // console.log(this.rutCliente);
      this.wsRecepcion.GuardarAsignacionPatenteCliente(data)
        .subscribe((result: any) => {
          if (this.rutCliente != null && this.ano != null) {
            if (result.GuardarAsignacionPatenteClienteResult.resultado == false) {
              this.snackBar.open('Cliente no registrado', null, { duration: 4000 })
              this.mostrar = true;
              this.mostrar2 = false;
  
            } else {
              // console.log("EL RESULTADO ES :",result.GuardarAsignacionPatenteClienteResult.resultado);
              // console.log("EL RESULTADO ES :",result.existe);
              this.snackBar.open('Patente Agregada Correctamente', null, { duration: 4000 })
              this.onNoClick();
            }
  
          } else {
            this.snackBar.open('Falta Ingresar Datos', null, { duration: 4000 })
          }
        },
          error => console.log(error)
        )
    } else{
      this.snackBar.open('Falta Ingresar Datos', null, { duration: 4000 })
    }
    
  }
}
