import { Component, OnInit, Inject } from '@angular/core';
import { RecepcionvehiculosService } from '../../../service/recepcionvehiculos.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modal-agregaractividad',
  templateUrl: './modal-agregaractividad.component.html',
  styleUrls: ['./modal-agregaractividad.component.css']
})
export class ModalAgregaractividadComponent implements OnInit {
  _data_completa: any = [];
  _listaKilometrajes: any = [];
  _lista_actividades: any = [];
  _valor = 0;
  _desc_actividad = '';
  _model_km;
  _model_act;
  constructor(private wsRecepcion: RecepcionvehiculosService,
    public dialog: MatDialogRef<ModalAgregaractividadComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this._data_completa = data.data;
      this.listarKilmetraje();
     }

  ngOnInit() {
  }

  cerrar() {
    this.dialog.close( {result : false} );
  }

  listarKilmetraje() {
    console.log(this._data_completa);

    this.wsRecepcion.ListarKilometraje(this._data_completa)
      .subscribe((data: any) => {
        console.log(data.ListadoTareaKilometrajeResult.lista);
        // console.log(JSON.stringify(this.ListMotivo));
        this._listaKilometrajes = data.ListadoTareaKilometrajeResult.lista;
        // data.ListadoTareaKilometrajeResult.lista.forEach(element => {
        //   this._listaKilometrajes.push(element.num_kmMayor_snapTareaActVehi);
        // });

      }, (error) => {
    });
  }

  select_km(km) {

    if (km === 'sinkilometraje') {

      this.wsRecepcion.ListarTareas().subscribe( result => {
        console.log(result);
        if ( result.ListadoTareasResult.MensajError ) {
          alert('Error al mostrar actividades');
          return ;
        }
        this._valor = 0;
        this._lista_actividades = result.ListadoTareasResult.lista;
      });
    } else {
      this.wsRecepcion.ListarTareasPorKilometraje(this._data_completa, km).subscribe( result => {
        console.log(result);
        if ( result.ListadoTareasxKmResult.MensajError ) {
          alert('Error al mostrar actividades');
          return ;
        }

        this._lista_actividades = result.ListadoTareasxKmResult.lista;
      });

    }
  }

  select_act(act) {
    const d_valor = this._lista_actividades.find( element => {
        return element.id_tarea === act;
    });
    this._valor = d_valor.num_valor_snapTareaActVehi;
    this._desc_actividad = d_valor.des_tarea;
  }

  AgregarTarea() {

    const tareaAdiconal = {
        id_codigo_recvehiculo : this._data_completa[0].id_codigo_recvehiculo ,
        id_tarea : this._model_act,
        valor : this._valor,
    };


    this.wsRecepcion.AgregarTareaAdicional(tareaAdiconal).subscribe( result => {
      console.log(result.GuardarTareasMantenimientosAdicionalesResult.resultado);
      const respuesta = result.GuardarTareasMantenimientosAdicionalesResult;
      if ( !respuesta.resultado) {
        alert(respuesta.MensajeError);
        return;
      }


      const agregarTarea = {
        bit_programada_detTareaManVehi: false,
        des_tarea: this._desc_actividad,
        id_codigo_recvehiculo: this._data_completa[0].id_codigo_recvehiculo,
        id_tarea: this._model_act,
        num_valor_snapTareaActVehi: this._valor,
      };

      console.log(agregarTarea);
      this.dialog.close( {result : true, data : agregarTarea} );

    });
  }


}
