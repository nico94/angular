import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { RecepcionvehiculosService } from '../../../service/recepcionvehiculos.service';

@Component({
  selector: 'app-modal-recepcionvehiculo',
  templateUrl: './modal-recepcionvehiculo.component.html',
  styleUrls: ['./modal-recepcionvehiculo.component.css']
})
export class ModalRecepcionvehiculoComponent implements OnInit {

  listaEstadoRecepcion: any[] = [
    {
      id: 10,
      descTitulo: 'CARROCERIA',
      items: [
        {
          bit: true,
          descItem: 'Asientos'
        },
        {
          bit: false,
          descItem: 'Capot'
        }
      ]
    },
    {
      id: 10,
      descTitulo: 'FAROLES',
      items: [
        {
          bit: false,
          descItem: 'Cuñas'
        }
      ]
    }
  ];

  data_global: any;

  constructor(
    public dialogRef: MatDialogRef<ModalRecepcionvehiculoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private wsRecepcion: RecepcionvehiculosService
  ) {
     this.data_global = data.data[0];
     console.log(this.data_global);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {

    const id_vehiculo = this.data_global.id_codigo_recvehiculo ;
    this.wsRecepcion.crearRecepcion( id_vehiculo )
      .subscribe((data: any) => {
        console.log(data.ListadoEstadoVehiculoResult.lista);
        this.listaEstadoRecepcion = data.ListadoEstadoVehiculoResult.lista;
      }, (error) => {
        console.log(error);
      });
  }

  guardar() {
    const id_vehiculo = this.data_global.id_codigo_recvehiculo ;
    this.wsRecepcion.guardarEstado(this.listaEstadoRecepcion, id_vehiculo).subscribe( (data: any) => {
      console.log(data);

      if (!data.GuardarEstadoVehiculoResult.resultado) {
        alert('Error al actualizar');
        return;
      } else {
        console.log("cerrar dialogo doc")
        this.dialogRef.close({result: true});
      }
    } , err => {
      alert('Error al actualizar');
    });
  }

}

export interface DialogData {
  title: string;
  name: string;
}
