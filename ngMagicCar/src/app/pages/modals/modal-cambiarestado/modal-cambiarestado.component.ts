import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';
import { RecepcionvehiculosService } from '../../../service/recepcionvehiculos.service';
@Component({
  selector: 'app-modal-cambiarestado',
  templateUrl: './modal-cambiarestado.component.html',
  styleUrls: ['./modal-cambiarestado.component.css']
})
export class ModalCambiarestadoComponent {
  id = '';
  constructor( public dialogRef: MatDialogRef<ModalCambiarestadoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private wsRecepcion: RecepcionvehiculosService,
    private snackBar: MatSnackBar) {
      console.log(data);
      this.id = data.id;
     }




  cerrar() {
    this.dialogRef.close({
      anulado: false,
      cerrar: true
    });
  }

  anular() {
    this.wsRecepcion.anularRegistroRecepcion(this.id).subscribe( result => {
      console.log(result.anularRegistroRecepcionResult);
      if ( result.anularRegistroRecepcionResult.anulacion ) {
        this.snackBar.open('El estado se cambio correctamente', null, { duration: 5000 });
        this.dialogRef.close({
          anulado : true,
          cerrar: false
        });
      } else {
        this.snackBar.open('Tiene orden de trabajo en curso', null, { duration: 5000 });
        this.dialogRef.close({
          anulado: false,
          cerrar: false
        });
      }

    });
  }

}
