import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCambiarestadoComponent } from './modal-cambiarestado.component';

describe('ModalCambiarestadoComponent', () => {
  let component: ModalCambiarestadoComponent;
  let fixture: ComponentFixture<ModalCambiarestadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCambiarestadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCambiarestadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
