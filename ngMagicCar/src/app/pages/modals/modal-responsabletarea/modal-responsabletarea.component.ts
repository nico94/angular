import { Component, OnInit, Inject, ɵConsole } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { OrdentrabajoService } from '../../../service/ordentrabajo.service';
import * as moment from 'moment';
import { FormControl, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
export interface Materia {
  cod_producto_materiaprima: string;
  des_nombre_materiaprima: string;
}

@Component({
  selector: 'app-modal-responsabletarea',
  templateUrl: './modal-responsabletarea.component.html',
  styleUrls: ['./modal-responsabletarea.component.css']
})
export class ModalResponsabletareaComponent implements OnInit {
  myControl = new FormControl();
  options: Materia[] = [];
  filteredOptions: Observable<Materia[]>;
  private responsables: any[] = [];
  private insumos: any[] = [];
  private insumosOT: any[] = [];
  public cargando: boolean = true;
  private cod_responsable: number;
  private estado: string;
  private fechaAsiganda: string;
  private fechaIniciadaMuestra: string;
  private fechaTerminoMuestra: string;
  private horaAsignada: string;
  private fechaTermino: string;
  private horaTermino: string;
  private fechaIniciada: string;
  private horaIniciada: string;
  private patenteGlob: string;
  private id_tarea: number;
  private cod_ot: number;
  private cod_recepcion: number;
  private cod_insumo: any = null;
  private det_insumo ='';
  private cantidad_insumo: number = 0;

  public focused = false;
  insumoInput = new FormControl();

  constructor(
    @Inject(MAT_DIALOG_DATA) public tarea: any,
    public dialogRef: MatDialogRef<ModalResponsabletareaComponent>,
    private ordenTrabajoService: OrdentrabajoService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.cargaDatos();
    this.cargarResponsables(0);
    this.cargarInsumos();
    this.cargarInsumoPorOT(this.cod_recepcion, this.id_tarea, this.cod_ot);

    // this.filteredOptions = this.myControl.valueChanges
    //   .pipe(
    //     startWith<string | any>(''),
    //     map(value => typeof value === 'string' ? value : value.des_nombre_materiaprima),
    //     map(name => name ? this._filter(name) : this.insumos.slice())
    //   );

  }

  displayFn(materia?: Materia): string | undefined {
    console.log('Display ',materia);
    return materia ? materia.des_nombre_materiaprima : undefined;
  }

  private _filter(des: string): Materia[] {
    const filterValue = des.toLowerCase();
    console.log("EL DATO ES :", des);
    return this.insumos.filter(option => option.des_nombre_materiaprima.toLowerCase().indexOf(filterValue) === 0);
  }

  cerrar() {
    this.dialogRef.close({ patente: this.patenteGlob, cod_recepcion: this.cod_recepcion });
  }

  cargaDatos() {
    this.cod_responsable = this.tarea['tarea']['cod_fichapersonal_fichapersonal'];
    this.id_tarea = this.tarea['tarea']['id_tarea'];
    this.estado = this.tarea['tarea']['Estado'];
    this.patenteGlob = this.tarea['patente'];
    this.cod_ot = this.tarea['cod_ot'];
    this.cod_recepcion = this.tarea['tarea']['id_codigo_recvehiculo'];
    let fecIni = this.tarea['tarea']['dat_fecasignada_DTMantVehiculo'];
    let fecTermino = this.tarea['tarea']['dat_fectermino_DTMantVehiculo'];
    if (fecIni) {
      fecIni = new Date(parseInt(fecIni.replace(/(^.*\()|([+-].*$)/g, ''))).toString();
      if (moment(fecIni, moment.ISO_8601).isValid) {
        this.fechaIniciadaMuestra = moment(fecIni).format('DD-MM-YYYY hh:mm');
      }
    } else {
      this.fechaIniciadaMuestra = '';
    }
    if (fecTermino) {
      fecTermino = new Date(parseInt(fecTermino.replace(/(^.*\()|([+-].*$)/g, ''))).toISOString();
      if (moment(fecTermino, moment.ISO_8601).isValid) {
        this.fechaTerminoMuestra = moment(fecTermino).format('DD-MM-YYYY hh:mm');
      }
    } else {
      this.fechaTerminoMuestra = '';
    }
    // console.log('TAREA COMPLETA ' + JSON.stringify(this.tarea));
  }

  cargarResponsables(responsable: number) {
    this.ordenTrabajoService.listadoResponsables(0)
      .subscribe((data: any) => {
        let lista = data['ListadoFichaPersonalResult']['lista'];
        console.log('RESPONSABLES :', lista);
        if (lista.length > 0) {
          this.responsables = lista;
        }
        this.cargando = false;
      }, (error) => {
        this.snackBar.open('Error al cargar responsables', null, { duration: 3000 });
        this.cargando = false;
      });
  }

  agregarInsumo() {
    console.log(this.cod_insumo.cod_producto_materiaprima)
    if (this.cod_insumo.cod_producto_materiaprima) {
      if (this.cantidad_insumo > 0) {
        this.ordenTrabajoService.guardarInsumoTarea(this.cod_recepcion, this.cod_insumo.cod_producto_materiaprima, this.cantidad_insumo, this.cod_ot, this.id_tarea)
          .subscribe((data) => {
            let resultado = data['GuardarInsumoPatenteRecepcionResult']['resultado'];
            console.log("EL RESULTADO ES :", resultado);

            if (resultado) {
              this.snackBar.open('Insumo agregado correctamente', null, { duration: 3000 });
              this.cod_insumo = 0;
              this.cantidad_insumo = 0;
              // console.log("los insumos son :" , resultado);
            } else {
              this.snackBar.open('Error al guardar cantidad de insumos', null, { duration: 3000 });
            }
            this.cargarInsumoPorOT(this.cod_recepcion, this.id_tarea, this.cod_ot);
          }, (error) => {
            this.snackBar.open('Error al guardar el insumo', null, { duration: 3000 });
            console.log('RESULTADO GUARDAR INSUMO ' + JSON.stringify(error));
          });
      } else {
        this.snackBar.open('Ingrese una cantidad válida de insumo', null, { duration: 3000 });
      }
    } else {
      this.snackBar.open('Debe seleccionar el insumo', null, { duration: 3000 });
    }
  }

  cargarInsumos() {
    this.ordenTrabajoService.listadoInsumos()
      .subscribe((data: any) => {
        // console.log("la data es :", data['ListadoMateriasPrimasResult']['lista'][25]['des_nombre_materiaprima']);
        let lista = data['ListadoMateriasPrimasResult']['lista'];
        // console.log("la lista de insumos son :", lista);
        // console.log("LA DATA DE OPTIONES :" , this.options)
        this.filteredOptions = this.myControl.valueChanges
          .pipe(
            startWith<string | any>(''),
            map(codigo => typeof codigo === 'string' ? codigo : codigo.des_nombre_materiaprima),
            map(des => des ? this._filter(des) : lista.slice())
          );
        // console.log(lista.items);
        if (lista.length > 0) {
          if (lista[0]['cod_producto_materiaprima'] != null) {
            this.insumos = lista;
          }
        }
        // this.cargando = false;
      }, (error) => {
        this.snackBar.open('Error al cargar materias primas', null, { duration: 3000 });
        console.log('Error al cargar materias primas');
        this.cargando = false;
      });
  }

  cargarInsumoPorOT(cod_recepcion: number, id_tarea: number, cod_ot: number) {
    if (cod_recepcion > 0 && id_tarea > 0 && cod_ot > 0) {
      this.ordenTrabajoService.listadoInsumosPorOT(cod_recepcion, id_tarea, cod_ot)
        .subscribe((data: any) => {
          let lista = data['ListadoInsumosActividadPatenteRecepcionResult']['lista'];
          // console.log('RESPUESTA INSUMOS ' + JSON.stringify(data));
          if (lista.length > 0) {
            if (lista[0]['id_tarea'] != 0) {
              this.insumosOT = lista;
            }
          }
          this.cargando = false;
        }, (error) => {
          // console.log('ERROR INSUMOS ' + JSON.stringify(error));
          this.snackBar.open('Error al cargar insumos', null, { duration: 3000 });
          this.cargando = false;
        });
    }
  }

  obtenerResp(data) {
    this.cod_responsable = 0;
    this.cod_responsable = data['cod_fichapersonal_fichapersonal'];
  }

  // obtenerInsumo(data) {
  //   console.log('INSUMO ',data);
  //   this.cod_insumo = 0;
  //   this.cod_insumo = data['cod_producto_materiaprima'];
  // }

  convertirFecha(fech: string) {
    var date = new Date(fech), mnth = ("0" + (date.getMonth() + 1)).slice(-2), day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }

  guardarResponsable() {
    if (this.cod_responsable > 0) {
      if (this.fechaAsiganda && this.horaAsignada) {
        if (this.fechaTermino && this.horaTermino) {
          this.ordenTrabajoService.guardarResponsableActividad(
            this.patenteGlob.trim(), this.cod_recepcion,
            this.cod_ot, this.cod_responsable, 0, this.id_tarea, this.convertirFecha(this.fechaAsiganda) + 'T' + this.horaAsignada + '-03:00',
            this.convertirFecha(this.fechaTermino) + 'T' + this.horaTermino + '-03:00')
            .subscribe((data) => {
              console.log("la data es ", data);
              // console.log('REPSUESTA AL GUARDAR RESPONSABLE' + JSON.stringify(data));
              let result = data['EditarActividadPatenteRecepcionResult'];
              console.log(result);
              if (result['realizado']) {
                this.snackBar.open('Responsable guardado correctamente', null, { duration: 3000 });
                this.cerrar();
              } else {
                this.snackBar.open('Error al actualizar ' + result['mensajeError'], null, { duration: 3000 });
              }
            }, (error) => {
              this.snackBar.open('Error al actualizar responsable', null, { duration: 3000 });
            });
        } else {
          this.snackBar.open('Falta ingresar fecha/hora de término', null, { duration: 3000 });
        }
      } else {
        this.snackBar.open('Falta ingresar fecha/hora para asignación', null, { duration: 3000 });
      }
    } else {
      this.snackBar.open('Falta seleccionar responsable', null, { duration: 3000 });
    }
  }
}