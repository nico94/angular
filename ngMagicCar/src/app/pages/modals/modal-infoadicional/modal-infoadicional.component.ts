import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RecepcionvehiculosService } from '../../../service/recepcionvehiculos.service';

@Component({
  selector: 'app-modal-infoadicional',
  templateUrl: './modal-infoadicional.component.html',
  styleUrls: ['./modal-infoadicional.component.css']
})
export class ModalInfoadicionalComponent implements OnInit {



  data_recep: any = [];
  data_detalle_mantencion_listo: [];
  data_detalle_mantencion_faltante: [];
  responsable = '';
  patente = '';
  cargando: Boolean = true;
  constructor(
    public dialogRef: MatDialogRef<ModalInfoadicionalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private wsRecepcion: RecepcionvehiculosService
  ) {
    console.log(this.data.data);
    this.data_recep = this.data.data[0];
    this.responsable = this.data_recep.des_nombre_fichapersonal;
    this.patente = this.data.patente;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }



  ngOnInit() {
    this.wsRecepcion.getListaDetalleUltimaVisita( this.data_recep.fechaString, this.data_recep.id_codigo_recvehiculo)
    .subscribe((datas: any) => {
      this.cargando = false;
      this.data_detalle_mantencion_listo = datas.ListadoDetalleUltimaVisitaResult.lista;
    }, (error) => {
      this.cargando = false;
    });

    this.wsRecepcion.getListaDetalleUltimaVisitaFaltantes( this.data_recep.fechaString, this.data_recep.id_codigo_recvehiculo)
    .subscribe((datas: any) => {
      console.log(datas);
      this.cargando = false;
      this.data_detalle_mantencion_faltante = datas.ListadoLogEliminadosResult.lista;
      console.log(this.data_detalle_mantencion_faltante);
    }, (error) => {
      this.cargando = false;
    });


  }
}

export interface DialogData {
  title: string;
  name: string;
}


