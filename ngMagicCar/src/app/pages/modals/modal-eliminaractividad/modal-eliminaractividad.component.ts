import { Component, OnInit,Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RecepcionvehiculosService } from '../../../service/recepcionvehiculos.service';

@Component({
  selector: 'app-modal-eliminaractividad',
  templateUrl: './modal-eliminaractividad.component.html',
  styleUrls: ['./modal-eliminaractividad.component.css']
})
export class ModalEliminaractividadComponent implements OnInit {
  datos_actividad: any = [];
  lista_causas = [];
  observacion = '';
  codigo_causa = '';
  constructor(public dialogRef: MatDialogRef<ModalEliminaractividadComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private wsRecepcion: RecepcionvehiculosService) {
    console.log(data);
    this.datos_actividad = data.act;
    this.listaCausa();
  }

  listaCausa() {
    this.wsRecepcion.ListarCausa().subscribe( result => {
      console.log(result);
      this.lista_causas = result.ListadoCausaResult.lista;
    });
  }

  cerrar(): void {
    this.dialogRef.close({ result : false });
  }
  eliminar() {

    const info = {
      id_codigo_recvehiculo : this.datos_actividad.id_codigo_recvehiculo,
      id_tarea : this.datos_actividad.id_tarea,
      cod_causa : this.codigo_causa,
      des_observacion_legeliminacion : this.observacion
    };


    this.wsRecepcion.EliminarActividad(info).subscribe( result => {
      console.log(result);
      const r =  result.GuardarLogEliminarResult.realizado;
      if ( !r ) {
        alert(result.GuardarLogEliminarResult.MensajeError)
        this.dialogRef.close({result : false});
      } else {
        this.dialogRef.close({ result : true , data : this.datos_actividad});
      }

    });

  }

  ngOnInit() {
  }

}
