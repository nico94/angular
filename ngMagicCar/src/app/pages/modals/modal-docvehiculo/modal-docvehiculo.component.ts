import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
// Servicios
import { RecepcionvehiculosService } from '../../../service/recepcionvehiculos.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
@Component({
  selector: 'app-modal-docvehiculo',
  templateUrl: './modal-docvehiculo.component.html',
  styleUrls: ['./modal-docvehiculo.component.css']
})
export class ModalDocvehiculoComponent implements OnInit {

  _revision_tecnica = '';
  _revision_gases = '';
  _permiso_circulacion = '';
  _seguro_obligatorio = '';
  data_dialog = [];
  _patente = '';
  _enviado = false;

  revisionTecnica:any = '';
  revisionGases:any = '';
  permisoCirculacion:any = '';
  seguroObligatorio:any = '';
  constructor(public dialog: MatDialogRef<ModalDocvehiculoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private wsRecepcion: RecepcionvehiculosService) {
    console.log(data.data[0]);
    this.data_dialog = data.data[0];
    this._patente = data.patente;
  }

  ngOnInit() {
    this.wsRecepcion.getListadoDocVehiculo(this._patente)
      .subscribe(data => {       
        this.revisionTecnica = this.formatearFecha(data.ListadoDocVehiculoResult.lista[0].dat_fechavenc_snapPatenteDocVehiculo);
        this.revisionGases = this.formatearFecha(data.ListadoDocVehiculoResult.lista[1].dat_fechavenc_snapPatenteDocVehiculo);
        this.permisoCirculacion = this.formatearFecha(data.ListadoDocVehiculoResult.lista[2].dat_fechavenc_snapPatenteDocVehiculo);
        this.seguroObligatorio = this.formatearFecha(data.ListadoDocVehiculoResult.lista[3].dat_fechavenc_snapPatenteDocVehiculo); 

        this._revision_tecnica = this.revisionTecnica;
        this._revision_gases = this.revisionGases;
        this._permiso_circulacion = this.permisoCirculacion;
        this._seguro_obligatorio = this.seguroObligatorio;

      });

  }
  cerrar(): void {
    this.dialog.close();
  }

  formatearFecha(fecha_entrada){
    let fecha = null;
        fecha = fecha_entrada;
        let fechaFormateada = null;
        if (fecha) {
          fechaFormateada = new Date(parseInt(fecha.replace(/(^.*\()|([+-].*$)/g, ''))).toISOString();
        } else {
          fechaFormateada = '';
        }
        console.log(fechaFormateada);
        return fechaFormateada;
  }

  // correccion_fecha(fc) {
  //   console.log(fc);

  //   const fecha =  new Date(fc).toLocaleDateString().split('/');
  //   let dia = fecha[0];
  //   let mes = fecha[1];
  //   let year = fecha[2];
  //   if ( parseInt(fecha[0], 10) < 10 ) {
  //     dia = '0' + fecha[0];
  //   }

  //   if ( parseInt(fecha[1], 10) < 10 ) {
  //     mes = '0' + fecha[1];
  //   }


  //   return mes + '/' + dia + '/' + year;

  // }
  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    console.log(event)
  }

  async guardarDoc() {
    const fechas_doc = {
      revisionTecnica: this._revision_tecnica,
      revisionGases: this._revision_gases,
      permisoCirculacion: this._permiso_circulacion,
      seguroObligatorio: this._seguro_obligatorio
    };
    console.log(fechas_doc);
    this._enviado = true;
    this.wsRecepcion.GuardarDocumentos(this.data_dialog, fechas_doc.revisionTecnica, this._patente, 1).subscribe(result_tecnica => {
      console.log(result_tecnica);
      if (!result_tecnica.GuardarDocVehiculosResult.realizado) { alert('Error'); this.dialog.close({ result: false }); }
      this.wsRecepcion.GuardarDocumentos(this.data_dialog, fechas_doc.revisionGases, this._patente, 2).subscribe(result_gases => {
        console.log(result_gases);
        if (!result_gases.GuardarDocVehiculosResult.realizado) { alert('Error'); this.dialog.close({ result: false }); }
        this.wsRecepcion.GuardarDocumentos(
          this.data_dialog, fechas_doc.permisoCirculacion, this._patente, 3).subscribe(result_circulacion => {
            console.log(result_circulacion);
            if (!result_circulacion.GuardarDocVehiculosResult.realizado) { alert('Error'); this.dialog.close({ result: false }); }
            this.wsRecepcion.GuardarDocumentos(
              this.data_dialog, fechas_doc.seguroObligatorio, this._patente, 4).subscribe(result_seguro => {
                console.log(result_seguro);
                if (!result_seguro.GuardarDocVehiculosResult.realizado) { alert('Error'); this.dialog.close({ result: false }); }
                this.dialog.close({ result: true });
              });

          });

      });


    });

  }

}
