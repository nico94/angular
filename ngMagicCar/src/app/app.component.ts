import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from './service/login.service';
import { Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  mode = new FormControl('over');
  shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));
  // Instalaciones
  /*
  npm install bootstrap
  npm install ngx-device-detector --save
  npm i @tipes/node
  npm install md5-typescript -save
  npm install @angular/animations --save
  npm install --save @angular/material @angular/cdk @angular/animations
  npm install --save angular/material2-builds angular/cdk-builds angular/animations-builds
  npm install --save hammerjs
  */

  key: string;

  home: boolean;

  private _Subscription: Subscription;

  // Verificar Login
  constructor(private _router: Router, private wsLogin: LoginService) {

    this._Subscription = this.wsLogin.getEmittedValue()
      .subscribe((item: any) => {
        this.home = item;
        console.log('Home: ' + this.home);
      });

    // localStorage.clear();
    this.key = localStorage.getItem('Session');

    if (this.key !== 'true') {
      this._router.navigate(['/Login']);
    } else {
      this.wsLogin.change(true);
    }
  }

  cerrarSession() {
    localStorage.removeItem('Session');
    localStorage.removeItem('cod_usuario');
    this.wsLogin.change(false);
    this._router.navigate(['/Login']);
  }
}
