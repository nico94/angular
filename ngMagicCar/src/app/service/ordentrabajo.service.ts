import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Insumo, IInsumo } from '../interfaces/Insumo.class';

@Injectable({
  providedIn: 'root'
})
export class OrdentrabajoService {
  private cabeceras: any;
  // private ip = 'http://186.0.100.135'; //Chechin
  private ip = '192.168.100.135'; //Server Cristian  
  // private ip = '192.168.0.100'; //Server 
  // private ip = '186.10.19.170'; //Server Ip Publica

  // private ip = '186.10.19.170'; //Server
  // private ws_nombre = '/MagicCarWs/Servicios/OrdenTrabajo.svc/rest/'; // Server 0.100 Name Public
  private ws_nombre = '/WsMagicCar/Servicios/OrdenTrabajo.svc/rest/'; //server 100.135 Name Cristian

  private ws_nombre_mantencion = '/WsMagicCar/Servicios/mantencion.svc/rest/';
  private apiURL_Listado = 'http://' + this.ip + this.ws_nombre + 'ListadoOTMesAno';
  private apiURL_ListadoPendientes = 'http://' + this.ip + this.ws_nombre + 'ListadoOTRecepcionPatente';
  private apiURL_ListadoActividades = 'http://' + this.ip + this.ws_nombre + 'ListadoActividadesPatenteRecepcion';
  private apiURL_ListadoResponsables = 'http://' + this.ip + this.ws_nombre + 'ListadoFichaPersonal';
  private apiURL_ListadoOTAnoMes = 'http://' + this.ip + this.ws_nombre + 'ListadoMesAnOT';
  private apiURL_ListadoEstadosOt = 'http://' + this.ip + this.ws_nombre + 'ListadoEstadoOT';
  private apiURL_CrearActividadesPatente = 'http://' + this.ip + this.ws_nombre + 'CrearActividadesPatenteRecepcion';
  private apiURL_GuardarResponsable = 'http://' + this.ip + this.ws_nombre + 'EditarActividadPatenteRecepcion';
  private apiURL_ListadoInsumosPorOT = 'http://' + this.ip + this.ws_nombre + 'ListadoInsumosActividadPatenteRecepcion';
  private apiURL_GuardarInsumoTarea = 'http://' + this.ip + this.ws_nombre + 'GuardarInsumoPatenteRecepcion';
  private apiURL_FinalizaOT = 'http://' + this.ip + this.ws_nombre + 'CambiarEstadoOTPatenteRecepcion';
  private apiURL_ListadoInsumos = 'http://' + this.ip + this.ws_nombre_mantencion + 'ListadoMateriasPrimas';

  private apiURL_Patente = 'http://' + this.ip + this.ws_nombre + '';

  constructor(private http: HttpClient) {
    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  listadoOT(ano: number, mes: number) {
    const cuerpo = { 'ListadoOTMesAnorequest': { 'Mes': mes, 'Año': ano } };
    return this.http.post<any>(this.apiURL_Listado, cuerpo, { headers: this.cabeceras });
  }

  listadoPendientes() {
    const cuerpo = { 'ListadoOTRecepcionPatenterequest': { 'Patente': '' } };
    return this.http.post<any>(this.apiURL_ListadoPendientes, cuerpo, { headers: this.cabeceras });
  }

  listadoActividadesPatente(patente: string, cod_rec: number) {
    const cuerpo = { 'ListadoActividadesPatenteRecepcionrequest': { 'Patente': patente, 'id_codigo_recvehiculo': cod_rec } };
    return this.http.post<any>(this.apiURL_ListadoActividades, cuerpo, { headers: this.cabeceras });
  }

  consultarPatente(patente: string) {
    const cuerpo = { 'Request': { 'id_patente': patente } };
    return this.http.post<any>(this.apiURL_Patente, cuerpo, { headers: this.cabeceras });
  }

  listadoResponsables(responsable: number) {
    const cuerpo = { 'ListadoFichaPersonalrequest': { 'cod_fichapersonal_fichapersonal': responsable } };
    return this.http.post<any>(this.apiURL_ListadoResponsables, cuerpo, { headers: this.cabeceras });
  }

  listadoAnoMesOt() {
    let cuerpo = {};
    return this.http.post<any>(this.apiURL_ListadoOTAnoMes, cuerpo, { headers: this.cabeceras });
  }

  listadoEstadosOT() {
    let cuerpo = {
      'ListadoEstadoOTrequest': {
        'cod_estadoservicio_estadoservicio': 0
      }
    };
    return this.http.post<any>(this.apiURL_ListadoEstadosOt, cuerpo, { headers: this.cabeceras });
  }

  crearActividadesPatente(patente: string, cod_recepcion: number, cod_fichapersonal: number) {
    const cuerpo = {
      'CrearActividadesPatenteRecepcionrequest': {
        'Patente': patente,
        'id_codigo_recvehiculo': cod_recepcion,
        'cod_fichapersonal_fichapersonal': cod_fichapersonal
      }
    };
    // console.log('CREAR ACTIVIDADES: ' + JSON.stringify(cuerpo));
    return this.http.post<any>(this.apiURL_CrearActividadesPatente, cuerpo, { headers: this.cabeceras });
  }

  guardarResponsableActividad(
    patente: string, cod_recepcion: number, cod_ot: number, cod_ficha_personal: number, cod_estado: number,
    id_tarea: number, FechaHoraAsignadaInicio: string, FechaHoraAsignadaTermino: string
  ) {
    const cuerpo = {
      'EditarActividadPatenteRecepcionrequest': {
        'Patente': patente,
        'id_codigo_recvehiculo': cod_recepcion,
        'cod_OT_OTVehi': cod_ot,
        'cod_fichapersonal_fichapersonal': cod_ficha_personal,
        'cod_estado': cod_estado,
        'id_tarea': id_tarea,
        'FechaHoraAsignadaInicio': FechaHoraAsignadaInicio,
        'FechaHoraAsignadaTermino': FechaHoraAsignadaTermino
      }
    };
    console.log('ENVIO GUARDAR RESPONSABLE ', cuerpo);
    return this.http.post<any>(this.apiURL_GuardarResponsable, cuerpo, { headers: this.cabeceras });
  }

  listadoInsumos() {
    const cuerpo = {
      'ListadoMateriasPrimasrequest': {
        'cod_producto_materiaprima': -1
      }
    };
    return this.http.post<any>(this.apiURL_ListadoInsumos, cuerpo, { headers: this.cabeceras });
  }

  listadoInsumosPorOT(cod_recepccion: number, id_tarea: number, cod_ot: number) {
    const cuerpo = {
      'ListadoInsumosActividadPatenteRecepcionrequest': {
        'id_codigo_recvehiculo': cod_recepccion,
        'cod_OT_OTVehi': cod_ot,
        'id_tarea': id_tarea
      }
    };
    // console.log('LISTADO INSUMOS ' + JSON.stringify(cuerpo));
    return this.http.post<any>(this.apiURL_ListadoInsumosPorOT, cuerpo, { headers: this.cabeceras });
  }

  guardarInsumoTarea(cod_recepcion: number, cod_insumo: string, cantidad: number, cod_OT: number, id_tarea: number) {
    const cuerpo = {
      'GuardarInsumoPatenteRecepcionrequest': {
        'detmateriasprimasot': {
          'id_codigo_recvehiculo': cod_recepcion,
          'cod_producto_materiaprima': cod_insumo,
          'num_cantidad_DMatOT': cantidad,
          'cod_OT_OTVehi': cod_OT,
          'id_tarea': id_tarea
        }
      }
    };
    console.log('GUARDAR INSUMO ' , (cuerpo));
    return this.http.post<any>(this.apiURL_GuardarInsumoTarea, cuerpo, { headers: this.cabeceras });
  }

  finalizarOT(cod_recepcion: number, cod_ot: number, cod_estado: number) {
    const cuerpo = {
      'CambiarEstadoOTPatenteRecepcionrequest': {
        'id_codigo_recvehiculo': cod_recepcion,
        'cod_OT_OTVehi': cod_ot,
        'cod_estado': cod_estado
      }
    };
    // console.log('FINALIZAR OT ' + JSON.stringify(cuerpo));
    return this.http.post<any>(this.apiURL_FinalizaOT, cuerpo, { headers: this.cabeceras });
  }
}