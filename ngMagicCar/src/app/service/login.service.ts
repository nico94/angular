import { Injectable, Output, EventEmitter } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { RUTA_WS } from '../config/webService';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  @Output() fire: EventEmitter<any> = new EventEmitter();

  private cabeceras: any;

  private apiURL_Login = '/Servicios/Usuario.svc/rest/login';

  constructor( private http: HttpClient ) {
    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  change(status: boolean) {
    console.log('change started');
    this.fire.emit(status);
  }

  getEmittedValue() {
    return this.fire;
  }

  iniciarSession(usuario: string, password: string) {
    const cuerpo = {
      'usuario':
        {
          'IdUsuario': usuario,
          'PswUsuario': password
        }
    };
    return this.http.post<any>(RUTA_WS + this.apiURL_Login, cuerpo, { headers: this.cabeceras });
  }

}
