import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class RecepcionvehiculosService {

  private cabeceras: any;
  // private ip = 'http://186.10.19.170'; //Server
  private ip = 'http://192.168.100.135'; //Server Cristian  
  // private ip = 'http://192.168.0.100'; //Server
  // private nombreWS = this.ip + '/MagicCarWs/';
  private nombreWS = this.ip + '/WsMagicCar/'; //Server 100.100 Name Cristin

  private apiURL_Listado = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoRecepcion';
  private apiURL_Patente = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoClientePatente';
  private apiURL_Motivo = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoMotivo';
  private apiURL_Crear = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoEstadoVehiculo';
  private apiURL_ListadodetalleUltimaVisita = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoDetalleUltimaVisita';
  private apiURL_ListadodetalleUltimaVisitaSR = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoLogEliminados';
  private apiURL_GuardarEstado = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/GuardarEstadoVehiculo';
  private apiURL_ListarKilometraje = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoTareaKilometraje';
  private apiURL_GuardarKilometraje = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/GuardarTareasMantenimientosProgramadas';
  private apiURL_GuardarRecepcionVehiculo = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/GuardarRecepcionVehiculo';
  private apiURL_GuardarDocumetosVehiculo = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/GuardarDocVehiculos';
  private apiURL_ListadoDocumentoVehiculo = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoDocVehiculo';

  private apiURL_GuardarFotoVehiculo = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/GuardarFotosVehiculo';
  private apiURL_ListarActividades = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoActividadAsignada';
  private apiURL_ListarCausas = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoCausa';
  private apiURL_EliminarActividad = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/GuardarLogEliminar';
  private apiURL_ListarTareasPorKilometraje = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoTareasxKm';
  private apiURL_AgregarTareaAdicional = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/GuardarTareasMantenimientosAdicionales';
  private apiURL_ListarTareas = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoTareas';
  private apiURL_RescatarIdRecVehiculo = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/RetornarID';
  private apiURL_AnularRegistroRecepcion = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/anularRegistroRecepcion';
  private apiURL_ListadoMarca = this.nombreWS + 'Servicios/mantencion.svc/rest/ListadoMarca';
  private apiURL_ListadoVersionPorModelo = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoVersionPorModelo';
  private apiURL_ListadoColor = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoColor';
  private apiURL_GuardarAsignacionPatenteCliente = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/GuardarAsignacionPatenteCliente';
  private apiURL_ValidarCliente = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ValidarCliente';
  private apiURL_ListadoComuna = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoComuna';
  private apiURL_GuardarNuevoCliente = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/GuardarNuevoCliente';
  private apiURL_ListadoModeloPorMarca = this.nombreWS + 'Servicios/recepcionVehiculo.svc/rest/ListadoModeloPorMarca';

  constructor(private http: HttpClient) {
    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  ListadoComuna(){
    const cuerpo = {
      'ListadoComunarequest':
      {
        'cod_comuna_comuna' : -1
      }
    };    
    return this.http.post<any>(this.apiURL_ListadoComuna, cuerpo, { headers: this.cabeceras });
  }
  GuardarNuevoCliente(data){
    const cuerpo = {
      'GuardarNuevoClienterequest': data
    };    
    return this.http.post<any>(this.apiURL_GuardarNuevoCliente, cuerpo, { headers: this.cabeceras });
  }
  getListadoDocVehiculo(patente: string) {
    const cuerpo = {
      'ListadoDocVehiculorequest':
      {
        'id_Patente_snapClienteVehiculo': patente
      }
    };
    console.log(cuerpo);
    return this.http.post<any>(this.apiURL_ListadoDocumentoVehiculo, cuerpo, { headers: this.cabeceras });
  }

  validarCliente(codValido: string){
    const cuerpo = {
      'ValidarClienterequest':
      {
        'cod_cliente_cliente' : codValido
      }
    };
    console.log("El cuerpo es",cuerpo);
    return this.http.post<any>(this.apiURL_ValidarCliente, cuerpo, { headers: this.cabeceras });
  }  

  obtenerListaColor(){
    const cuerpo = {
      'ListadoColorrequest':
      {
        'cod_color_color' : -1
      }
    };
    return this.http.post<any>(this.apiURL_ListadoColor, cuerpo, { headers: this.cabeceras });
  }


  obtenerListadoModeloPorMarca(codMarca: number){
    const cuerpo = {
      'ListadoModeloPorMarcarequest':
      {
        'cod_marca_marca' : codMarca
      }
    };
    return this.http.post<any>(this.apiURL_ListadoModeloPorMarca, cuerpo, { headers: this.cabeceras });
    
  }
  obtenerVersionPorModelo(codVersion: number){
    const cuerpo = {
      'ListadoVersionPorModelorequest':
      {
        'cod_modelo_modelo': codVersion
      }
    };    
    return this.http.post<any>(this.apiURL_ListadoVersionPorModelo, cuerpo, { headers: this.cabeceras });
  }

  obtenerListadoMarca() {
    const cuerpo = {
      'ListadoMarcarequest':
      {
        'cod_marca_marca': -1
      }
    };    
    return this.http.post<any>(this.apiURL_ListadoMarca, cuerpo, { headers: this.cabeceras });
  }

  getListado(year: string, month: string) {
    const cuerpo = {
      'ListadoRecepcionrequest':
      {
        'year': year,
        'month': month
      }
    };
    return this.http.post<any>(this.apiURL_Listado, cuerpo, { headers: this.cabeceras });
  }

  consultarPatente(patente: string) {
    const cuerpo = {
      'ListadoClientePatenterequest':
      {
        'id_PAtente_snapClienteVehiculo': patente
      }
    };
    console.log(cuerpo);

    return this.http.post<any>(this.apiURL_Patente, cuerpo, { headers: this.cabeceras });
  }

  getListadoMotivo() {
    const cuerpo = {};
    return this.http.post<any>(this.apiURL_Motivo, cuerpo, { headers: this.cabeceras });
  }


  getListaDetalleUltimaVisita(fecha: String, cod_rec_vehiculo: Number) {

    const cuerpo = {
      'ListadoDetalleUltimaVisitarequest':
      {
        'fecha_ultima': fecha.valueOf(),
        'id_codigo_recvehiculo': cod_rec_vehiculo
      }
    };
    return this.http.post<any>(this.apiURL_ListadodetalleUltimaVisita, cuerpo, { headers: this.cabeceras });
  }

  GuardarAsignacionPatenteCliente( data ){
    const cuerpo = {
      'GuardarAsignacionPatenteClienterequest': data
      
    };
    return this.http.post<any>(this.apiURL_GuardarAsignacionPatenteCliente, cuerpo, { headers: this.cabeceras });
  }

  getListaDetalleUltimaVisitaFaltantes(fecha: String, cod_rec_vehiculo: Number) {

    const cuerpo = {
      'ListadoLogEliminadosrequest':
      {
        'id_codigo_recvehiculo': cod_rec_vehiculo
      }
    };
    return this.http.post<any>(this.apiURL_ListadodetalleUltimaVisitaSR, cuerpo, { headers: this.cabeceras });
  }




  crearRecepcion(id_vehiculo) {
    const cuerpo = {
      'ListadoEstadoVehiculorequest':
      {
        'id_codigo_recvehiculo': id_vehiculo
      }
    };
    return this.http.post<any>(this.apiURL_Crear, cuerpo, { headers: this.cabeceras });
  }


  guardarEstado(estados, id_vehiculo) {
    const cuerpo = {
      'GuardarEstadoVehiculorequest':
      {
        'lista': estados,
        'id_codigo_recvehiculo': id_vehiculo
      }
    };
    console.log(cuerpo);
    return this.http.post<any>(this.apiURL_GuardarEstado, cuerpo, { headers: this.cabeceras });
  }

  ListarKilometraje(data: any) {
    console.log(data[0]);
    const { cod_marca_marca, cod_modelo_modelo, cod_version_version, num_year_vehiculo } = data[0];
    const cuerpo = {
      'ListadoTareaKilometrajerequest':
      {
        cod_marca_marca: cod_marca_marca,
        cod_modelo_modelo: cod_modelo_modelo,
        cod_version_version: cod_version_version,
        cod_year_Vehiculo: num_year_vehiculo
      }
    };

    console.log(cuerpo);

    return this.http.post<any>(this.apiURL_ListarKilometraje, cuerpo, { headers: this.cabeceras });
  }

  GuardarKilometraje(data: any, kilometraje) {
    console.log(data);
    const { cod_marca_marca, cod_modelo_modelo, cod_version_version, num_year_vehiculo, id_codigo_recvehiculo } = data;
    const cuerpo = {
      'GuardarTareasMantenimientosProgramadasrequest':
      {
        cod_marca_marca: cod_marca_marca,
        cod_modelo_modelo: cod_modelo_modelo,
        cod_version_version: cod_version_version,
        cod_year_Vehiculo: num_year_vehiculo,
        num_kmMayor_snapTareaActVehi: kilometraje,
        id_codigo_recvehiculo: id_codigo_recvehiculo
      }
    };

    console.log(cuerpo);

    return this.http.post<any>(this.apiURL_GuardarKilometraje, cuerpo, { headers: this.cabeceras });
  }


  GuardarRecepcionVehiculo(data: any) {
    console.log(data);
    const cuerpo = {
      'GuardarRecepcionVehiculorequest': {
        dat_fecharecepcion_encrecvehiculo: data.dat_fecharecepcion_encrecvehiculo,
        num_prioridad_encrecvehiculo: data.num_prioridad_encrecvehiculo,
        num_kilometraje_encrecvehiculo: data.num_kilometraje_encrecvehiculo,
        num_nivelcombustible_encrecvehiculo: data.num_nivelcombustible_encrecvehiculo,
        num_profneumDder_encrecvehiculo: data.num_profneumDder_encrecvehiculo,
        num_profneumDizq_encrecvehiculo: data.num_profneumDizq_encrecvehiculo,
        num_profneumTder_encrecvehiculo: data.num_profneumTder_encrecvehiculo,
        num_profneumTizq_encrecvehiculo: data.num_profneumTizq_encrecvehiculo,
        des_observacion_encrevehiculo: data.des_observacion_encrevehiculo,
        id_Patente_snapClienteVehiculo: data.id_Patente_snapClienteVehiculo,
        cod_cliente_cliente: data.cod_cliente_cliente,
        cod_fichapersonal_fichapersonal: data.cod_fichapersonal_fichapersonal,
        listaMotivos: data.listaMotivos,
        num_kmant_encrecvehiculo: data.num_kmant_encrecvehiculo,
        bit_espropietario_encrecvehiculo: data.bit_espropietario_encrecvehiculo,
        des_chofer_encrecvehiculo: data.des_chofer_encrecvehiculo
      }
    };

    console.log(cuerpo);

    return this.http.post<any>(this.apiURL_GuardarRecepcionVehiculo, cuerpo, { headers: this.cabeceras });
  }

  GuardarDocumentos(data, fecha_doc, patente, id) {
    console.log(data);
    const cuerpo = {
      'GuardarDocVehiculosrequest':
      {
        lista: [
          {
            id_codigo_docvehiculo: id,
            // bit_activo_snapPatenteDocVehiculo : data.bit_activo_snapPatenteDocVehiculo,
            id_Patente_snapClienteVehiculo: patente,
            cod_cliente_cliente: data.cod_cliente_cliente,
            dat_fechavenc_snapPatenteDocVehiculo: fecha_doc,
          }
        ]


      }
    };
    console.log(cuerpo);
    return this.http.post<any>(this.apiURL_GuardarDocumetosVehiculo, cuerpo, { headers: this.cabeceras });
  }

  GuardarFotoVehiculo(data) {
    console.log(data);
    const cuerpo = {
      'GuardarFotosVehiculorequest': {
        lista: data
      }
    };
    console.log(cuerpo);
    return this.http.post<any>(this.apiURL_GuardarFotoVehiculo, cuerpo, { headers: this.cabeceras });
  }

  ListarActividades(id) {
    const cuerpo = {
      'ListadoActividadAsignadarequest': {
        id_codigo_recvehiculo: id
      }
    };
    return this.http.post<any>(this.apiURL_ListarActividades, cuerpo, { headers: this.cabeceras });
  }


  ListarCausa() {
    const cuerpo = {};
    return this.http.post<any>(this.apiURL_ListarCausas, cuerpo, { headers: this.cabeceras });
  }

  EliminarActividad(data) {
    console.log(data);
    const cuerpo = {
      'GuardarLogEliminarrequest': data
    };
    console.log(cuerpo);
    return this.http.post<any>(this.apiURL_EliminarActividad, cuerpo, { headers: this.cabeceras });
  }


  ListarTareasPorKilometraje(data: any, km) {
    console.log(data[0]);
    const { cod_marca_marca, cod_modelo_modelo, cod_version_version, num_year_vehiculo } = data[0];
    const cuerpo = {
      'ListadoTareasxKmrequest':
      {
        cod_marca_marca: cod_marca_marca,
        cod_modelo_modelo: cod_modelo_modelo,
        cod_version_version: cod_version_version,
        cod_year_Vehiculo: num_year_vehiculo,
        num_kmMayor_snapTareaActVehi: km
      }
    };

    console.log(cuerpo);

    return this.http.post<any>(this.apiURL_ListarTareasPorKilometraje, cuerpo, { headers: this.cabeceras });
  }


  ListarTareas() {
    const cuerpo = {};

    return this.http.post<any>(this.apiURL_ListarTareas, cuerpo, { headers: this.cabeceras });
  }

  AgregarTareaAdicional(data) {
    const cuerpo = {
      'GuardarTareasMantenimientosAdicionalesrequest': data
    };

    console.log(cuerpo);

    return this.http.post<any>(this.apiURL_AgregarTareaAdicional, cuerpo, { headers: this.cabeceras });
  }

  RescatarIdRecVeiculo(patente) {
    console.log(patente);
    const cuerpo = {
      'RetornarIDrequest': {
        id_PAtente_snapClienteVehiculo: patente
      }
    };
    console.log(cuerpo);
    return this.http.post<any>(this.apiURL_RescatarIdRecVehiculo, cuerpo, { headers: this.cabeceras });
  }

  anularRegistroRecepcion(id) {
    console.log(id);
    const cuerpo = {
      'anularRegistroRecepcionrequest': {
        id_codigo_recvehiculo: id
      }
    };
    console.log(cuerpo);
    return this.http.post<any>(this.apiURL_AnularRegistroRecepcion, cuerpo, { headers: this.cabeceras });
  }

}
