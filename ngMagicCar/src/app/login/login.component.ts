import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Md5 } from 'md5-typescript';
import { LoginService } from '../service/login.service';

// Angualr Material
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  key: string;
  Loginkey: string;
  config: string [];

  private _Subscription: Subscription;

  interval: any;

  // Variables
  user: any[] = [];
  _usr = '';
  _psw = '';

  constructor(
    private _router: Router,
    private httpService: HttpClient,
    private wsLogin: LoginService,
    public snackBar: MatSnackBar) {

      this.key = localStorage.getItem('Session');

      if (this.key === 'true') {
        this._router.navigate(['/Home']);
      }

    // this.timerOn();
  }

  ngOnDestroy() {
    // this._Subscription.unsubscribe();
    // clearInterval(this.interval);
  }

  timerOn() {
    this.interval = setInterval(() => {
      console.log('Consultando al WS');
    }, 1000);
  }

  ngOnInit() {
    // this.httpService.get('./assets/config.json').subscribe(
    //   data => {
    //     this.config = data as string [];
    //     console.log('MAC ' + this.config[0]['MAC']);
    //   },
    //   (err) => {
    //     console.log (err.message);
    //   }
    // );
  }

  iniciarSession() {
    this._Subscription = this.wsLogin.iniciarSession(this._usr, Md5.init(this._psw))
    .subscribe((data: any) => {
      this.user = data['InciarSesionResult'];
      if (this.user['estado']) {
        this.Loginkey = 'true';
        localStorage.setItem('Session', this.Loginkey);
        localStorage.setItem('cod_usuario', this.user['codigo']);
        this._router.navigate(['/Home']);
        this.wsLogin.change(true);
      } else {
        this.snackBar.open('Usuario o contraseña incorrecto', null, {
          duration: 3000,
        });
      }
    }, (error) => {

    });
  }

}
