export class IUsuario {
    IdUsuario: number;
    Rut: string;
    Nombre: string;
    Apellidos: string;
    Email: string;
    Password: string;
    Estado: string;
}