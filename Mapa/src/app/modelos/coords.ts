export class ICoords {
    Latitude: number;
    Longitude: number;
    Nombre: string;
    NumeroHectareas: number;
}