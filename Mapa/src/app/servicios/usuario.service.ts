import { Injectable } from '@angular/core';

@Injectable()
export class UsuarioService {
  private estaLogueado;
  private rut;

  constructor() { 
    this.estaLogueado = false;
  }

  sesionIniciada(){
    this.estaLogueado = true;
  }

  obtenerEstadoLogueo(){
    return this.estaLogueado;
  }
}