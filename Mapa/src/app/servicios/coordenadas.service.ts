import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse } from '@angular/common/http'
import {Observable} from 'rxjs/Observable';
import { ICoords } from '../modelos/coords';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
 
@Injectable()
export class CoordsService {
    ip = '186.10.19.170:8080';
    //ip = '192.168.50.100:8080';
    //ip = '192.168.50.219:8081';
    url_coords = 'http://'+this.ip+'/mapa/coordenadas.php';
    url_cuadrados = 'http://'+this.ip+'/mapa/cuadrados.php';
    url_predios = 'http://'+this.ip+'/mapa/predios.php';
    url_gastos = 'http://'+this.ip+'/mapa/gastos.php';
    url_tip_gastos = 'http://'+this.ip+'/mapa/tip_gastos.php';
    private idUsuario: string;
    
    constructor(private http:HttpClient) {
        this.idUsuario = localStorage.getItem('IdUsuario');
    }
 
    getCoords(): Observable<any> {
        return this.http
        .get<any>(this.url_coords+"?IdUsuario="+this.idUsuario)
        .retry(3)
        //.do(data =>console.log('Response ' + JSON.stringify(data)))
        .catch((err:any)=>{
            return Observable.throw(err);
        });
    }

    getCuadrados(): Observable<any> {
        return this.http
        .get<any>(this.url_cuadrados+"?IdUsuario="+this.idUsuario)
        .retry(3)
        // .do(data =>console.log('Response ' + JSON.stringify(data)))
        .catch((err:any)=>{
            return Observable.throw(err);            
        });
    }

    getPredios(): Observable<any>{
        return this.http
        .get<any>(this.url_predios+"?IdUsuario="+this.idUsuario)
        .retry(3)
        //.do(data =>console.log('Response ' + JSON.stringify(data)))
        .catch((err:any)=>{
            return Observable.throw(err);
        });
    }

    getGastos(): Observable<any>{
        return this.http
        .get<any>(this.url_gastos+"?IdUsuario="+this.idUsuario)
        .retry(3)
        //.do(data =>console.log('Response Gastos '+JSON.stringify(data)))
        .catch((err:any)=>{
            return Observable.throw(err);
        });
    }

    getTipGastos(): Observable<any>{
        return this.http
        .get<any>(this.url_tip_gastos+"?IdUsuario="+this.idUsuario)
        .retry(3)
        //.do(data =>console.log('Response Gastos '+JSON.stringify(data)))
        .catch((err:any)=>{
            return Observable.throw(err);
        });
    }
}