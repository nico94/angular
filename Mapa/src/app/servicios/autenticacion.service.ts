import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IUsuario } from '../modelos/usuario';
import { RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AlertService } from './alert.service';
import { Router } from '@angular/router';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';

@Injectable()
export class AutenticacionService {
  ip = '186.10.19.170:8080';
  // ip = '192.168.50.219:8081';
  urlLogin:string='http://'+this.ip+'/mapa/login.php';
  usuario: IUsuario[];
  
  constructor(
    private http: HttpClient,
    private router: Router
  ){}

  hacerLogin(rut: string, pass: string): Observable<any>{
    if(rut!=null && pass!=null){
      return this.http
      .get<any>(this.urlLogin+"?rut="+rut+"&password="+pass)
      //.retry(3)
      //.do(data=>console.log('response '+JSON.stringify(data)))
      .catch((err:any)=>{
        return Observable.throw(err);
      });
    }
  }

  usuarioGuardado(){
    return JSON.parse(localStorage.getItem('usuarioLogueado'));
  }

  cerrarSesion(){
    localStorage.removeItem('IdUsuario');
    this.router.navigate(['app-login']);
    //localStorage.removeItem('usuarioLogueado');
  }
}