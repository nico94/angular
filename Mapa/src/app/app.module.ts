import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
//MIS COMPONENTES
import { AppComponent } from './app.component';
import { MapaComponent } from './mapa/mapa.component';
import { LoginComponent } from './login/login.component';
//HTTP
import { HttpModule }    from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { CoordsService } from './servicios/coordenadas.service';
//MODULOS
import { AgmCoreModule } from '@agm/core';
import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts';
import { MatButtonModule, 
      MatMenuModule,
      MatToolbarModule,
      MatIconModule,
      MatSnackBarModule,
      MatCardModule,
      MatInputModule,
      MatGridListModule,
      MatDialogModule,
      MatProgressSpinnerModule,
      MatTabsModule,
      MatCheckboxModule,
      MatTooltipModule
     } from '@angular/material';
//SERVICIOS
import { AutenticacionService } from './servicios/autenticacion.service';
import { AlertService } from './servicios/alert.service';
import { UsuarioService } from './servicios/usuario.service';
import { AuthguardGuard } from './guards/authguard.guard';
//import { DialogoInformacionComponent } from './dialogo-informacion/dialogo-informacion.component';
import { DialogoLogoutComponent } from './dialogo-logout/dialogo-logout.component';

//Declaro mis rutas de componentes
const appRoutes: Routes = [
  { 
    path: 'app-login',
    component: LoginComponent
  },
  {
    path:'mapa',
    canActivate: [AuthguardGuard],
    component: MapaComponent 
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { useHash: true } ),
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ChartsModule,
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatSnackBarModule,
    MatCardModule,
    MatInputModule,
    MatGridListModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatCheckboxModule,
    MatTooltipModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD6mFrHNGKGm0fYv55pzik8iugcMzgWVQ0'
    }),
    AgmSnazzyInfoWindowModule
  ],
  entryComponents: [
    DialogoLogoutComponent
  ],
  providers: [ 
    AuthguardGuard,
    CoordsService,
    AutenticacionService,
    AlertService,
    UsuarioService
  ],
  declarations: [ 
    AppComponent, LoginComponent, MapaComponent, DialogoLogoutComponent
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule {}