import { Component, OnInit } from '@angular/core';
//Router
import { Router, ActivatedRoute } from '@angular/router';
//Servicios
import { AutenticacionService } from '../servicios/autenticacion.service';
import { UsuarioService } from '../servicios/usuario.service';
//Material
import { MatSnackBar } from '@angular/material';
//Animaciones
import { trigger, style, animate, transition, state } from '@angular/animations';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations:[
    trigger('show', [
        transition(':enter', [
            style({ opacity: 0 }),
            animate(800, style({ opacity: 1 }))
        ]),
        transition(':leave', [
            style({ opacity: 1 }),
            animate(800, style({ opacity: 0 }))
        ])
    ])
  ],
  providers: [ AutenticacionService ]
})

export class LoginComponent implements OnInit {
  modelo: any = {};
  arregloUsuario: any[];
  cargando: boolean;
  usuario: string = null;
  estado: string = null;
  
  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private auth: AutenticacionService,
    private usuarioS: UsuarioService,
    private snackBar: MatSnackBar
  ){
    this.cargarStorage();
  }
   
  ngOnInit() {
    this.auth.cerrarSesion();
  }

  cargarStorage(){
    if(localStorage.getItem('IdUsuario')){
      this.usuario = localStorage.getItem('IdUsuario');
    }else{
      this.usuario = null;
    }
  }

  iniciarSesion(): void{
    this.cargando=true;
    this.auth.hacerLogin(this.modelo.usuario, this.modelo.password)
      .subscribe(data=>{
          this.arregloUsuario = data;
          this.estado = this.arregloUsuario[0].Estado;
          if(this.estado == 'Conectado'){
            this.usuarioS.sesionIniciada();
            this.snackBar.open('Bienvenido '+ this.arregloUsuario[0].Nombre +" "+this.arregloUsuario[0].Apellidos, null, {duration: 3000});
            localStorage.setItem('IdUsuario', this.arregloUsuario[0].IdUsuario);
           //localStorage.setItem('usuarioLogueado',JSON.stringify(data));
            this.router.navigate(['mapa']);
            this.cargando=false;
          }else{
            this.snackBar.open(this.estado,null,{ duration: 2000});   
            this.cargando=false;         
          }
        },
          error=>{
            console.log(error);
            this.snackBar.open('Error en servidor',null,{ duration: 2000});
            this.cargando=false;
          },
          ()=>{
            this.cargando=false;
          });
    }
}