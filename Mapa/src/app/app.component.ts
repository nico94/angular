import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AnonymousSubscription } from "rxjs/Subscription";
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {
  
  constructor(private router: Router){}
  
  ngOnInit(): void {
    this.estaLogueado();
  }

  estaLogueado(): boolean  {
    if(localStorage.getItem('IdUsuario')){
      this.router.navigate(['mapa']);
      return true;
    }else{
      this.router.navigate(['app-login']);
      return false;
    }
  }

  ngOnDestroy(): void{
  }
}