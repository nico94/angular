import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable()
export class AuthguardGuard implements CanActivate {

  constructor(
    public router: Router
    ){}

  canActivate() {
    if(localStorage.getItem('IdUsuario')){
      return true;
    }else{
      this.router.navigate(['app-login']);
      return false;
    }
  }
}
