import { TransaccionService } from './servicios/transaccion.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    MatButtonModule, MatMenuModule, MatToolbarModule, MatIconModule, MatSnackBarModule, MatCardModule, MatInputModule, MatGridListModule,
    MatDialogModule, MatProgressSpinnerModule, MatTabsModule, MatCheckboxModule, MatTooltipModule, MatExpansionModule
} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { environment } from '../environments/environment';
import { FirebaseService } from './servicios/firebase.service';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        HttpModule,
        HttpClientModule,
        FormsModule,
        MatButtonModule,
        MatMenuModule,
        MatToolbarModule,
        MatIconModule,
        MatSnackBarModule,
        MatCardModule,
        MatInputModule,
        MatGridListModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatTabsModule,
        MatCheckboxModule,
        MatTooltipModule,
        MatExpansionModule,
        BrowserModule,
        BrowserAnimationsModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireDatabaseModule
    ],
    providers: [
        TransaccionService,
        FirebaseService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
