import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TransaccionService {
  ip = 'http://186.10.19.170/';
  urlTrans = this.ip + '/wsEstacionamiento/Servicios/Vehiculo.svc/rest/registro';
  urlTransCod = this.ip + '/wsEstacionamiento/Servicios/Vehiculo.svc/rest/obtener_por_codigo';
  urlCalcularTarifa = this.ip + '/wsEstacionamiento/Servicios/Tarifa.svc/rest/calcular_tarifa';
  cabeceras: any;

  constructor(
    private http: HttpClient,
  ) {
    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  obtenerTransaccion(cod: string){
    let cuerpo = {
      "cod": cod
    };
    return this.http.post<any>(this.urlTransCod, cuerpo, { headers: this.cabeceras });
  }

  calcularTarifa(trans: any){
    let cuerpo = {
      "trans": {
        "id": trans.id,
        "estado": trans.estado,
        "patente": trans.patente,
        "fechaIngreso": trans.fechaIngreso,
        "horaIngreso": trans.horaIngreso,
        "fechaSalida": trans.fechaSalida,
        "horaSalida": trans.horaSalida,
        "pago": trans.pago,
        "imagen": trans.imagen
      }
    };
    return this.http.post(this.urlCalcularTarifa, cuerpo, { headers: this.cabeceras });
  }

  ingresarTransaccion(trans: any) {
    let cuerpo = {
      "trans": {
        "id": trans.id,
        "estado": trans.estado,
        "patente": trans.patente,
        "fechaIngreso": trans.fechaIngreso,
        "horaIngreso": trans.horaIngreso,
        "fechaSalida": trans.fechaSalida,
        "horaSalida": trans.horaSalida,
        "pago": trans.pago,
        "imagen": trans.imagen
      }
    };
    return this.http.post(this.urlTrans, cuerpo, { headers: this.cabeceras });
  }
}