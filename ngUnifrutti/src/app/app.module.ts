import { Modificar_peso_brutoComponent } from './pages/logistica/fruta_granel/recepcion/dialogs/modificar_peso_bruto/modificar_peso_bruto.component';
import { Nuevo_contenedorComponent } from './pages/logistica/parametros/dialogs/nuevo_contenedor/nuevo_contenedor.component';
import { Lista_parametrosComponent } from './pages/logistica/parametros/vistas/lista_parametros/lista_parametros.component';
import { Lista_contenedoresComponent } from './pages/logistica/parametros/vistas/lista_contenedores/lista_contenedores.component';
import { Subir_documentoComponent } from './shared/dialogs/subir_documento/subir_documento.component';
import { DocumentosComponent } from './pages/documentos/documentos.component';
import { Despacho_trazabilidadComponent } from './shared/dialogs/despacho_trazabilidad/despacho_trazabilidad.component';

import { DefFuncionalidadesGlobalesComponent } from './pages/seguridad/def-funcionalidades-globales/def-funcionalidades-globales.component';
import { DefFolioComponent } from './shared/dialogs/def-folio/def-folio.component';
import { Abrir_lotesComponent } from './shared/dialogs/abrir_lotes/abrir_lotes.component';
import { CkuConTablaComponent } from './pages/logistica/fruta_granel/recepcion/cku-con-tabla/cku-con-tabla.component';
import { EncabezadoComponent } from './pages/logistica/fruta_granel/recepcion/componentes_compartidos/encabezado/encabezado.component';
import { DestareComponent } from './pages/logistica/fruta_granel/recepcion/destare/destare.component';
import { Env_salidaComponent } from './pages/logistica/fruta_granel/recepcion/env_salida/env_salida.component';
import { PantallaCkuComponent } from './pages/logistica/fruta_granel/recepcion/pantalla-cku/pantalla-cku.component';
import { PesoBrutoComponent } from './shared/dialogs/peso-bruto/peso-bruto.component';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DragScrollModule } from 'ngx-drag-scroll/lib';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { MAT_DATE_LOCALE, MatRippleModule, MatAutocompleteModule,
  MatBadgeModule, MatBottomSheetModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule,
  MatChipsModule, MatStepperModule, MatDialogModule, MatDividerModule, MatGridListModule, MatListModule,
  MatMenuModule, MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatSelectModule,
  MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule,
  MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule, MatTreeModule } from '@angular/material';

  import { NavbarComponent } from './shared/navbar/navbar.component';
  import { SidenavComponent } from './shared/sidenav/sidenav.component';
  import { ConfignavComponent } from './shared/confignav/confignav.component';
  import { RolAgregarComponent } from './shared/dialogs/rol-agregar/rol-agregar.component';
  import { UsuarioAgregarComponent } from './shared/dialogs/usuario-agregar/usuario-agregar.component';
  import { FavoritosComponent } from './pages/favoritos/favoritos.component';

// Http
import { HttpClientModule } from '@angular/common/http';

import {
  BrowserAnimationsModule
} from '@angular/platform-browser/animations';

// Angular Material
import { MatIconModule, MatFormFieldModule, MatInputModule, MatButtonModule,
   MatDatepickerModule, MatNativeDateModule, MatRadioModule} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { LoginComponent } from './login/login.component';
import { ConfiUsuarioComponent } from './shared/dialogs/confi-usuario/confi-usuario.component';
import { RecFrutaGranelComponent } from './pages/logistica/fruta_granel/recepcion/rec-fruta-granel/rec-fruta-granel.component';
import { DesEnvvaciosComponent } from './pages/logistica/fruta_granel/recepcion/des-envvacios/des-envvacios.component';
import { RecCkuComponent } from './pages/logistica/fruta_granel/recepcion/rec-cku/rec-cku.component';
import { CtacteEnvcosComponent } from './pages/logistica/fruta_granel/recepcion/ctacte-envcos/ctacte-envcos.component';
import { RecTrazabilidadComponent } from './pages/logistica/fruta_granel/recepcion/rec-trazabilidad/rec-trazabilidad.component';
import { RecFrioComponent } from './pages/logistica/fruta_granel/recepcion/rec-frio/rec-frio.component';
import { RolesListadoComponent } from './pages/seguridad/roles-listado/roles-listado.component';
import { RolesCrearComponent } from './pages/seguridad/roles-crear/roles-crear.component';
import { UsuarioCrearComponent } from './pages/seguridad/usuario-crear/usuario-crear.component';
import { UsuarioListadoComponent } from './pages/seguridad/usuario-listado/usuario-listado.component';
import { AñadirPantallaComponent } from './shared/dialogs/añadir-pantalla/añadir-pantalla.component';
import { BuscardormenuComponent } from './pages/buscardormenu/buscardormenu.component';
import { Ot_relacionada_trazabilidadComponent } from './shared/dialogs/ot_relacionada_trazabilidad/ot_relacionada_trazabilidad.component';
import { Imp_folio_binsComponent } from './pages/logistica/fruta_granel/recepcion/dialogs/imp_folio_bins/imp_folio_bins.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidenavComponent,
    ConfignavComponent,
    RolAgregarComponent,
    UsuarioAgregarComponent,
    LoginComponent,
    FavoritosComponent,
    ConfiUsuarioComponent,
    PesoBrutoComponent,
    RecFrutaGranelComponent,
    DesEnvvaciosComponent,
    RecCkuComponent,
    CtacteEnvcosComponent,
    RecTrazabilidadComponent,
    RecFrioComponent,
    RolesListadoComponent,
    RolesCrearComponent,
    UsuarioCrearComponent,
    UsuarioListadoComponent,
    PantallaCkuComponent,
    Env_salidaComponent,
    DestareComponent,
    EncabezadoComponent,
    CkuConTablaComponent,
    Abrir_lotesComponent,
    DefFolioComponent,
    DefFuncionalidadesGlobalesComponent,
    AñadirPantallaComponent,
    BuscardormenuComponent,
    Ot_relacionada_trazabilidadComponent,
    Despacho_trazabilidadComponent,
    DocumentosComponent,
    Subir_documentoComponent,
    Imp_folio_binsComponent,
    Lista_contenedoresComponent,
    Lista_parametrosComponent,
    Nuevo_contenedorComponent,
    Modificar_peso_brutoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CommonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatRippleModule,
    MatExpansionModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatGridListModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    ScrollingModule,
    ReactiveFormsModule,
    DragScrollModule
  ],
  entryComponents: [
    RolAgregarComponent,
    UsuarioAgregarComponent,
    ConfiUsuarioComponent,
    PesoBrutoComponent,
    Abrir_lotesComponent,
    DefFolioComponent,
    AñadirPantallaComponent,
    Ot_relacionada_trazabilidadComponent,
    Despacho_trazabilidadComponent,
    Subir_documentoComponent,
    Imp_folio_binsComponent,
    Nuevo_contenedorComponent,
    Modificar_peso_brutoComponent
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
