import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { ApiCommonService } from '../../../services/apicommonservice.service';
import { Subscription } from 'rxjs';
import { AutenticacionService } from '../../../services/autenticacion.service';

export interface Exportadora {
  value: string;
  viewValue: string;
}
export interface Planta {
  value: string;
  viewValue: string;
}
export interface Temporada {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-confi-usuario',
  templateUrl: './confi-usuario.component.html',
  styleUrls: ['./confi-usuario.component.css']
})
export class ConfiUsuarioComponent implements OnInit {

  msgError : string;
  private _Subscription: Subscription;
  user: any[] = [];
  _usr = localStorage.getItem('User');

  exportadora: Exportadora[] = [];
  temporada: Temporada[] = [];
  planta: Planta[] = [];

    _planta: string;
    _temporada: string;
    _exportadora =  'UNI';

    _Nplanta: string;
    _Ntemporada: string;
    _Nexportadora: string;

  constructor(public dialogRef: MatDialogRef<ConfiUsuarioComponent>,
    private _router: Router,
    private apicommonService: ApiCommonService,
    private authService: AutenticacionService,
    public snackBar: MatSnackBar
    ) {
      dialogRef.disableClose = true;
     }

  ngOnInit() {
    this.loadExportadoras();
    this.loadTemporadas();
    /*this.loadPlantas();*/
  }

  onClick(): void {
    if (this._planta == undefined || this._temporada == undefined || this._exportadora == undefined)
    {
      //this.msgError = "<p>Por favor, seleccione todos los parámetros</p>";
      this.snackBar.open('Por favor, seleccione todos los parámetros', null, { duration: 800 });
      return;
    }
    console.log("datos ok");
    //guardar datos en storage
    localStorage.setItem("planta", this._planta);
    localStorage.setItem("exportadora", this._exportadora);
    localStorage.setItem("temporada", this._temporada);
    localStorage.setItem("Nplanta", this._Nplanta);
    localStorage.setItem("Nexportadora", this._Nexportadora);
    localStorage.setItem("Ntemporada", this._Ntemporada);
    localStorage.setItem("NidPlanta", this._planta);
    localStorage.setItem("NidExportadora", this._exportadora);

    console.log('Temporada Nombre:' + this._Ntemporada +' / ' + this._Nexportadora +' / ' + this._Nplanta);
    this.dialogRef.close();

  }

  close(): void {
    this.dialogRef.close();
  }

  loadExportadoras()
  {
    this.apicommonService.cargarExportadoras(this._usr).subscribe((data: any) => {
      this.exportadora = [];
      data['items'].forEach(element => {
        this.exportadora.push({
          value: element.id,
          viewValue: element.nombre
        });
      });

       var obj = JSON.parse( JSON.stringify(this.exportadora));
       var nombre = obj[0].value;
        this.loadNExportadora();
  }, (error) => {
    this.loadExportadoras();
  })}

  loadTemporadas()
  {
    console.log(" usuario : " + this._usr);
    this.apicommonService.cargarTemporadas(this._usr).subscribe((data: any) => {
      data['items'].forEach(element => {
        this.temporada.push({
          value: element.id,
          viewValue: element.nombre
        });
      });
      // console.log(JSON.stringify(this.ListMotivo));
      var obj = JSON.parse( JSON.stringify(this.temporada));
      var nombre = obj[0].value;
      this._temporada =  nombre;
      nombre = obj[0].viewValue;
      this._Ntemporada =  nombre;
      console.log(this._Ntemporada);

    }, (error) => {
      this.loadTemporadas();
  });
  }

  loadPlantas()
  {

    this.planta=[];

    this.apicommonService.cargarPlantas(this._usr, this._exportadora).subscribe((data: any) => {
      this.planta = [];
      data['items'].forEach( e => {
        this.planta.push({value: e.id, viewValue: e.nombre});
      })
    }, (error) => {
      this.loadPlantas();
      // this.planta.push({value: "", viewValue : "ERROR"})
    })
  }

  loadNExportadora()
  {
    console.log(this._exportadora);
    let result = this.exportadora.find( element => element.value == this._exportadora )

     this._Nexportadora = result.viewValue;

    this.loadPlantas();
  }

  loadNtemporada()
  {
    console.log(this._temporada);
    let result = this.temporada.find( element => element.value == this._temporada )

     this._Ntemporada = result.viewValue;

  }

  loadNPlantas()
  {
    console.log(this._planta);
    let result = this.planta.find( element => element.value == this._planta )

     this._Nplanta = result.viewValue;

  }

  logout() {
    localStorage.removeItem('Session')
    localStorage.clear();
    this.authService.change(false);
    this._router.navigate(['login']);
    this.dialogRef.close();
  }
}

