import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-def-folio',
  templateUrl: './def-folio.component.html',
  styleUrls: ['./def-folio.component.css']
})
export class DefFolioComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DefFolioComponent>) { }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }
}
