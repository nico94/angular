import { ApiseguridadService } from '../../../services/apiseguridad.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-rol-agregar',
  templateUrl: './rol-agregar.component.html',
  styleUrls: ['./rol-agregar.component.css']
})
export class RolAgregarComponent implements OnInit {
  public id: string;
  public rol: string;
  public esAgregar: boolean = true;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<RolAgregarComponent>,
    public _servSeguridad: ApiseguridadService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    if (this.data != null) {
      if (this.data.id) {
        this.id = this.data.id;
      }
      if (this.data.nombre) {
        this.rol = this.data.nombre;
      }
      this.esAgregar = false;
    } else {
      this.esAgregar = true;
    }
  }

  guardarRol() {
    if (this.id) {
      if (this.rol) {
        if (this.esAgregar) {
          this._servSeguridad.agregarRol(this.id, this.rol)
            .subscribe((res) => {
              // console.log('RESPUESTA ' + JSON.stringify(res));
              if (res.id == this.id) {
                this.snackBar.open('Rol guardado correctamente', null, { duration: 3000 });
                this.dialogRef.close();
              } else {
                this.snackBar.open('Error al guardar rol', null, { duration: 3000 });
              }
            }, (error) => {
              this.snackBar.open('Error al ingresar nuevo rol', null, { duration: 3000 });
            });
        } else {
          this._servSeguridad.editarRol(this.id, this.rol)
            .subscribe((data) => {
              // console.log('EDITADO ' + JSON.stringify(data));
              this.snackBar.open('Rol editado correctamente', null, { duration: 3000 });
              this.dialogRef.close();
            }, (error) => {
              this.snackBar.open('Error al guardar rol', null, { duration: 3000 });
            });
        }
      } else {
        this.snackBar.open('Ingrese descripción del rol', null, { duration: 3000 });
      }
    } else {
      this.snackBar.open('Ingrese código del rol', null, { duration: 3000 });
    }
  }

  close() {
    this.dialogRef.close();
  }
}
