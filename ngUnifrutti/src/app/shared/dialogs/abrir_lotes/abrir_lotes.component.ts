import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-abrir_lotes',
  templateUrl: './abrir_lotes.component.html',
  styleUrls: ['./abrir_lotes.component.css']
})
export class Abrir_lotesComponent implements OnInit {
  displayedColumns: string[] = ['1', '2', '3', '4', '5', '6'];
  dataSource: any = [];
  lineaSKU: any[] = [];
  especies: any[] = [];
  variedades: any[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<Abrir_lotesComponent>,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.lineaSKU = this.data;
    this.especies = [
      { id: '1', nombre: 'Manzana' },
      { id: '2', nombre: 'Pera' },
      { id: '3', nombre: 'Cereza' },
      { id: '4', nombre: 'Especie 4' }
    ];
    this.variedades = [
      { id: '1', nombre: 'Fuji' },
      { id: '2', nombre: 'Royal Gale' },
      { id: '3', nombre: 'Variedad prueba' }
    ];
  }

  cerrar() {
    this.dialogRef.close({ data: this.lineaSKU });
  }

  guardarLoteEditado() {
    this.snackBar.open('Lote editado correctamente', null, { duration: 1000 });
    this.cerrar();
  }
}
