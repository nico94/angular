import { Component, OnInit, Inject,ViewChild,ElementRef } from '@angular/core';
import { MatDialogRef, MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-peso-bruto',
  templateUrl: './peso-bruto.component.html',
  styleUrls: ['./peso-bruto.component.css']
})
export class PesoBrutoComponent implements OnInit {
  @ViewChild('refVariedad') refVariedad: any;
  @ViewChild('refEspecie') refEspecie: any;
  isActive: boolean;
  controldepeso: boolean = true;
  mostrarPeso = '0';
  variedades = [];
  contenedores: any = [];
  detContenedores: any = [];
  sdp: any = '';
  especie = '';
  variedad = '';
  elementoPesado = '1';
  especies = [
    { nombre : 'Cereza' , id : 1,
    variedades : [
      { nombre : 'Bing' , id : 1},
      { nombre : 'Kordia' , id : 2},
      { nombre : 'lapins' , id : 3},
    ]
  },
    { nombre : 'Manzana' , id : 2 ,
    variedades : [
      { nombre : 'Fuji' , id : 1},
      { nombre : 'Gala' , id : 2},
      { nombre : 'Granny Smith' , id : 3},
    ]},
];

  tipoContenedores = [
    { especie : 'Cereza' , id : 1 , contenedores : [
      { nombre :  'Contenedor bins plástico' , id: 1 ,
      grupo : [
        { nombre : 'Bins Plastico' , tipo: 'contenedor', id : 1 , configuracion : 1 },
        { nombre : 'Envase Cosecha', tipo: 'Env. Cosecha', id : 2 , configuracion : 24 }
      ]
    },
      { nombre :  'Contenedor base pallet' , id: 2 ,
      grupo : [
        { nombre : 'Pallet' ,  tipo: 'contenedor', id : 1},
      ]
    },
      { nombre :  'Sin contenedor, fruta a piso' , id: 3 ,
      grupo : [
        { nombre : 'Sin contenedor' ,  tipo: 'contenedor', id : 1}
      ]},
    ] }
  ];

  constructor(public dialogRef: MatDialogRef<PesoBrutoComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
                console.log(data)
                this.mostrarPeso = data.mostrarPeso;
                this.elementoPesado = data.elementoPesado === '1' ? '2' : '1';

               }

  ngOnInit() {
  }

  selEspecie( especie ) {
    console.log(especie);
    const variedad = this.especies.filter( element => {
      return element.id === especie;
    });

    let contenedor = this.tipoContenedores.filter( element => {
      return element.id === especie;
    });
    console.log(contenedor);


    if (contenedor.length > 0 ) {
      this.contenedores =  contenedor[0].contenedores;
      console.log(this.contenedores);
    } else {
      this.contenedores = [];
    }
    this.variedades = variedad[0].variedades;
    console.log(this.variedades)
  }

  selVariedad( variedad ) {
    console.log( variedad );
  }

  selContenedor( contendor ) {
    console.log(this.refVariedad.triggerValue)
    const detCont = this.contenedores.filter( element => {
      return element.id === contendor;
    });

    detCont[0].grupo.forEach(element => {
      element['check'] =  true;
      element['tara'] =  50;
      element['cantEnvasesUni'] =  20;
      element['cantEnvasesOtros'] =  0;
      element['total'] =  parseInt(element['cantEnvasesUni'], 10) + parseInt(element['cantEnvasesOtros'], 10);
      element['sdp'] =  this.sdp;
      element['especie'] =  this.especie;
      element['especieNombre'] =  this.refEspecie.triggerValue;
      element['variedad'] =  this.variedad;
      element['variedadNombre'] = this.refVariedad.triggerValue;
    });

    console.log(detCont[0].grupo);
    this.detContenedores = detCont[0].grupo;
  }

  onNoClick(): void {

    let elemento = [];
    if ( this.detContenedores.length === 0) {
      const data = {
        tara : 0,
        cantEnvasesUni : 20,
        cantEnvasesOtros : 0,
        total : 0,
        sdp : this.sdp,
        especie : this.especie,
        especieNombre : this.refEspecie.triggerValue,
        variedad : this.variedad,
        variedadNombre : this.refVariedad.triggerValue
      }
      elemento.push(data);
      this.detContenedores = elemento;
    }

    this.dialogRef.close({resultado : this.detContenedores , otros : { elementoPesado : this.elementoPesado}});
  }

  onToggleChange() {
    this.isActive = !this.isActive;
    if (this.isActive) {
      this.controldepeso = false;
    }
    else {
      this.controldepeso = true;
    }
  }

  close() {
    this.dialogRef.close();
  }
}
