import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatTableDataSource } from '@angular/material';


const ELEMENT_DATA3 = [
  {
    id: '1', fecha: '11/01/2019', cod: '1', tipo_proceso:'normal', nro: '2', tipo_frio:'01', variedad: '45'
  },
  {
    id: '2', fecha: '11/01/2019', cod: '1', tipo_proceso: 'normal', nro: '1', tipo_frio: '01', variedad: 'hw'
  },
  {
    id: '2', fecha: '11/01/2019', cod: '1', tipo_proceso: 'normal', nro: '1', tipo_frio: '01', variedad: 'hw'
  }
];

@Component({
  selector: 'app-ot_relacionada_trazabilidad',
  templateUrl: './ot_relacionada_trazabilidad.component.html',
  styleUrls: ['./ot_relacionada_trazabilidad.component.css']
})
export class Ot_relacionada_trazabilidadComponent implements OnInit {

  id: any;
  cod: any;
  fecha: any;
  tipo: string;
  nro: number;
  tipo_frio: any;
  tipo_proceso: any;
  variedad: any;

  displayedColumns3: string[] = ['id', 'fecha', 'cod', 'tipo_proceso', 'nro', 'tipo_frio', 'variedad'];
  dataSource3 = new MatTableDataSource(ELEMENT_DATA3);
  
  constructor(public dialogRef: MatDialogRef<Ot_relacionada_trazabilidadComponent>) { }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }
}
