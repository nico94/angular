import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AutenticacionService } from './services/autenticacion.service';
import { Component, ViewChild, EventEmitter } from '@angular/core';
import { SidenavComponent } from './shared/sidenav/sidenav.component';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public home: boolean;
  public config = false;
  public title = 'Unifrutti';
  public _Subscription: Subscription;
  key: string;
  exportadora: string;
  temporada: string;
  planta: string;
  esFavorito: boolean;

  @ViewChild(SidenavComponent) sidenav: SidenavComponent;

  constructor(
    private authService: AutenticacionService,
    private _router: Router,
    private snackBar: MatSnackBar
  ) {
    this._Subscription = this.authService.getEmitteedValue()
      .subscribe((item: any) => {
        console.log(JSON.stringify(item));
        this.home = item;
      });

    this.key = localStorage.getItem('Session');

    if (this.key !== 'true') {
      this._router.navigate(['login']);
    } else {
      this.authService.change(true);
    }

    this.exportadora = localStorage.getItem('Exportadora');
    this.temporada = localStorage.getItem('Temporada');
    this.planta = localStorage.getItem('Planta');

    if (this.exportadora !== '' && this.temporada !== '' && this.planta !== '') {
      this._router.navigate(['favoritos']);
    } else {
      this.authService.change(true);
    }
  }

  cerrarSesion() {
    localStorage.removeItem('Session');
    localStorage.removeItem('User');
    this.authService.change(false);
    this._router.navigate(['login']);
  }

  procesaPropagar(event) {
    this.sidenav.recibirMenu(event);
  }

  busquedaMenu(event) {
    console.log(event);
  }

  
}

