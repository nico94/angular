import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AutenticacionService {
  @Output() fire: EventEmitter<any> = new EventEmitter();
  private logueado: boolean = false;
  private apiURL_Login = '/negSeguridad/api/'
  /*http://186.10.19.170/wsJesusPons/Servicios/Usuario.svc/rest/login';*/
  private cabeceras: any;

  constructor(private http: HttpClient) {
    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  change(status: boolean) {
    // console.log('Change started');
    this.fire.emit(status);
  }

  getEmitteedValue() {
    return this.fire;
  }

  iniciarSesion(usuario: string, password: string) {
    const cuerpo = { 'id': usuario, 'password': password };
    console.log(this.apiURL_Login + '*/login')
    return this.http.post<any>(this.apiURL_Login + 'login', cuerpo, { headers: this.cabeceras });
  }
}