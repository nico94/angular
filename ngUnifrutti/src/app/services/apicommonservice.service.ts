import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class ApiCommonService {
  private apiURL_DSCommon = '/apiCommon/api/';
  private cabeceras: any;
  private subject = new Subject<any>();

  constructor(private http: HttpClient) {
    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }

  cargarExportadoras(usuario: string) {
    return this.http.get<any>(this.apiURL_DSCommon + "Exportadora/usuario/" + usuario, { headers: this.cabeceras });
  }

  cargarTemporadas(usuario: string) {
    return this.http.get<any>(this.apiURL_DSCommon + "Temporada", { headers: this.cabeceras });
  }

  cargarPlantas(usuario: string, exp: string) {
    return this.http.get<any>(this.apiURL_DSCommon + "Planta/Usuario/" + usuario + "/" + exp, { headers: this.cabeceras });
  }

  enviarMenusBusqueda(menus, buscar) {
    this.subject.next({ menus, buscar });
  }

  recibirMenusBusqueda(): Observable<any> {
    return this.subject.asObservable();
  }

  cargarTodasExportadoras() {
    return this.http.get<any>(this.apiURL_DSCommon + "Exportadora", { headers: this.cabeceras });
  }

  cargarTodasPlantas() {
    return this.http.get<any>(this.apiURL_DSCommon + "Planta/", { headers: this.cabeceras });
  }


  listarGuiaRecepcion () {
    const data ={
      "Pendientes": true,
      "medioambiente": {
        "CodTemp": "sample string 1",
        "CodPlanta": "sample string 2",
        "CodExportadora": "sample string 3",
        "CodUsuario": "sample string 4"
      }
     }
    let url ='http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/GuiaRecepcion';
    return this.http.post<any>( url, data,
    { headers: this.cabeceras } );
  }



  ///////////////////
  // APIS PARA CKU //
  ///////////////////
  cargarEspecies() {
    return this.http.get<any>(this.apiURL_DSCommon + 'Especie/', { headers: this.cabeceras });
  }

  cargarVarieades() {
    return this.http.get<any>(this.apiURL_DSCommon + 'Variedad/', { headers: this.cabeceras });
  }

  cargarEnvases() {
    return this.http.get<any>(this.apiURL_DSCommon + 'Envases/', { headers: this.cabeceras });
  }

  cargarDuchas() {
    return this.http.get<any>(this.apiURL_DSCommon + 'Duchas/', { headers: this.cabeceras });
  }

  cargarCondicionesCKU() {
    return this.http.get<any>(this.apiURL_DSCommon + 'CondicionesCKU/', { headers: this.cabeceras });
  }

  guardaLineaCKU() {

  }
}
