import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { UserInterface } from "../model/UsuarioInterface";

@Injectable({
  providedIn: 'root'
})

export class ApiseguridadService {
  private apiURL_DSSeguridad = '/apiSeguridad/api/';
  private nombreApi: string = '/apiSeguridad/api/';
  private nombreApiSeg: string = '/negSeguridad/api/';
  private apiURL_Rol: string = this.nombreApi + 'rol';
  private apiURL_Pantalla: string = this.nombreApi + 'pantalla';
  private cabeceras: any;

  constructor(private http: HttpClient) {

    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }


  private usuario: UserInterface = {
    id: "",
    nombre: "",
    password: "",
    activo: "",
    desde: "",
    hasta: ""
  };


  //////////////
  // USUARIOS //
  //////////////
  cargarListadoUsuarios(user: string) {
    return this.http.get<any>(this.apiURL_DSSeguridad + 'usuario', { headers: this.cabeceras });

  }

  AddUsuarios(id: string, nombre: string, password: string, activo: any, desde: any, hasta: any) {

    console.log('Datos a grabar: ' + id + ' - ' + nombre);
    const data = {
      id: id,
      nombre: nombre,
      password: password,
      activo: activo,
      desde: desde,
      hasta: hasta
    }
    return this.http.post<any>(this.apiURL_DSSeguridad + 'Usuario', data, { headers: this.cabeceras })
  }

  DelUsuarios(id: string) {
    console.log('Usuario borrado: ' + id);
    return this.http.delete<any>(this.apiURL_DSSeguridad + 'Usuario/' + id, { headers: this.cabeceras })
  }

  MenuUsuario(id, planta, exportadora) {
    // return this.http.get<any>(this.apiURL_DSSeguridad + `Usuario/Funcionalidades/${id}/${planta}/${exportadora}`,
    return this.http.get<any>(this.apiURL_DSSeguridad + `Usuario/Pantallas/${id}/${planta}/${exportadora}`,

    { headers: this.cabeceras });
  }
  cargarMenu(){
    return this.http.get<any>(this.apiURL_DSSeguridad + 'Menu' , { headers: this.cabeceras });
  }

  actualizarUsuario(id, data) {
    return this.http.put<any>(this.apiURL_DSSeguridad + 'Usuario/' + id, data, { headers: this.cabeceras })
  }

  ///////////
  // ROLES //
  ///////////
  obtenerRoles() {
    return this.http.get<any>(this.apiURL_Rol, { headers: this.cabeceras });
  }

  agregarRol(id: string, rol: string) {
    const cuerpo = { 'id': id, 'nombre': rol };
    return this.http.post<any>(this.apiURL_Rol, cuerpo, { headers: this.cabeceras });
  }

  editarRol(id: string, rol: string) {
    const cuerpo = { 'id': id, 'nombre': rol };
    return this.http.put<any>(this.apiURL_Rol + '/' + id, cuerpo, { headers: this.cabeceras });
  }

  eliminarRol(id: string) {
    return this.http.delete<any>(this.apiURL_Rol + '/' + id, { headers: this.cabeceras });
  }

  //////////////
  // PANTALLA //
  //////////////
  obtenerPantallas() {
    return this.http.get<any>(this.apiURL_Pantalla, { headers: this.cabeceras });
  }

  obtenerFuncionalidadesMenus(menu){
    console.log(menu);
    return this.http.get<any>( this.nombreApi + '/Pantalla/Funcionalidad?id='+ menu, { headers: this.cabeceras } );
  }

  obtenerFuncionalidadesUsuarios(usuario) {
    return this.http.get<any>( this.nombreApi + '/Usuario/Funcionalidades/' + usuario, { headers: this.cabeceras } );
  }

  agregarFuncionalidadesUsuarios(data) {
    return this.http.post<any>( this.nombreApi + 'Usuario/Funcionalidad/', data, { headers: this.cabeceras } );
  }

  copiarFuncionalidadesUsuarios(data) {
    return this.http.post<any>( this.nombreApiSeg + '/Usuario/CopiaFuncionalidades/', data, { headers: this.cabeceras } );
  }
  eliminarFuncionalidadesUsuarios(usuario, planta, exportadora, funcionalidad, pantalla) {
    return this.http.delete<any>( this.nombreApi +
      `/Usuario/Funcionalidad/${usuario}/${planta}/${exportadora}?funcionalidad=${funcionalidad}&pantalla=${pantalla}`,
      { headers: this.cabeceras } );
  }

  listaProductores() {
    return this.http.get<any>( 'http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/Productor/nombre_productor',
      { headers: this.cabeceras } );
  }


}

