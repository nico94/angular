import { Lista_contenedoresComponent } from './pages/logistica/parametros/vistas/lista_contenedores/lista_contenedores.component';
import { Lista_parametrosComponent } from './pages/logistica/parametros/vistas/lista_parametros/lista_parametros.component';
import { DocumentosComponent } from './pages/documentos/documentos.component';
import { DefFuncionalidadesGlobalesComponent } from './pages/seguridad/def-funcionalidades-globales/def-funcionalidades-globales.component';
import { CkuConTablaComponent } from './pages/logistica/fruta_granel/recepcion/cku-con-tabla/cku-con-tabla.component';
import { DestareComponent } from './pages/logistica/fruta_granel/recepcion/destare/destare.component';
import { RecFrutaGranelComponent } from './pages/logistica/fruta_granel/recepcion/rec-fruta-granel/rec-fruta-granel.component';
import { FavoritosComponent } from './pages/favoritos/favoritos.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CtacteEnvcosComponent } from './pages/logistica/fruta_granel/recepcion/ctacte-envcos/ctacte-envcos.component';
import { DesEnvvaciosComponent } from './pages/logistica/fruta_granel/recepcion/des-envvacios/des-envvacios.component';
import { RecCkuComponent } from './pages/logistica/fruta_granel/recepcion/rec-cku/rec-cku.component';
import { RecFrioComponent } from './pages/logistica/fruta_granel/recepcion/rec-frio/rec-frio.component';
import { RecTrazabilidadComponent } from './pages/logistica/fruta_granel/recepcion/rec-trazabilidad/rec-trazabilidad.component';
import { RolesListadoComponent } from './pages/seguridad/roles-listado/roles-listado.component';
import { RolesCrearComponent } from './pages/seguridad/roles-crear/roles-crear.component';
import { UsuarioCrearComponent } from './pages/seguridad/usuario-crear/usuario-crear.component';
import { UsuarioListadoComponent } from './pages/seguridad/usuario-listado/usuario-listado.component';
import { PantallaCkuComponent } from './pages/logistica/fruta_granel/recepcion/pantalla-cku/pantalla-cku.component';
import { Env_salidaComponent } from './pages/logistica/fruta_granel/recepcion/env_salida/env_salida.component';
import { BuscardormenuComponent } from './pages/buscardormenu/buscardormenu.component';

const routes: Routes = [
  {
    path: 'seguridad',
    loadChildren: 'app/pages/pages.module#PagesModule'
  },
  {
    path: 'logistica',
    loadChildren: 'app/pages/pages.module#PagesModule'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'favoritos',
    component: FavoritosComponent
  },
  {
    path: 'recfrutagranel',
    component: RecFrutaGranelComponent
  },
  {
    path: 'ctacte_envcos',
    component: CtacteEnvcosComponent
  },
  {
    path: 'Des_env_vacios',
    component: DesEnvvaciosComponent
  },
  {
    path: 'rec_cku',
    component: RecCkuComponent
  },
  {
    path: 'rec_frio',
    component: RecFrioComponent
  },
  {
    path: 'rec_trazabilidad',
    component: RecTrazabilidadComponent
  },
  {
    path: 'roleslistado',
    component: RolesListadoComponent
  },
  {
    path: 'rolescrear',
    component: RolesCrearComponent
  },
  {
    path: 'usuarioscrear',
    component: UsuarioCrearComponent
  },
  {
    path: 'usuarioslistado',
    component: UsuarioListadoComponent
  },
  {
    path: 'pantallacku',
    component: PantallaCkuComponent
  },
  {
    path: 'destare',
    component: DestareComponent
  },
  {
    path: 'env_salida',
    component: Env_salidaComponent
  },
  {
    path: 'cku-tabla',
    component: CkuConTablaComponent
  },
  {
    path: 'def-funcionalidades-globales',
    component: DefFuncionalidadesGlobalesComponent
  },
  {
    path: 'buscador',
    component: BuscardormenuComponent
  },
  {
    path: 'documentos_globales',
    component: DocumentosComponent
  },
  {
    path: 'parametros_logistica',
    component: Lista_parametrosComponent
  },
  {
    path: 'parametros_contenedores',
    component: Lista_contenedoresComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

