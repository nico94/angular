import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { Router } from '@angular/router';
import { AutenticacionService } from '../services/autenticacion.service';
import { Md5 } from 'md5-typescript';
import { Subscription } from 'rxjs';
import { MatDialog, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [
    trigger('show', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate(500, style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate(500, style({ opacity: 0 }))
      ])
    ])
  ]
})

export class LoginComponent implements OnInit {
  hide = true;
  sesion: boolean = false;
  urlLogoEmpresa: string = 'assets/img/unifrutti.png';
  urlLogoPall: string = 'assets/img/logopallb.png';
  key: string;
  Loginkey: string;
  config: string[];
  private _Subscription: Subscription;
  user: any[] = [];
  _usr = '';
  _psw = '';
  msgError: string;

  constructor(
    private _router: Router,
    private authService: AutenticacionService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
  ) {
    this.key = localStorage.getItem('Session');
    if (this.key === 'true') {
      this._router.navigate(['favoritos']);
    }
  }

  ngOnInit() { }

  openDialog() {
    /* const dialogRef = this.dialog.open(ConfiUsuarioComponent, {
       width: '250px',
      });

     dialogRef.afterClosed().subscribe(result => {
       this.NavExp = localStorage.getItem('Nexportadora');
       this.NavTemp = localStorage.getItem('Ntemporada');
       this.NavPlanta = localStorage.getItem('Nplanta');
       this.NavUsuario = localStorage.getItem('User');
       console.log('Exportadora (ts): ' + this.NavExp + '/ Temporada: ' + this.NavTemp + '/ Planta: ' + this.NavPlanta);
     });
       */
  }

  eventoEnter(event) {
    if (event.keyCode == 13) {
      this.login();
    }
  }

  login() {
    console.log('usuario ' + this._usr + ' pass ' + this._psw);
    /*    this._Subscription = this.authService.iniciarSesion(this._usr, Md5.init(this._psw))*/
    this._Subscription = this.authService.iniciarSesion(this._usr, this._psw)
      .subscribe((data: any) => {
        console.log('RESULT ' + JSON.stringify(data));
        this.user = data;
        if (this.user['valido']) {
          console.log('graba sesion');
          this.Loginkey = 'true';
          localStorage.setItem('Session', this.Loginkey);
          localStorage.setItem('User', this._usr);
          this.authService.change(true);
          /*this.user = data['InciarSesionResult'];
          if (this.user['estado']) {
            console.log('graba sesion');
            this.Loginkey = 'true';
            localStorage.setItem('Session', this.Loginkey);
            localStorage.setItem('User', this._usr);
            this.authService.change(true);
            this.openDialog();*/
        } else {
          this.snackBar.open(this.user['mensaje'], null, { duration: 3000 });
          // this.msgError = this.user['mensaje'];
        }
      }, (error) => {
        // console.log('Error login ' + JSON.stringify(error));
        // this.snackBar.open('Error en el servidor (' + error['status'] + ')', null, { duration: 3000 });
        this.snackBar.open('Usuario o Contraseña  no son validos', null, { duration: 3000 });
        // this.msgError = '<p>Error en el servidor (' + error['status'] + ')</p>';
      });
  }
}
