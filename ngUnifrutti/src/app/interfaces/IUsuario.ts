export interface IUsuario{
    id: number;
    nombre: string;
    password: string;
    activo: string;
    desde: string;
    hasta: string;
}