import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { RolesListadoComponent } from './seguridad/roles-listado/roles-listado.component';
import { RolesCrearComponent } from './seguridad/roles-crear/roles-crear.component';
import { UsuarioCrearComponent } from './seguridad/usuario-crear/usuario-crear.component';
import { UsuarioListadoComponent } from './seguridad/usuario-listado/usuario-listado.component';
import { FavoritosComponent } from './favoritos/favoritos.component';
import { DesEnvvaciosComponent } from './logistica/fruta_granel/recepcion/des-envvacios/des-envvacios.component';
import { CtacteEnvcosComponent } from './logistica/fruta_granel/recepcion/ctacte-envcos/ctacte-envcos.component';
import { RecCkuComponent } from './logistica/fruta_granel/recepcion/rec-cku/rec-cku.component';
import { RecFrioComponent } from './logistica/fruta_granel/recepcion/rec-frio/rec-frio.component';
import { RecFrutaGranelComponent } from './logistica/fruta_granel/recepcion/rec-fruta-granel/rec-fruta-granel.component';
import { RecTrazabilidadComponent } from './logistica/fruta_granel/recepcion/rec-trazabilidad/rec-trazabilidad.component';
import { BuscardormenuComponent } from './buscardormenu/buscardormenu.component';


const routes: Routes = [
  /*{
    path: '',
    redirectTo: 'palletizajetarja',
    pathMatch: 'full'
  },*/
  {
    path: 'roleslistado',
    component: RolesListadoComponent
  },
  {
    path: 'rolescrear',
    component: RolesCrearComponent
  },
  {
    path: 'usuarioscrear',
    component: UsuarioCrearComponent
  },
  {
    path: 'usuarioslistado',
    component: UsuarioListadoComponent
  },
  {
    path: 'favoritos',
    component: FavoritosComponent
  },
  {
    path: 'ctacte_envcos',
    component: CtacteEnvcosComponent
  },
  {
    path: 'Des_env_vacios',
    component: DesEnvvaciosComponent
  },
  {
    path: 'rec_cku',
    component: RecCkuComponent
  },
  {
    path: 'rec_frio',
    component: RecFrioComponent
  },
  {
    path: 'recfrutagranel',
    component: RecFrutaGranelComponent
  },
  {
    path: 'rec_trazabilidad',
    component: RecTrazabilidadComponent
  },
  {
    path: 'buscador',
    component: BuscardormenuComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PagesRoutingModule { }
