import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-modificar_peso_bruto',
  templateUrl: './modificar_peso_bruto.component.html',
  styleUrls: ['./modificar_peso_bruto.component.css']
})
export class Modificar_peso_brutoComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<Modificar_peso_brutoComponent>) { }

  ngOnInit() {
  }

   close(): void {
    this.dialogRef.close();
  }
  
}
