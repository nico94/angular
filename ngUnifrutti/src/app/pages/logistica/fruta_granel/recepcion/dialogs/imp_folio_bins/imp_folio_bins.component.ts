import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-imp_folio_bins',
  templateUrl: './imp_folio_bins.component.html',
  styleUrls: ['./imp_folio_bins.component.css']
})
export class Imp_folio_binsComponent implements OnInit {

  listaBins = [
    { codigo: '1232312' , check: false } ,
    { codigo: '1232343' , check: false } ,
    { codigo: '1232365' , check: false } ,
    { codigo: '1234234' , check: false } ,
    { codigo: '6454654' , check: false } ,
    { codigo: '3458545' , check: false } ,
    { codigo: '6875442' , check: false } ,
  ]
  constructor(public dialogRef: MatDialogRef<Imp_folio_binsComponent>) { }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }

  checkTodo(event) {

    if ( event.checked ) {
      this.listaBins.forEach( element => {
        element.check = true;
      });
    } else {
      this.listaBins.forEach( element => {
        element.check = false;
      });
    }
    console.log(this.listaBins);
  }

}
