import { Subscription } from 'rxjs';
import { Abrir_lotesComponent } from './../../../../../shared/dialogs/abrir_lotes/abrir_lotes.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatSnackBar, MatDialog, MatTableDataSource } from '@angular/material';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
const ELEMENT_DATA = [
  {
    cod_especie: '1', especie: 'Manzana', cod_variedad: '1', variedad: 'Fuji',
    cod_envase: '1', env_cosecha: 'Bins plástico', bultos: '12',
    cod_ducha: '1', cod_condicion: '2', cod_frio: '1',
    peso_neto: '4322', promedio: '465', folio_cku: '54',
    tipo_ducha: '1', tipo_frio: '1', cond_cku: '1', pre_frio: true, icons: ''
  },
  {
    cod_especie: '2', especie: 'Pera', cod_variedad: '2', variedad: 'Asiatica',
    cod_envase: '1', env_cosecha: 'Bins plástico', bultos: '12',
    cod_ducha: '2', cod_condicion: '2', cod_frio: '2',
    peso_neto: '500', promedio: '35', folio_cku: '32',
    tipo_ducha: '1', tipo_frio: '1', cond_cku: '1', pre_frio: false, icons: ''
  }
];
@Component({
  selector: 'app-cku-con-tabla',
  templateUrl: './cku-con-tabla.component.html',
  styleUrls: ['./cku-con-tabla.component.css']
})
export class CkuConTablaComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['especie', 'variedad', 'env_cosecha', 'bultos', 'peso_neto', 'promedio', 'folio_cku', 'tipo_ducha', 'tipo_frio',
    'cond_cku', 'pre_frio', 'icons'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  especie: string;
  variedad: string;
  env_cosecha: string;
  bultos: number;
  peso_neto: number;
  promedio: number;
  folio_cku: number;
  tipo_ducha: string;
  tipo_frio: string;
  cond_cku: string;
  pre_frio: string;
  icons: any;
  sub: Subscription;
  cku: any = [];
  asignarValida: boolean = false;

  ckuAsignar: any[] = [{
    cod_especie: null,
    cod_variedad: null,
    cod_envase: null,
    cod_condicion: null,
    cod_frio: null,
    cod_ducha: null,

    bultos: null,
    peso_neto: null,
    promedio: null,
    folio_cku: null,
    pre_frio: null

  }];
  //Areglos generales
  especies: any[] = [];
  variedades: any[] = [];
  envasesCosecha: any[] = [];
  condsCKU: any[] = [];
  tiposFrios: any[] = [];
  tipoDuchas: any[] = [];

  constructor(
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog,
    private route: ActivatedRoute
  ) {
    this.leerParametro();
    this.cargarDatos();
  }

  leerParametro() {
    this.sub = this.route.queryParams
      .subscribe(params => {
        this.cku = params;
      });
  }

  cargarDatos() {
    //Deberian ser de la API
    this.especies = [
      { id: '1', nombre: 'Manzana' },
      { id: '2', nombre: 'Pera' },
      { id: '3', nombre: 'Cereza' },
      { id: '4', nombre: 'Especie 4' }
    ];
    this.variedades = [
      { id: '1', nombre: 'Fuji' },
      { id: '2', nombre: 'Royal Gale' },
      { id: '3', nombre: 'Variedad prueba' }
    ];
    this.envasesCosecha = [
      { id: '1', nombre: 'Bins Plastico' },
      { id: '2', nombre: 'Totem' },
      { id: '3', nombre: '3/4' }
    ];
    this.tiposFrios = [
      { id: '1', nombre: 'Frio 1' },
      { id: '2', nombre: 'Ambiental' }
    ];
    this.tipoDuchas = [
      { id: '1', nombre: 'Ducha Fria' },
      { id: '2', nombre: 'Ducha Caliente' }
    ];
    this.condsCKU = [
      { id: '1', nombre: 'Condicion 1' },
      { id: '2', nombre: 'Condicion 2' },
      { id: '3', nombre: 'Condicion 3' },
    ];
  }

  ngOnInit() { }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  asignarCKU(item: any) {
    this.ckuAsignar = [];
    // console.log('ITEM CKU ', item);
    if (item) {
      this.asignarValida = true;
      this.ckuAsignar = item;
    } else {
      this.asignarValida = false;
      this.snackBar.open('No posee CKU', null, { duration: 2000 });
    }
  }

  cancelarLinea() {
    this.ckuAsignar = [];
    this.asignarValida = false;
  }

  actualizarLinea() {
    console.log('Update ', this.ckuAsignar);
    //Aqui consultar un POST a la api para update
    this.snackBar.open('Asignado correctamente', null, { duration: 2000 });
    this.cancelarLinea();
    this.leerParametro();
  }

  guardar() {
    this.snackBar.open(' ✔ \xa0\xa0 Datos guardados correctamente', null, { duration: 2000 });
    this.router.navigate(['/rec_cku']);
  }

  abrirLote(item: any) {
    const dialogRef = this.dialog.open(Abrir_lotesComponent, {
      width: '900px',
      data: item
    });
    dialogRef.afterClosed()
      .subscribe(data => {
        console.log('RETORNO ', data);
      });
  }
}