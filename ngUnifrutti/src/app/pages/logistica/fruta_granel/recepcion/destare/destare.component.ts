import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-destare',
  templateUrl: './destare.component.html',
  styleUrls: ['./destare.component.css']
})
export class DestareComponent implements OnInit {

  displayedColumns3: string[] = ['1', '2', '3'];
  dataSource: any = [];
  constructor(public snackBar: MatSnackBar, private router: Router) { }

  ngOnInit() {
  }

  guardar() {
    this.snackBar.open(' ✔ \xa0\xa0 Datos guardados correctamente', null, { duration: 2000 });
    this.router.navigate(['/pantallacku'])
  }
}
