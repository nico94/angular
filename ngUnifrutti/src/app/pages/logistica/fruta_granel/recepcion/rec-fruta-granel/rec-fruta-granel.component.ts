import { Modificar_peso_brutoComponent } from './../dialogs/modificar_peso_bruto/modificar_peso_bruto.component';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatSnackBar, MatDialog, MatTableDataSource } from '@angular/material';
import { PesoBrutoComponent } from '../../../../../shared/dialogs/peso-bruto/peso-bruto.component';
import { Router } from '@angular/router';
import chileanRut from 'chilean-rut';
import { element } from '@angular/core/src/render3';
const ELEMENT_DATA = [];
const ELEMENT_DATA_DESTARA = [];
const ELEMENT_DATA_ENVASES_ENTRADA =  [];
const ELEMENT_DATA_ENVASES_SALIDA =  [];
@Component({
  selector: 'app-rec-fruta-granel',
  templateUrl: './rec-fruta-granel.component.html',
  styleUrls: ['./rec-fruta-granel.component.css']
})
export class RecFrutaGranelComponent implements OnInit {
  displayedColumns: string[] = ['especie', 'variedad', 'sdp', 'pesobruto', 'cant_env', 'env', 'neto', 'promedio', 'linea', 'icons'];
  displayedColumns2: string[] = ['1', '2', '3', '4', '5', '6'];
  displayedColumns3: string[] = ['1', '2', '3', '4'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
  dataSourceEnvasesEntrada = new MatTableDataSource(ELEMENT_DATA_ENVASES_ENTRADA);
  dataSourceEnvasesSalida = new MatTableDataSource(ELEMENT_DATA_ENVASES_SALIDA);
  @ViewChild('rutInput') rutReference: ElementRef;
  @ViewChild('pesobruto') refPesoBruto: ElementRef;
  @ViewChild('cambioPeso') refCambioPeso: any;
  @ViewChild('ingresopeso') refIngresoPeso: ElementRef;
  especie: string;
  variedad: string;
  sdp: any;
  pesobruto: number;
  env: any;
  neto: any;
  promedio: any = 0;
  linea: number;
  rut: string = null;
  isLinear: boolean;
  mostrarPeso = '0';
  patenteValida: boolean = true;
  totalValida: boolean = true;
  pesoBruto: number = 0;
  pesoEnvase: number = 0;
  pesoPorEspecie: number = 0;
  checkCamion: boolean = false;
  pesoCamion: number = 0;
  pesoCarro: number = 0;
  ingresoPeso;
  elementoPesado = 0;
  totalSuma: number = 0;
  especies: any = [];
  pesoEnvaseSalida = 0;
  variedades = [];
  pesoDestare = 0;
  pesoDestarePorEspecie = 0;
  pesoBrutoSalida = 0;
  sumaTotalEnvases = 0;
  pesoPromedioFinal = 0 ;
  arrEnvasesEntrada = [];
  arrEnvasesSalida = [];
  constructor(
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    this.checkCamion = true;
  }

  saveInfo() {
    this.snackBar.open(' ✔  Ficha creada', null, { duration: 800 });
    this.router.navigate(['/pantallacku']);
  }

  agregarDestare() {
    const filtroLista: any = this.especies[this.especie].variedades.filter( result => {
      return result.variedadNombre === this.variedad;
    } );
    const data = {
      especie : this.especies[this.especie].especie ,
      variedad : filtroLista[0].variedadNombre ,
      pesoDestare: this.pesoDestarePorEspecie ,
      cantidadEnvases : filtroLista[0].totalBultos,
    };
    ELEMENT_DATA_DESTARA.push(data);
    this.dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
    this.pesoBrutoSalida  = 0;
    ELEMENT_DATA_DESTARA.forEach( element => {
      this.pesoBrutoSalida += parseFloat(element.pesoDestarePorEspecie);
    });
  }

  agregarDestareEspecie() {

    const data = {
      especie : '' ,
      variedad : '' ,
      pesoDestare: '' ,
      cantidadEnvases : this.sumaTotalEnvases,
    };
    ELEMENT_DATA_DESTARA.push(data);
    this.dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
    this.pesoBrutoSalida  = parseInt(this.pesoDestarePorEspecie.toString(), 10);
  }

  selEspecie( especie ) {
     this.variedades = this.especies[especie].variedades;
  }

  selVariedad( variedad ) {
  }


  cambioCheck(event) {
    this.ingresoPeso = '';
    setTimeout(() => {
      this.refIngresoPeso.nativeElement.focus();
    });
    if (event == 1) {
      this.checkCamion = true;
    } else if (event == 2) {
      this.checkCamion = false;
    }
  }

  cambioPeso(peso) {
    if (this.checkCamion) {
      this.pesoCamion = parseInt(peso.target.value);
    } else {
      this.pesoCarro = parseInt(peso.target.value);
    }
    this.pesoBruto = this.pesoCamion + this.pesoCarro;
    if (this.pesoBruto) {
      if (this.pesoBruto > 0) {
        this.totalValida = false;
      } else {
        this.totalValida = true;
      }
    } else {
      this.totalValida = true;
    }
  }

  formatoRut() {
    if (this.rut) {
      if (this.rut.length === 9) {
        var dv = this.rut.substring(8);
        this.rut = chileanRut.format(this.rut, dv);
        this.validarRut(this.rut);
      } else if (this.rut.length === 8) {
        var dv = this.rut.substring(7);
        this.rut = chileanRut.format(this.rut, dv);
        this.validarRut(this.rut);
      } else {
        this.snackBar.open('El largo del rut no corresponde', null, { duration: 800 })
        this.rut = "";
        this.rutReference.nativeElement.focus();
      }
    }
  }

  validarRut(rut: string) {
    if (!chileanRut.validate(rut)) {
      this.snackBar.open('Rut inválido', null, { duration: 800 })
      this.rut = "";
      this.rutReference.nativeElement.focus();
    }
  }

  nuevaLinea() {
    const dialogRef = this.dialog.open(PesoBrutoComponent, {
      disableClose: true,
      width: '600px',
      data: { mostrarPeso: this.mostrarPeso, elementoPesado: this.elementoPesado }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        this.elementoPesado = result.otros.elementoPesado;
        ELEMENT_DATA.push(result.resultado[0]);
        this.sumaTotalEnvases = 0;
        this.pesoEnvase = 0;
        const totalBins = this.totalEnvases(ELEMENT_DATA);
        ELEMENT_DATA.forEach( element => {
          element['cantEnvases'] = parseFloat(element.cantEnvasesUni) + parseFloat(element.cantEnvasesOtros)
          this.pesoEnvase = this.pesoEnvase + (element['cantEnvases'] * element.tara)
          element['pesoBruto'] = ((this.pesoBruto  /  totalBins) * element['cantEnvases']).toFixed(2);
          const calculoNeto =  element['pesoBruto'] - (element['cantEnvases'] * element.tara) ;
          element['promedio'] = calculoNeto / element['cantEnvases'];
          element['calculoNeto'] = calculoNeto;
          this.sumaTotalEnvases += element['cantEnvases'];
        });

        this.promedio = this.pesoBruto / ELEMENT_DATA.length;
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.listaDestare();
        this.pesoEnvaseSalida = this.pesoEnvase;
        this.pesoPromedioFinal = (this.pesoBruto - this.pesoEnvase) / this.sumaTotalEnvases;
        this.listarEnvasesEntrada(result);
        this.listarEnvasesSalida(result);

      });
  }

  listarEnvasesEntrada(listaEnvases) {
    listaEnvases.resultado.forEach( element => {
      element['cantEnvases'] = parseFloat(element.cantEnvasesUni) + parseFloat(element.cantEnvasesOtros);
      const index = this.verificar_listarEnvasesEntrada(element.id);

      if (index >= 0 ) {
        const nuevaSuma = this.agrupar_listaEnvaseEntrada(index, element);
        this.arrEnvasesEntrada[index].cantEnvases = nuevaSuma.uni + nuevaSuma.otro;
        this.arrEnvasesEntrada[index].cantEnvasesUni = nuevaSuma.uni;
        this.arrEnvasesEntrada[index].cantEnvasesOtros = nuevaSuma.otro;
      } else {
        this.arrEnvasesEntrada.push(element);
      }
    });
    this.dataSourceEnvasesEntrada = new MatTableDataSource(this.arrEnvasesEntrada)
  }

  verificar_listarEnvasesEntrada(id) {
    return this.arrEnvasesEntrada.findIndex( result => result.id === id);
  }

  agrupar_listaEnvaseEntrada(index, element) {
    const sumaUniffutti = this.arrEnvasesEntrada[index].cantEnvasesUni + element.cantEnvasesUni;
    const sumaOtro = this.arrEnvasesEntrada[index].cantEnvasesOtros + element.cantEnvasesOtros;
    return { uni: sumaUniffutti , otro: sumaOtro };
  }

  listarEnvasesSalida(listaEnvases) {
    listaEnvases.resultado.forEach( element => {
      element['cantEnvases'] = parseFloat(element.cantEnvasesUni) + parseFloat(element.cantEnvasesOtros);
      const index = this.verificar_listarEnvasesSalida(element.id);

      if (index >= 0 ) {
        const nuevaSuma = this.agrupar_listaEnvaseSalida(index, element);
        this.arrEnvasesSalida[index].cantEnvases = nuevaSuma.uni + nuevaSuma.otro;
        this.arrEnvasesSalida[index].cantEnvasesUni = nuevaSuma.uni;
        this.arrEnvasesSalida[index].cantEnvasesOtros = nuevaSuma.otro;
      } else {
        this.arrEnvasesSalida.push(element);
      }
    });
    this.dataSourceEnvasesSalida = new MatTableDataSource(this.arrEnvasesSalida)
  }

  verificar_listarEnvasesSalida(id) {
    return this.arrEnvasesSalida.findIndex( result => result.id === id);
  }

  agrupar_listaEnvaseSalida(index, element) {
    const sumaUniffutti = this.arrEnvasesSalida[index].cantEnvasesUni + element.cantEnvasesUni;
    const sumaOtro = this.arrEnvasesSalida[index].cantEnvasesOtros + element.cantEnvasesOtros;
    return { uni: sumaUniffutti , otro: sumaOtro };
  }


  listaDestare() {
    let data: any = [];
    const especiesRepetidad = [];
    ELEMENT_DATA.forEach( result => {
      const index = this.posicionEspecieDestare(result.especieNombre);
      if (index >= 0 ) {

         data = {
                variedadNombre : result.variedadNombre,
                totalBultos : result.total
                };
        const indexVariedad = this.posicionVariedadDestare(index, result.variedadNombre);
        if (indexVariedad < 0) {
            this.especies[index].variedades.push(data);
        }
      } else {
        especiesRepetidad.push(result.especieNombre);
        const dataVariedad = {
          variedadNombre : result.variedadNombre,
          totalBultos : result.total
          };
         data = {
                especie : result.especieNombre,
                variedades : [dataVariedad]
                };
        this.especies.push( data );
      }


    });
  }

  posicionEspecieDestare( nombre ){
    return this.especies.findIndex( elemento => elemento.especie === nombre );
  }
  posicionVariedadDestare( index,nombre ){
    return this.especies[index].variedades.findIndex( elemento => elemento.variedadNombre === nombre );
  }
  totalEnvases(data) {
    let total = 0;
    data.forEach( element => {
      total += parseFloat(element.cantEnvasesUni) + parseFloat(element.cantEnvasesOtros)
    });
    return total;
  }

  volverCku() {
    this.router.navigate(['/pantallacku']);
  }

  especievariedadOption(event) {
    if (event.value == 1) {
      this.totalValida = true;
    } else if (event.value == 0) {
      this.totalValida = false;
    }
    this.mostrarPeso = event.value;
  }

  validaPatente(patente) {
    if (patente.length > 0) {
      this.patenteValida = false;
    } else {
      this.patenteValida = true;
    }
  }

  modificarPesoBruto() {
    const dialogRef = this.dialog.open(Modificar_peso_brutoComponent, {
      width: '500px',
    });
  }
}
