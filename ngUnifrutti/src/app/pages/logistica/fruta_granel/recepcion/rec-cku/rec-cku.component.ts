import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { Router, NavigationExtras } from '@angular/router';
import { Imp_folio_binsComponent } from '../dialogs/imp_folio_bins/imp_folio_bins.component';

export interface PeriodicElement {
  planta: string;
  lote: string;
  fecha: any;
  nombreproductor: string;
  cod_productor: any;
  variedad: string;
  bandejas: any;
  totalkilos: any;
  hrllegada: any;
  icons: any;
}

const ELEMENT_DATA = [
  {
    rut: '111111111', bit_pesaje: true, csg: 'asdd', guia_sii: 'asdsa', cod_frigorifico: 'xd1234',
    fec_recepcion: '01/01/2019', fec_cosecha: '11/01/2019', fec_despacho: '23/01/2019',
    guia: '54345', cod_productor: 'Agricola Palomar LTDA.', huerto: 'Cordillera',
    guia_productor: '5435', cod_especie: 'Manzana', patente: 'VF-78-39', icons: ''
  },
  {
    rut: '222222222', bit_pesaje: false, csg: 'ASDFEE', guia_sii: '123456', cod_frigorifico: 'xd1234',
    fec_recepcion: '02/01/2019', fec_cosecha: '12/01/2019', fec_despacho: '24/01/2019',
    guia: '43534', lote: '23', fecha: '12/01/2019', cod_productor: 'Agricola Palomar LTDA.', huerto: 'Cordillera',
    guia_productor: '8075', cod_especie: 'Manzana', patente: 'VF-78-39', icons: ''
  }
];

const ELEMENT_DATA2 = [
  {
    rut: '222222222', bit_pesaje: false, csg: 'ASDFEE', guia_sii: '123456', cod_frigorifico: 'xd1234',
    fec_recepcion: '02/01/2019', fec_cosecha: '12/01/2019', fec_despacho: '24/01/2019',
    guia: '54345', cod_productor: 'Agricola Palomar LTDA.', huerto: 'Cordillera',
    guia_productor: '45345', cod_especie: 'Manzana', patente: 'VF-78-39', icons: ''
  }
];

@Component({
  selector: 'app-rec-cku',
  templateUrl: './rec-cku.component.html',
  styleUrls: ['./rec-cku.component.css']
})

export class RecCkuComponent implements OnInit {
  isActive: boolean;
  displayedColumns: string[] = ['guia', 'fecha', 'cod_productor', 'huerto',
    'guia_productor', 'cod_especie', 'patente', 'icons'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  dataSource2 = new MatTableDataSource(ELEMENT_DATA2);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  mostrarTabla: boolean = true;

  constructor(
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource2.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = 'Items por página';
  }

  imprimirFolio(): void {
    const dialogRef = this.dialog.open(Imp_folio_binsComponent, {
      width: '500px',
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  applyFilter2(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onToggleChange(event) {
    this.isActive = !this.isActive;
    if (this.isActive) {
      this.mostrarTabla = false;
    }
    else {
      this.mostrarTabla = true;
    }
  }

  ckuTabla(item: any) {
    console.log('ITEM ', item);
    let extras: NavigationExtras = {
      queryParams: {
        'rut': item['rut'],
        'cod_especie': item['cod_especie'],
        'cod_productor': item['cod_productor'],
        'cod_frigorifico': item['cod_frigorifico'],
        'csg': item['csg'],
        'fec_cosecha': item['fec_cosecha'],
        'fec_despacho': item['fec_despacho'],
        'fec_recepcion': item['fec_recepcion'],
        'guia_productor': item['guia_productor'],
        'guia_sii': item['guia_sii'],
        'huerto': item['huerto'],
        'bit_pesaje': item['bit_pesaje'],
        'icons': item['icons'],
        'patente': item['patente'],
      }
    };
    this.router.navigate(['/cku-tabla'], extras);
  }
}