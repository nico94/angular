import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-des-envvacios',
  templateUrl: './des-envvacios.component.html',
  styleUrls: ['./des-envvacios.component.css']
})
export class DesEnvvaciosComponent implements OnInit {
  displayedColumns2: string[] = ['1', '2', '3', '4', '5'];
  mostrarFecha = '0';
  check: boolean = true;
  constructor() { }

  ngOnInit() {
  }

recepcion_despacho_opciones(event) {
    if (event.value == 1) {
      this.check = true;
    } else if (event.value == 2) {
      this.check = false;
    }
    this.mostrarFecha = event.value;
  }
}
