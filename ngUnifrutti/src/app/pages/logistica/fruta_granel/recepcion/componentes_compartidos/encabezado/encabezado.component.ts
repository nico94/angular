import { Component, OnInit, ViewChild, ElementRef, Input, OnChanges } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import chileanRut from 'chilean-rut';

@Component({
  selector: 'app-encabezado',
  templateUrl: './encabezado.component.html',
  styleUrls: ['./encabezado.component.css']
})
export class EncabezadoComponent implements OnInit, OnChanges {
  @ViewChild('rutInput') rutReference: ElementRef;
  @Input() cku: string;
  rut: string = null;
  ckuEnt: any;

  constructor(
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() { }

  ngOnChanges() {
    this.ckuEnt = this.cku;
  }

  formatoRut() {
    if (this.rut) {
      if (this.rut.length === 9) {
        var dv = this.rut.substring(8);
        this.rut = chileanRut.format(this.rut, dv);
        this.validarRut(this.rut);
      } else if (this.rut.length === 8) {
        var dv = this.rut.substring(7);
        this.rut = chileanRut.format(this.rut, dv);
        this.validarRut(this.rut);
      } else {
        this.snackBar.open('El largo del rut no corresponde', null, { duration: 800 })
        this.rut = '';
        this.rutReference.nativeElement.focus();
      }
    }
  }

  validarRut(rut: string) {
    if (!chileanRut.validate(rut)) {
      this.snackBar.open('Rut inválido', null, { duration: 800 })
      this.rut = '';
      this.rutReference.nativeElement.focus();
    }
  }
}