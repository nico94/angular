import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { ApiCommonService } from '../../../../../services/apicommonservice.service';
const ELEMENT_DATA = [
  {
    guia: '54345', fecha: '11/01/2019', cod_productor: 'Agricola Palomar LTDA.', huerto: 'Cordillera',
    guia_productor: '5435', cod_especie: 'Manzana', patente: 'VF-78-39', icons: ''
  },
  {
    guia: '434234', lote: '23', fecha: '12/01/2019', cod_productor: 'Agricola Palomar LTDA.', huerto: 'Cordillera',
    guia_productor: '875', cod_especie: 'Manzana', patente: 'VF-78-39', icons: ''
  }
];

const ELEMENT_DATA2 = [
  {
    guia: '54345', fecha: '18/01/2019', cod_productor: 'Agricola Palomar LTDA.', huerto: 'Cordillera',
    guia_productor: '45345', cod_especie: 'Manzana', patente: 'VF-78-39', icons: ''
  }
];
@Component({
  selector: 'app-pantalla-cku',
  templateUrl: './pantalla-cku.component.html',
  styleUrls: ['./pantalla-cku.component.css']
})
export class PantallaCkuComponent implements OnInit {
  guia: string;
  fecha: any;
  zona: string;
  cod_productor: any;
  huerto: string;
  guia_productor: any;
  cod_especie: any;
  patente: any;
  icons: any;
  isActive: boolean;
  esFavorito: boolean;
  displayedColumns: string[] = ['guia', 'fecha', 'cod_productor', 'huerto',
    'guia_productor', 'cod_especie','patente','icons'];

  dataSource = new MatTableDataSource(ELEMENT_DATA);
  dataSource2 = new MatTableDataSource(ELEMENT_DATA2);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  mostrarTabla: boolean = true;

  constructor(private router: Router, private snackBar: MatSnackBar, public wsCommon: ApiCommonService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource2.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = 'Items por página';
    this.wsCommon.listarGuiaRecepcion().subscribe( result => {
      console.log(result);
      this.dataSource = new MatTableDataSource( result.lista );
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  applyFilter2(filterValue: string) {
    this.dataSource2.filter = filterValue.trim().toLowerCase();
  }

  onToggleChange2() {
    this.isActive = !this.isActive;
    if (this.isActive) {
      this.mostrarTabla = false;
    }
    else {
      this.mostrarTabla = true;
    }
  }
  agregar() {
    this.router.navigate(['/recfrutagranel']);
  }
  envSalida(){
    this.router.navigate(['/env_salida'])
  }
  destare() {
    this.router.navigate(['/destare'])
  }
  trazabilidad() {
    this.router.navigate(['/rec_trazabilidad'])
  }
  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open(' ✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open(' ✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }
}
