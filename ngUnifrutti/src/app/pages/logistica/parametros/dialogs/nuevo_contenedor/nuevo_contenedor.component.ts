import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatTableDataSource, MatSnackBar } from '@angular/material';
const ELEMENT_DATA = [
  {
    tipo: 'contenedor', envase: 'Bins plástico', cantidad: '1', e_tara: 'si', e_cta_cte: 'si', tara: '43'
  }
];
@Component({
  selector: 'app-nuevo_contenedor',
  templateUrl: './nuevo_contenedor.component.html',
  styleUrls: ['./nuevo_contenedor.component.css']
})
export class Nuevo_contenedorComponent implements OnInit {
  displayedColumns: string[] = ['tipo', 'envase', 'cantidad', 'e_tara', 'e_cta_cte', 'tara'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  constructor(public dialogRef: MatDialogRef<Nuevo_contenedorComponent>, private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  close(){
    this.dialogRef.close();
  }

  guardarContenedor(){
    this.snackBar.open('Contenedor guardado correctamente', null, { duration: 3000 });
    this.dialogRef.close();
  }
}
