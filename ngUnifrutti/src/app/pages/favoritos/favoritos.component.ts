import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCommonService } from '../../services/apicommonservice.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.component.html',
  styleUrls: ['./favoritos.component.css']
})
export class FavoritosComponent implements OnInit {
  menuFavoritos: any[] = []
  message: any;
  subscription: Subscription;
  menuBusqueda = [];
  buscando = false;
  constructor(
    private router: Router, public apiCommon: ApiCommonService
  ) { }

  ngOnInit() {}

  frutagranel() {
    this.router.navigate(['/pantallacku']);
  }
  cku() {
    this.router.navigate(['/rec_cku']);
  }
  frio() {
    this.router.navigate(['/rec_frio']);
  }
  roles() {
    this.router.navigate(['/roleslistado']);
  }
  usuarios() {
    this.router.navigate(['/usuarioslistado']);
  }
  envvacios() {
    this.router.navigate(['/Des_env_vacios']);
  }
  funcionalidades() {
    this.router.navigate(['/def-funcionalidades-globales']);
  }

  documentos_globales(){
    this.router.navigate(['/documentos_globales']);
  }
  
  parametros_logistica() {
    this.router.navigate(['/parametros_logistica']);
  }
}

