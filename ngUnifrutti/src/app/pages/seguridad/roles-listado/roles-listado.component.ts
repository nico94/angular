import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog, MatSnackBar, MatSort } from '@angular/material';
import { Subscription } from 'rxjs';
import { RolAgregarComponent } from '../../../shared/dialogs/rol-agregar/rol-agregar.component';
//Service
import { ApiseguridadService } from './../../../services/apiseguridad.service';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-roles-listado',
  templateUrl: './roles-listado.component.html',
  styleUrls: ['./roles-listado.component.css']
})

export class RolesListadoComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public displayedColumns: string[] = ['id', 'nombre', 'icons'];
  public dataSourceRoles = new MatTableDataSource(null);
  public sub: Subscription;
  public cargando = true;

  constructor(
    public dialog: MatDialog,
    private _servSeguridad: ApiseguridadService,
    private snackBar: MatSnackBar,
    private _router: Router
  ) { }

  ngOnInit() {
    this.cargarListaRoles();
  }

  ngAfterViewInit() {
    this.paginator.page.pipe(
      tap(() => this.cargarListaRoles)
    ).subscribe();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  agregarRol() {
    const dialogo = this.dialog.open(RolAgregarComponent, {
      width: '250px',
      height: 'auto'
    });
    dialogo.afterClosed().subscribe(res => {
      this.cargarListaRoles();
    });
  }

  eliminarRol(data) {
    // console.log('ELIMINAR ' + JSON.stringify(data));
    if (data.id) {
      this._servSeguridad.eliminarRol(data.id)
        .subscribe((data) => {
          // console.log('ELIMINAR ROL '+JSON.stringify(data));
          this.cargarListaRoles();
        }, (error) => {
          this.snackBar.open('Error al eliminar rol', null, { duration: 3000 });
        });
    }
  }

  editarRol(data) {
    // console.log('EDITAR ' + JSON.stringify(data));
    const dialogo = this.dialog.open(RolAgregarComponent, {
      width: '250px',
      height: 'auto',
      data: data
    });
    dialogo.afterClosed().subscribe(res => {
      this.cargarListaRoles();
    });
  }

  asignarFunc(data) {
    this._router.navigate(['/rolescrear'], {
      queryParams: {
        rol_id: data['id'],
        rol_des: data['nombre']
      }
    });
  }

  filtrar(filterValue: string) {
    this.dataSourceRoles.filter = filterValue.trim().toLowerCase();
  }

  cargarListaRoles() {
    this.cargando = true;
    this.sub = this._servSeguridad.obtenerRoles()
      .subscribe((data) => {
        // console.log('ROLES ' + JSON.stringify(data));
        if (data.items != null) {
          this.dataSourceRoles = new MatTableDataSource(data.items);
          this.dataSourceRoles.paginator = this.paginator;
          this.paginator._intl.itemsPerPageLabel = 'Items por página';
          this.dataSourceRoles.sort = this.sort;
        } else {
          this.snackBar.open('Sin roles cargados', null, { duration: 3000 });
        }
        this.cargando = false;
      }, (error) => {
        this.snackBar.open('Error al cargar roles', null, { duration: 3000 });
        // console.log('Error ' + JSON.stringify(error));
        this.cargando = false;
      });
  }
}
