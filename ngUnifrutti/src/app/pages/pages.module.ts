

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
// ANGULAR MATERIAL
import { FormsModule } from '@angular/forms';
import {
  MatToolbarModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatTableModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatIconModule,
  MatOptionModule,
  MatSelectModule,
  MatRadioModule,
  MatSlideToggleModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatMenuModule,
  MatSnackBarModule,
  MatCardModule,
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonToggleModule,
  MatChipsModule,
  MatStepperModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatListModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatSidenavModule,
  MatSliderModule,
  MatSortModule,
  MatTabsModule,
  MatTooltipModule,
  MatTreeModule
} from '@angular/material';

// import { RolesListadoComponent } from './seguridad/roles-listado/roles-listado.component';
// import { RolesCrearComponent } from './seguridad/roles-crear/roles-crear.component';
// import { UsuarioCrearComponent } from './seguridad/usuario-crear/usuario-crear.component';
import { UsuarioListadoComponent } from './seguridad/usuario-listado/usuario-listado.component';
import { RecFrutaGranelComponent } from './logistica/fruta_granel/recepcion/rec-fruta-granel/rec-fruta-granel.component';
import { DesEnvvaciosComponent } from './logistica/fruta_granel/recepcion/des-envvacios/des-envvacios.component';
import { RecCkuComponent } from './logistica/fruta_granel/recepcion/rec-cku/rec-cku.component';
import { CtacteEnvcosComponent } from './logistica/fruta_granel/recepcion/ctacte-envcos/ctacte-envcos.component';
import { RecTrazabilidadComponent } from './logistica/fruta_granel/recepcion/rec-trazabilidad/rec-trazabilidad.component';
import { RecFrioComponent } from './logistica/fruta_granel/recepcion/rec-frio/rec-frio.component';
import { BuscardormenuComponent } from './buscardormenu/buscardormenu.component';


@NgModule({
  // schemas: [
  //   CUSTOM_ELEMENTS_SCHEMA
  // ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    FormsModule,
    MatIconModule,
    MatOptionModule,
    MatSelectModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatMenuModule,
    MatSnackBarModule,
    MatCardModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatStepperModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatListModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatSidenavModule,
    MatSliderModule,
    MatSortModule,
    MatTabsModule,
    MatTooltipModule,
    MatTreeModule
  ],
  declarations: [
    // RolesListadoComponent,
    // RolesCrearComponent,
    // UsuarioCrearComponent,
    // UsuarioListadoComponent,
    // RecFrutaGranelComponent,
    // DesEnvvaciosComponent,
    // RecCkuComponent,
    // CtacteEnvcosComponent,
    // RecTrazabilidadComponent,
    // RecFrioComponent,
    // BuscardormenuComponent
  ]
})
export class PagesModule { }
