import { Component, OnDestroy, OnInit, AfterViewInit, ComponentRef } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-ingresoip',
  templateUrl: './ingresoip.component.html',
  styleUrls: ['./ingresoip.component.css']
})

export class IngresoipComponent implements AfterViewInit, OnInit, OnDestroy {
  public ipserver: string = null;

  constructor(
    public dialogRef: MatDialogRef<IngresoipComponent>,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.ngOnDestroy();
  }

  ngOnDestroy() { }

  guardarIP() {
    if (this.ipserver) {
      localStorage.setItem('ip', this.ipserver);
      this.cerrar();
    } else {
      this.snackBar.open('Debe ingresar una IP', null, { duration: 3000 });
    }
  }

  cerrar() {
    this.dialogRef.close();
  }
}
