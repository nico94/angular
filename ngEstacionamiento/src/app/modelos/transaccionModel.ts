export class TransaccionModel {
    constructor(
        public id: number,
        public estado: number,
        public patente: string,
        public fechaIngreso: string,
        public horaIngreso: string,
        public fechaSalida: string,
        public horaSalida: string,
        public pago: number,
        public imagen: string
    ) { }
}