import { TransaccionService } from './servicios/transaccion.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatSnackBar, MatDialog } from '@angular/material';
import { AngularFireDatabase, AngularFireAction } from 'angularfire2/database';
import * as moment from 'moment';
import * as firebase from 'firebase';
import { IngresoipComponent } from './modals/ingresoip/ingresoip.component';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

interface Trans {
    estado: number;
    patente: string;
    fechaIngreso: string;
    horaIngreso: string;
    fechaSalida: string;
    horaSalida: string;
    pago: number;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [
        TransaccionService
    ]
})

export class AppComponent implements OnInit {
    @ViewChild("codigo_input") inputCodigo: ElementRef;
    titulo: string = "Estacionamiento";
    cargando: boolean;
    estado: boolean = false;
    encontrado: boolean = false;
    mostrarCarga: boolean = false;
    codigoEscan: string;
    imagen64: any = null;
    horaActual: Date;
    tiempo: string;
    total_cobro: number;
    imagenEncontrada: boolean = false;
    transaccion: any = {
        id: null,
        estado: null,
        patente: null,
        fechaIngreso: null,
        horaIngreso: null,
        fechaSalida: null,
        horaSalida: null,
        pago: null,
        imagen: null,
    };
    data: any;

    constructor(
        private tranService: TransaccionService,
        private snackBar: MatSnackBar,
        private db: AngularFireDatabase,
        private dialog: MatDialog,
        private sanitizer: DomSanitizer
    ) {
        this.cargando = true;
        this.actualizaReloj();
    }

    ngOnInit() {
        this.inputCodigo.nativeElement.focus();
        if (!localStorage.getItem('ip')) {
            this.abrirIngresoIP();
        }
    }

    abrirIngresoIP() {
        const dialogRef = this.dialog.open(IngresoipComponent, {
            width: '300px',
            height: 'auto',
            disableClose: true
        });
        dialogRef.afterClosed()
            .subscribe(res => {

            });
    }

    actualizaReloj() {
        setInterval(() => {
            this.horaActual = new Date();
        }, 1000);
    }

    buscarRegistroFirebase(cod: string) {
        if (cod && cod.length === 13) {
            let fecha = cod.substring(0, 8);
            const sub = this.db.object('automoviles/' + fecha + '/' + cod).valueChanges()
                .subscribe(
                    data => {
                        // console.log('DATA ' + JSON.stringify(data));
                        this.transaccion = data;
                        this.transaccion.id = cod;
                        this.encontrado = true;
                        if (this.transaccion.imagen) {
                            this.imagen64 = this.sanitizer.bypassSecurityTrustResourceUrl('data:Image/*;base64,' + this.transaccion.imagen);
                        } else {
                            this.imagen64 = null;
                        }
                        if (this.transaccion.estado == 0) {
                            // console.log('ENVIO WS ' + JSON.stringify(this.transaccion));
                            this.tranService.calcularTarifa(this.transaccion)
                                .subscribe(data1 => {
                                    // console.log('DATA ' + JSON.stringify(data1));
                                    let respuesta1 = data1['CalcularTarifaResult'];
                                    if (respuesta1['id_transaccion']) {
                                        this.tiempo = respuesta1.tiempo_transcurrido;
                                        this.total_cobro = respuesta1.total_cobro;
                                    }
                                }, error => {
                                    // console.log('Error calculando ' + JSON.stringify(error));
                                    this.snackBar.open('Error al calcular tarifa', null, { duration: 3000 });
                                });
                            this.estado = false;
                        } else if (this.transaccion.estado == 1) {
                            this.snackBar.open('Este vehículo ya fue despachado', null, { duration: 3000 });
                            this.codigoEscan = '';
                            this.total_cobro = 0;
                            this.estado = true;
                            this.encontrado = false;
                            this.cargando = false;
                            this.encontrado = false;
                            this.transaccion = '';
                            this.imagen64 = '';
                            this.tiempo = '';
                            this.total_cobro = null;
                        }
                    }, error => {
                        this.snackBar.open('No encontrado', null, { duration: 3000 });
                        this.codigoEscan = '';
                        this.total_cobro = 0;
                        this.cargando = false;
                        this.encontrado = false;
                        this.transaccion = '';
                        this.imagen64 = '';
                        this.tiempo = '';
                        this.total_cobro = null;
                    },
                    () => {
                        this.snackBar.open('No encontrado', null, { duration: 3000 });
                        this.codigoEscan = '';
                        this.total_cobro = 0;
                        this.cargando = false;
                        this.encontrado = false;
                        this.transaccion = '';
                        this.imagen64 = '';
                        this.tiempo = '';
                        this.total_cobro = null;
                    });
        } else {
            this.cargando = false;
            this.encontrado = false;
            this.transaccion = '';
            this.imagen64 = '';
            this.tiempo = '';
            this.total_cobro = null;
        }

        this.inputCodigo.nativeElement.focus();
    }

    // buscaTransaccion(cod: string) {
    //     if (cod && cod.length === 13) {
    //         this.tranService.obtenerTransaccion(cod)
    //             .subscribe(data => {
    //                 let respuesta = data['ObtenerPorCodigoResult'];
    //                 this.transaccion = respuesta;
    //                 if (this.transaccion.id > 0) {
    //                     this.encontrado = true;
    //                     this.imagen64 = 'data:image/jpeg;base64,' + this.transaccion.imagen;
    //                     this.cargando = false;

    //                     this.tranService.calcularTarifa(this.transaccion)
    //                         .subscribe(data1 => {
    //                             let respuesta1 = data1['CalcularTarifaResult'];
    //                             this.tiempo = respuesta1.tiempo_transcurrido;
    //                             this.total_cobro = respuesta1.total_cobro;
    //                         }, error => {
    //                             console.log('Error calculando ' + JSON.stringify(error));
    //                             this.snackBar.open('Error al calcular tarifa', null, { duration: 3000 });
    //                         });

    //                     if (respuesta.estado == 0) {
    //                         this.estado = false;
    //                     } else {
    //                         this.snackBar.open('Este vehículo ya fue despachado', null, { duration: 3000 });
    //                         this.estado = true;
    //                         this.encontrado = false;
    //                     }
    //                 } else {
    //                     this.snackBar.open('Código no encontrado', null, { duration: 3000 });
    //                     this.cargando = false;
    //                     this.encontrado = false;
    //                     this.transaccion = "";
    //                     this.imagen64 = "";
    //                     this.tiempo = "";
    //                     this.total_cobro = null;
    //                 }
    //             }, error => {
    //                 this.encontrado = false;
    //                 this.cargando = false;
    //                 this.encontrado = false;
    //                 this.transaccion = "";
    //                 this.imagen64 = "";
    //                 this.tiempo = "";
    //                 this.total_cobro = null;
    //                 this.snackBar.open('Error al consultar al servidor', null, { duration: 3000 });
    //                 console.log('Error ' + JSON.stringify(error));
    //             });

    //     } else {
    //         this.cargando = false;
    //         this.encontrado = false;
    //         this.transaccion = "";
    //         this.imagen64 = "";
    //         this.tiempo = "";
    //         this.total_cobro = null;
    //     }
    // }

    despacharFirebase() {
        if (this.encontrado) {
            let respuesta;
            this.mostrarCarga = true;
            this.cargando = true;
            var fechaSalidaActual = moment(this.horaActual).format('DD-MM-YYYY');
            var horaSalidaActual = moment(this.horaActual).format('HH:mm:ss');
            let fecha = this.transaccion.id.substr(0, 8);
            this.db.object('automoviles/' + fecha + '/' + this.transaccion.id)
                .update({
                    estado: 1,
                    patente: this.transaccion.patente,
                    fechaIngreso: this.transaccion.fechaIngreso,
                    horaIngreso: this.transaccion.horaIngreso,
                    fechaSalida: fechaSalidaActual,
                    horaSalida: horaSalidaActual,
                    pago: this.total_cobro,
                    //imagen: this.transaccion.imagen                  
                });
            this.snackBar.open('Automóvil despachado correctamene', null, { duration: 3000 });
            this.cargando = false;
            this.encontrado = false;
            this.codigoEscan = '';
            this.transaccion = '';
            this.imagen64 = '';
            this.tiempo = '';
            this.total_cobro = null;
            this.mostrarCarga = false;
        }
        this.inputCodigo.nativeElement.focus();
    }

    // despachar() {
    //     if (this.encontrado) {
    //         this.mostrarCarga = true;
    //         this.cargando = true;
    //         var fechaSalida = moment(this.horaActual).format('DD-MM-YYYY');
    //         var horaSalida = moment(this.horaActual).format('HH:mm:ss');
    //         this.transaccion.fechaSalida = fechaSalida;
    //         this.transaccion.horaSalida = horaSalida;
    //         this.transaccion.estado = 1;
    //         this.transaccion.pago = this.total_cobro;
    //         //console.log("Transaccion "+JSON.stringify(this.transaccion));
    //         this.tranService.ingresarTransaccion(this.transaccion)
    //             .subscribe(data => {
    //                 let respuesta = data['IngresarRegistroResult'];
    //                 console.log("Respuesta: " + JSON.stringify(respuesta));
    //                 let estado = respuesta.estado_res;
    //                 if (estado) {
    //                     this.snackBar.open('Automóvil despachado correctamene', null, { duration: 3000 });
    //                     this.cargando = false;
    //                     this.encontrado = false;
    //                     this.codigoEscan = '';
    //                     this.transaccion = '';
    //                     this.imagen64 = '';
    //                     this.tiempo = '';
    //                     this.total_cobro = null;
    //                     this.mostrarCarga = false;
    //                 } else {
    //                     this.snackBar.open('Automóvil no despachado', null, { duration: 3000 });
    //                     this.mostrarCarga = false;

    //                 }
    //             }, error => {
    //                 this.snackBar.open('Error al despachar automóvil', null, { duration: 3000 });
    //                 this.mostrarCarga = false;
    //             });
    //     }
    // }
}