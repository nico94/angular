// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAZNDVz_qkknZMwPLngTS828NA-j5B6FKM',
    authDomain: 'serviciogps-202119.firebaseapp.com',
    databaseURL: 'https://serviciogps-202119.firebaseio.com',
    projectId: 'serviciogps-202119',
    storageBucket: 'serviciogps-202119.appspot.com',
    messagingSenderId: '460791084107'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
