import { MensajeService } from './../../servicios/mensaje.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
//Material
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar, MatTabChangeEvent } from '@angular/material';
//Firebase
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css'],
  providers:[
    MensajeService
  ]
})
export class MapaComponent implements OnInit {
  public ubicacion$: Observable<any>;
  ubicacion: any;
  arregloUbicacion:any[];
  titulo:string="Where's pingón?";
  lat:number;
  lng:number;
  fechaHora:string;
  token:string;
  zoom:number = 5;

  constructor(
    private db: AngularFireDatabase,
    private snackBar: MatSnackBar,
    private msjsrv: MensajeService
  ) {}

  ngOnInit() {
    this.obtenerUbicacion();
  }

  obtenerUbicacion():void{
    this.db.list<any>('ubicacion').valueChanges()
    .subscribe(data=>{
      if(data){
        this.arregloUbicacion = data;   
        this.fechaHora = this.arregloUbicacion[0];
        this.lat = parseFloat(this.arregloUbicacion[1]);
        this.lng = parseFloat(this.arregloUbicacion[2]);
        this.token = this.arregloUbicacion[3];
        this.zoom = 17;
      }else{
        this.lat = -33.45686058850875;
        this.lng = -70.6370727139157;
        this.zoom = 5;
      }
    });
  }

  gatillarEvento():void{
    this.msjsrv.enviarMensaje(this.token)
      .subscribe(
        (val)=>{
          var success;
          var failure;
          var results;
          var error;

          success = parseInt(val.success);
          failure = parseInt(val.failure);
          results = val.results;
          error = results[0].error;

          if(failure === 1){
            this.snackBar.open('Error: '+error, null, { duration: 2000 });
          }
          if(success === 1){
            this.snackBar.open('Enviado a dispositivo', null, { duration: 2000 });
          }
          console.log('Respuesta POST '+JSON.stringify(val));
      },
      response=>{
        console.log("POST call in error", response);
        this.snackBar.open('Error al enviar a server ', null, { duration: 2000 });
      }); 
  }
}