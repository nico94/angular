import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'Mapa';
  
  constructor(private router: Router){}
  
  ngOnDestroy(): void {}

  ngOnInit(): void {
    this.mostrarMapa();
  }

  mostrarMapa(){
    this.router.navigate(['mapa']);
  }
}