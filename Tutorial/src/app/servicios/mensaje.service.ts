import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MensajeService {
  url: string ="https://fcm.googleapis.com/fcm/send";

  constructor(public http: HttpClient) {}

enviarMensaje(token:string): Observable<any>{
  let body =
    {
      "data": {
        "estado": "true"
      },
      "to": token
    };

  let headers = new HttpHeaders({
    'Content-Type':'application/json',
    'Authorization':'key=AIzaSyDMvpX_R-zSeAzagYtiYNGv83YCp4JpTz4'
  });
  return this.http.post(this.url, body, {headers: headers});
 }
}