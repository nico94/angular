<?php 
 	header('Access-Control-Allow-Origin: *');  
	$serverName = "192.168.0.100"; //serverName\instanceName
	$connectionInfo = array("Database"=>"SGA_lite", "UID"=>"sa", "PWD"=>"Pall2013","CharacterSet" =>"UTF-8");
	$conn = sqlsrv_connect($serverName,$connectionInfo);

	date_default_timezone_set('America/Santiago');

	$fechahora = date("Y-d-m H:i:s");

	if($conn){
		if(isset($_GET['IdUsuario'])){
		 	$cod_usuario = $_GET['IdUsuario'];
			$query2 = "SELECT Cuartel.IdCuartel, Cuartel.Nombre AS NombreCuartel, Cuartel.NumeroHectareas, Cuartel.NumeroPlantas, Cuartel.DistanciaPlantas, Cuartel.IdPredio, Predio.Nombre AS NombrePredio, Cuartel.IdEspecie, Especie.Nombre AS NombreEspecie, Cuartel.IdVariedad, Variedad.Nombre AS NombreVariedad, Cuartel.Center AS Coords, Cuartel.Points AS Cuadrado, Usuario.Nombre, Usuario.Apellidos FROM Cuartel 
				INNER JOIN Predio ON Cuartel.IdPredio = Predio.IdPredio 
				INNER JOIN Especie ON Cuartel.IdEspecie = Especie.IdEspecie 
				INNER JOIN Variedad ON Cuartel.IdVariedad = Variedad.IdVariedad 
				INNER JOIN Usuario ON Predio.IdUsuario = Usuario.IdUsuario 
				WHERE Usuario.IdUsuario = '".$cod_usuario."'";

			$stmt = sqlsrv_query($conn,$query2);
			if($stmt === false){
				echo '[{"Estado":"Error al ejecutar Query"}]';
			}

			$json = array();
			do{
				while ($row = sqlsrv_fetch_array($stmt)) {
					if(!is_null($row)){
						$json[] = str_replace(
							array('Latitude','Longitude'),
							array('lat','lng'),
							$row['Cuadrado']);
					}			
		     	}
			} while (sqlsrv_next_result($stmt));

			sqlsrv_free_stmt($stmt);
			sqlsrv_close($conn);

			}else{
				echo '[{"Estado":"Usuario no especificado"}]';
			}
	}else{
		echo '[{"Estado":"Sin conexión a base de datos"}]';
	}
	
	$obj;
	$forma = array();

	 foreach ($json as $key => $value) {
	  	$obj = json_decode($value, true);
	 	array_push($forma, $obj);
	 }
	echo json_encode($forma,JSON_PRETTY_PRINT);
?>