<?php 
 	header('Access-Control-Allow-Origin: *');  
	$serverName = "192.168.0.100"; //serverName\instanceName
	$connectionInfo = array("Database"=>"SGA_lite", "UID"=>"sa", "PWD"=>"Pall2013","CharacterSet" =>"UTF-8");
	$conn = sqlsrv_connect($serverName,$connectionInfo);

	date_default_timezone_set('America/Santiago');

	$fechahora = date("Y-d-m H:i:s");

	if($conn){
		if(isset($_GET['IdUsuario'])){
		 	$cod_usuario = $_GET['IdUsuario'];
			$query2 = "SELECT 'MO' Tipo, sum(Total) AS Total
				FROM Gasto
				WHERE EsManoDeObra=1
				UNION 
				SELECT 'IN' Tipo, sum(Total) AS Total
				FROM Gasto
				WHERE EsInsumo=1
				UNION 
				SELECT 'MA' Tipo, sum(Total) AS Total
				FROM Gasto
				WHERE EsMaquinaria=1
				UNION 
				SELECT 'OG' Tipo, sum(Total) AS Total
				FROM Gasto
				WHERE EsOtro=1";

			$stmt = sqlsrv_query($conn,$query2);
			if($stmt === false){
				echo '[{"Estado":"Error al ejecutar Query"}]';
			}

			$json;
			do{
				while ($row = sqlsrv_fetch_array($stmt)) {
					if(!is_null($row)){
						$json[] = $row;
					}			
		     	}
			} while (sqlsrv_next_result($stmt));

			sqlsrv_free_stmt($stmt);
			sqlsrv_close($conn);

			}else{
				echo '[{"Estado":"Usuario no especificado"}]';
			}
	}else{
		echo '[{"Estado":"Sin conexión a base de datos"}]';
	}
	
	echo json_encode($json);
?>