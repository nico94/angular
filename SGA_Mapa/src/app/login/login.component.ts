import { Component, OnInit } from '@angular/core';
//Router
import { Router } from '@angular/router';
//Servicios
import { AutenticacionService } from '../servicios/autenticacion.service';
import { UsuarioService } from '../servicios/usuario.service';
//Material
import { MatSnackBar } from '@angular/material';
//Animaciones
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [
    trigger('show', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate(800, style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate(800, style({ opacity: 0 }))
      ])
    ])
  ],
  providers: [AutenticacionService]
})

export class LoginComponent implements OnInit {
  modelo: any = {};
  arregloUsuario: any[];
  cargando: boolean;
  usuario: string = null;
  estado: string = null;

  constructor(
    private router: Router,
    private auth: AutenticacionService,
    private usuarioS: UsuarioService,
    private snackBar: MatSnackBar
  ) {
    this.cargarStorage();
  }

  ngOnInit() {
    this.auth.cerrarSesion();
  }

  cargarStorage() {
    if (localStorage.getItem('IdUsuario')) {
      this.usuario = localStorage.getItem('IdUsuario');
    } else {
      this.usuario = null;
    }
  }

  iniciarSesion() {
    this.cargando = true;
    this.auth.hacerLogin(this.modelo.usuario, this.modelo.password)
      .subscribe(data => {
        this.arregloUsuario = data;
        this.estado = this.arregloUsuario[0].Estado;
        if (this.estado == 'Conectado') {
          this.usuarioS.sesionIniciada();
          let nombres = this.arregloUsuario[0].Nombre;
          let apellidos = this.arregloUsuario[0].Apellidos;
          this.snackBar.open('Bienvenido ' + nombres[0].toUpperCase() + nombres.slice(1) + " " + apellidos[0].toUpperCase() + apellidos.slice(1), null, { duration: 3000 });
          localStorage.setItem('IdUsuario', this.arregloUsuario[0].IdUsuario);
          this.router.navigate(['mapa']);
          this.cargando = false;
        } else {
          this.snackBar.open(this.estado, null, { duration: 2000 });
          this.cargando = false;
        }
      },
        error => {
          console.log(error);
          this.snackBar.open('Error en servidor', null, { duration: 2000 });
          this.cargando = false;
        },
        () => {
          this.cargando = false;
        });
  }
}