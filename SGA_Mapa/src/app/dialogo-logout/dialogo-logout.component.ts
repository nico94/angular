import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AutenticacionService } from '../servicios/autenticacion.service';

@Component({
  selector: 'app-dialogo-logout',
  templateUrl: './dialogo-logout.component.html',
  styleUrls: ['./dialogo-logout.component.css']
})
export class DialogoLogoutComponent {

  constructor(
    public auth: AutenticacionService,
    public dialogRef: MatDialogRef<DialogoLogoutComponent>) { }

  cancelar() {
    this.dialogRef.close();
  }

  cerrarSesion() {
    this.auth.cerrarSesion();
    this.dialogRef.close();
  }
}