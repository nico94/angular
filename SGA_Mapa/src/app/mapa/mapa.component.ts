import { Component, ViewChild, OnInit, NgZone } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
//Servicios
import { CoordsService } from '../../app/servicios/coordenadas.service';
import { AutenticacionService } from '../servicios/autenticacion.service';
//Material
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar, MatTabChangeEvent } from '@angular/material';
//Animaciones
import { trigger, style, animate, transition } from '@angular/animations';
//Dialogos
// import { DialogoInformacionComponent } from '../dialogo-informacion/dialogo-informacion.component';
import { DialogoLogoutComponent } from '../dialogo-logout/dialogo-logout.component';
//Modulos
import { BaseChartDirective } from 'ng2-charts/ng2-charts';

@Component({
    selector: 'app-mapa',
    templateUrl: './mapa.component.html',
    styleUrls: ['./mapa.component.css'],
    animations: [
        trigger('show', [
            transition(':enter', [
                style({ opacity: 0 }),
                animate(800, style({ opacity: 1 }))
            ]),
            transition(':leave', [
                style({ opacity: 1 }),
                animate(800, style({ opacity: 0 }))
            ])
        ])
    ],
    providers: [
        CoordsService,
        BaseChartDirective
    ]
})

export class MapaComponent implements OnInit {
    @ViewChild(BaseChartDirective) chart: BaseChartDirective;
    //Variables
    titulo: string = null;
    usuario: string;
    icono: string;
    urlDashboards: string = "www.google.cl";
    //Para mapa
    arregloCoords: any[];
    markers: any;
    lat: number = -33.45686058850875;
    lng: number = -70.6370727139157;
    zoom: number = 10;
    cuarteles = [];
    predios = [];
    //Graficos
    tit_gra1: string = null;
    gra1_labels: string[] = [];
    gra1_data: number[] = [];
    tit_gra2: string = null;
    gra2_labels: string[] = [];
    gra2_data: number[] = [];
    pieChartType: string = 'pie';
    opciones: any = {
        legend: { position: 'bottom' }
    }
    //Estilo del mapa
    estiloMapa = [{
        elementType: "labels.icon",
        stylers: [{
            visibility: "off"
        }]
    }];
    //Para dashboards
    url: any;
    urlVisor: string = 'http://186.10.19.170/DashboardMap/Visor.aspx';
    cod_cuartel: string = null;
    mostrarMapa: boolean = true;
    tabSeleccionado: string = "0";

    constructor(
        private coordsService: CoordsService,
        private auth: AutenticacionService,
        private dialog: MatDialog,
        private ngZone: NgZone,
        private snackBar: MatSnackBar,
        public domSanitizer: DomSanitizer,
    ) { }

    ngOnInit() {
        this.getCoordenadas();
    }

    getCoordenadas() {
        this.arregloCoords = [];
        this.coordsService.getCoords()
            .subscribe(res => {
                this.arregloCoords = res;
                this.titulo = "SGA Lite - Pall Pro®";
                if (this.arregloCoords.length > 0) {
                    this.lat = this.arregloCoords[0].Latitude;
                    this.lng = this.arregloCoords[0].Longitude;
                    this.getCuadrados();
                    this.getPredios();
                    this.getGastos();
                    this.getTipGastos();
                } else {
                    this.lat = -33.45686058850875;
                    this.lng = -70.6370727139157;
                    this.zoom = 5;
                    this.snackBar.open('Sin Cuarteles asociados');
                }
            },
                error => {
                    console.log('Error Coordenadas', error);
                    this.snackBar.open('Error al comunicar con el servidor al cargar coordenadas', null, { duration: 2000 });
                });
    }

    getCuadrados() {
        this.cuarteles = [];
        this.coordsService.getCuadrados()
            .subscribe(res => {
                this.cuarteles = res;
                // console.log("Cuadrados " + JSON.stringify(this.cuarteles));
            },
                error => {
                    console.log('Error cuadrados', error);
                    this.snackBar.open('Error al comunicar con el servidor al cargar cuarteles', null, { duration: 2000 });
                });
    }

    getPredios() {
        this.predios = [];
        this.coordsService.getPredios()
            .subscribe(res => {
                this.predios = res;
                // console.log("PREDIOS " + JSON.stringify(this.predios));
            },
                error => {
                    console.log('Error Predio', error);
                    this.snackBar.open('Error al comunicar con el servidor', null, { duration: 2000 });
                });
    }

    getGastos() {
        this.gra1_labels = [];
        this.gra1_data = [];
        this.coordsService.getGastos()
            .subscribe(res => {
                this.tit_gra1 = 'Gastos por predio';
                for (var i = 0, l = res.length; i < l; i++) {
                    this.gra1_labels.push(res[i].Nombre);
                    this.gra1_data.push(res[i].Gasto);
                }
            },
                error => {
                    console.log('Error Gastos', error);
                    this.snackBar.open('Error al comunicar con el servidor', null, { duration: 2000 });
                });
    }

    getTipGastos() {
        this.gra2_labels = [];
        this.gra2_data = [];
        this.coordsService.getTipGastos()
            .subscribe(res => {
                this.tit_gra2 = 'Tipos de Gastos';
                for (var i = 0, l = res.length; i < l; i++) {
                    this.gra2_labels.push(res[i].Tipo);
                    this.gra2_data.push(res[i].Total);
                }
            },
                error => {
                    console.log('Error Tipo Gasto', error);
                    this.snackBar.open('Error al comunicar con el servidor', null, { duration: 2000 });
                });
    }

    // public graficoClick(e: any) {
    //   if (e.active.length > 0) {
    //     const chart = e.active[0]._chart;
    //     const activePoints = chart.getElementAtEvent(e.event);
    //     if (activePoints.length > 0) {
    //       const indice = activePoints[0]._index;
    //       const etiqueta = chart.data.labels[indice];
    //       let valor = chart.data.datasets[0].data[indice];
    //       console.log(indice, etiqueta, valor);
    //     }
    //   }
    // }

    actualizar() {
        this.getCoordenadas();
    }

    obtenerDashboards() {
        window.open("http://186.10.19.170/PallGroup/Dashboard/Home.aspx", "_blank");
    }

    cerrarSesion() {
        this.auth.cerrarSesion();
    }

    mostrarDashboard(cod: string) {
        if (this.mostrarMapa == true) {
            this.cod_cuartel = cod;
            let usuario = localStorage.getItem('IdUsuario');
            if (this.cod_cuartel && this.tabSeleccionado) {
                this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(this.urlVisor + '?id=' + this.cod_cuartel + '&usuario=' + usuario + '&index=' + this.tabSeleccionado);
            }
            this.mostrarMapa = false;
        } else {
            this.tabSeleccionado = "0";
            this.mostrarMapa = true;
        }
    }

    tabsCambio(event: MatTabChangeEvent) {
        this.tabSeleccionado = event.index.toString();
        let usuario = localStorage.getItem('IdUsuario');
        console.log('Usuario log: ' + usuario);
        if (this.cod_cuartel && this.tabSeleccionado) {
            this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(this.urlVisor + '?id=' + this.cod_cuartel + '&usuario=' + usuario + '&index=' + this.tabSeleccionado);
        }
    }

    dialogoLogOut() {
        this.ngZone.run(() => {
            let dialogRef = this.dialog.open(DialogoLogoutComponent, {
                width: '250px'
            });
        });
    }

    // abrirDialogo(cod: string): void {
    //   this.ngZone.run(()=>{
    //     this.dialogRef = this.dialog.open(DialogoInformacionComponent, {
    //       disableClose : false,
    //       height: '99%',
    //       width: '98%',        
    //       data: { 'id': cod }
    //     });
    //   });
    //   this.dialogRef.componentInstance.dialogRef= this.dialogRef;
    //   this.dialogRef.beforeClose().subscribe((result: string) => {
    //   });
    //   this.dialogRef.afterClosed().subscribe(res=>{
    //     this.dialogRef = null;
    //   });
    // } 
}