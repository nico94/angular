import { UsuarioService } from './servicios/usuario.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[
    UsuarioService
  ]
})
export class AppComponent implements OnInit {
  title = 'app';
  nombre:string;
  ap_paterno:string;
  ap_materno:string;
  
  constructor(private usuService:UsuarioService){}
  
  ngOnInit(): void {
    this.iniciarSesion();
  }
  

  iniciarSesion(){
    this.usuService.iniciarSesion()
    .subscribe(
      (val)=>{
        let usuario = val['InciarSesionResult'];
        this.nombre = usuario['nombres'];
        this.ap_paterno = usuario['ap_paterno'];
        this.ap_materno = usuario['ap_materno'];
        console.log(JSON.stringify(val));
      },
      response=>{
        console.log('Error en peticion POST',response);
      });
  }
}