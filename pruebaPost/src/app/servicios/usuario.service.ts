import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  url: string = "http://186.10.19.170/wsJesusPons/Servicios/Usuario.svc/rest/login";

  constructor(public http: HttpClient) {}

  iniciarSesion(): Observable<any>{
    let body =
      {
        "usuario":
          {
            "IdUsuario": "jefes",
            "PswUsuario": "21232F297A57A5A743894A0E4A801FC3"
          }
      };

    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.url, body, { headers: headers });
  }
}
