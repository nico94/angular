import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuServiceService {

  menu: any[] = [{
    nombre: null, path: null, icon: null,
    menus: [{
      nombre: null, path: null, icon: null,
      sub1: [{
        nombre: null, path: null, icon: null,
        sub2: [{
          nombre: null, path: null, icon: null,
        }]
      }]
    }]
  }];

  constructor() { }

  cargarMenus(): any {
    this.menu = [];
    return this.menu;
  }

}
