import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { RUTA_WS } from '../config/webService';
import { IZPLPalletizaje } from '../interfaces/IZPLPalletizaje';

@Injectable({
  providedIn: 'root'
})
export class WsPalletizajeService {

  cabeceras: any;

  ZPL: IZPLPalletizaje[] = [];

  constructor( public http: HttpClient ) {
    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json'
    });
   }

  getLista() {

    const cuerpo = {
      'ListadoPalletTarja': ''
    };

    return this.http.post<any>(RUTA_WS + '/Servicios/Repalletizaje.svc/rest/ListadoPalletTarja', cuerpo, { headers: this.cabeceras });
  }

  sendZPL( ZPL, StringZPL: string, ip: string ) {
    // console.log('ZPL: ' + StringZPL);
    // console.log( ZPL[0].idpallet);

    // console.log(ip);

    let myStr = StringZPL;

    myStr = myStr
    .replace('#calibrerotu#', ZPL[0].calibrerotu)
    .replace('#especie#', ZPL[0].especie)
    .replace('#variedadrotu#', ZPL[0].variedadrotu)
    .replace('#catrotu#', ZPL[0].catrotu)
    .replace('#idpallet#', ZPL[0].idpallet)
    .replace('#idpallet#', ZPL[0].idpallet)
    .replace('#numcajaspallet#', ZPL[0].numcajaspallet);


    // console.log('ZPL 2: ' + myStr);
    // StringZPL.replace('#calibrerotu#', ZPL[0].calibrerotu);
    // .replace('#especie#', ZPL[0].especie)
    // .replace('#variedadrotu#', ZPL[0].variedadrotu)
    // .replace('#catrotu#', ZPL[0].catrotu)
    // .replace('#numcajaspallet#', ZPL[0].numcajaspallet)
    // .replace('#idpallet#', ZPL[0].idpallet);
    // console.log('ZPL: ' + StringZPL);

    // console.log(ip);

    // ip = '192.168.0.182';

    const cuerpo = {
      'Imprimir_Zplrequest':
      {
        'Ip': ip,
        'Puerto': '9100',
        'zpl': myStr
      }
    };
    return this.http.post<any>(RUTA_WS + '/Servicios/Repalletizaje.svc/rest/Imprimir_Zpl', cuerpo, { headers: this.cabeceras });
  }

  getZPL( codZPL: string ) {
    const cuerpo = {
      'CodigoZPLrequest':
      {
        'id_etiquetaZPL': codZPL
      }
    };

    return this.http.post<any>(RUTA_WS + '/Servicios/Repalletizaje.svc/rest/CodigoZPL', cuerpo, { headers: this.cabeceras });
  }

  ZPLNuevo( folio: string ) {
    const cuerpo = {
      'palletventanarequest':
      {
       'folio': folio
      }
    };

    return this.http.post<any>(RUTA_WS + '/Servicios/Repalletizaje.svc/rest/PalletVentana', cuerpo, { headers: this.cabeceras });
  }


}
