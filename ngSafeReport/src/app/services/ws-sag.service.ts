import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { RUTA_WS } from '../config/webService';

@Injectable({
  providedIn: 'root'
})
export class WsSagService {

  cabeceras: any;

  constructor( public http: HttpClient ) {
    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json'
    });
   }

  getLista() {

    const cuerpo = { };

    return this.http.post<any>(RUTA_WS + '/Servicios/Repalletizaje.svc/rest/ListidoInspeccionSag', cuerpo, { headers: this.cabeceras });
  }
}
