import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { RUTA_WS } from '../config/webService';

@Injectable({
  providedIn: 'root'
})
export class WsNegocioService {

  cabeceras: any;

  urlLista = '/Servicios/Repalletizaje.svc/rest/ListadoNegocio';

  constructor( public http: HttpClient ) {
    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  getListaNegocio( nro_neg: number ) {
    const cuerpo = {
      'ListadoNegociorequest': {
        'nro_neg': nro_neg
      }
    };

    return this.http.post<any>(RUTA_WS + this.urlLista, cuerpo, { headers: this.cabeceras });
  }

}
