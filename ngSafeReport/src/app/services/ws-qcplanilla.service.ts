import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { RUTA_WS } from '../config/webService';
import { EncMuestraGranModel } from '../model/EncMuestraGran';
import { COD_PLANTA, COD_TEMPORADA } from '../config/configuracion';

@Injectable({
  providedIn: 'root'
})
export class WsQCPlanillaService {

  cabeceras: any;

  constructor( public http: HttpClient ) {
    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  getListaQC( pendientes: boolean) {
    // true = pendientes (nuevo)
    // false = realizadas (editar)
    const cuerpo = {
      'ListadoQcrequest':
      {
        'cod_temp': '005',
        'cod_planta': '00',
        'Condicion': pendientes
      }
    };
    return this.http.post<any>(RUTA_WS + '/Servicios/RecepcionGranel.svc/rest/ListadoQc', cuerpo, { headers: this.cabeceras });
  }



  nuevoRecepcionCerezas( Enc_MuestraGran: EncMuestraGranModel ) {
    const cuerpo = {
      'GuardarEncMuestraGranrequest':
      {
        Enc_MuestraGran
      }
    };
    return this.http.post<any>(RUTA_WS + '/Servicios/RecepcionGranel.svc/rest/GuardarEncMuestraGran', cuerpo, { headers: this.cabeceras });
  }



  // MuestraGran
  listadoEnc_MuestraGran( Num_Muestra: number ) {
    const cuerpo = {
      'DatosEncabezadoMuestrarequest':
      {
        'cod_temp': COD_TEMPORADA,
        'cod_planta': COD_PLANTA,
        'Num_Muestra': Num_Muestra
      }
    };
    return this.http.post<any>(RUTA_WS + '/Servicios/RecepcionGranel.svc/rest/DatosEncabezadoMuestra', cuerpo, { headers: this.cabeceras });
  }
  actualizarEnc_MuestraGran( Num_Muestra: number, Glosa: string,
                             TotDef: number, PorcExp: number,
                             aptitudguarda: number, Segregacion: number) {
    const cuerpo = {
      'ActualizarEncabezadoMuestrarequest':
      {
          'cod_planta': COD_PLANTA,
          'cod_temp': COD_TEMPORADA,
          'Num_Muestra': Num_Muestra,
          'Glosa': Glosa,
          'Total_Defecto': TotDef,
          'PorExport': PorcExp,
          'cod_aptitudguarda': aptitudguarda,
          'Id_Segregacion': Segregacion
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ActualizarEncabezadoMuestra', cuerpo, { headers: this.cabeceras });
  }
  // .MuestraGran

  // TipoProceso
  listadoTipoProceso( codTP: string) {
    const cuerpo = {
      'ListadoTipoProcesorequest':
      {
        'COD_TPROC': codTP
      }
     };
    return this.http.post<any>(RUTA_WS + '/Servicios/Repalletizaje.svc/rest/ListadoTipoProceso', cuerpo, { headers: this.cabeceras });
  }
  // .TipoProceso

  // Aptitud Guarda
  listadoAptitudGuarda( codAG: string) {
    const cuerpo = {
      'ListadoAptitudGuardarequest':
      {
        'cod_aptitudguarda': codAG
      }
      };
    return this.http.post<any>(RUTA_WS + '/Servicios/Repalletizaje.svc/rest/ListadoAptitudGuarda', cuerpo, { headers: this.cabeceras });
  }
  // .Aptitud Guarda

  // Segregacion
  listadoSegregacion( codS: string) {
    const cuerpo = {
      'Listadosegregacionrequest':
      {
        'Id_Segregacion': codS
      }
     };
    return this.http.post<any>(RUTA_WS + '/Servicios/Repalletizaje.svc/rest/Listadosegregacion', cuerpo, { headers: this.cabeceras });
  }
  // .Segregacion

  // CondicionTransporte
  listadoCondicionTransporte( codCT: string ) {
    // tslint:disable-next-line:radix
    const codM = parseInt(codCT);
    const cuerpo = {
      'ListadoCondicionTransporterequest':
      {
        'cod_planta' : COD_PLANTA,
        'cod_temp': COD_TEMPORADA,
        'Num_Muestra': codM
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ListadoCondicionTransporte', cuerpo, { headers: this.cabeceras });
  }
  actualizarCondicionTransporte( data: any[], Ok: boolean ) {
    const cuerpo = {
      'ActualizarCondicionTransporterequest':
      {
          'cod_planta': data['cod_planta'],
          'cod_temp': data['cod_temp'],
          'Num_Muestra': data['num_muestra'],
          'codigo_condicion': data['codigo_condicion'],
          'Ok': Ok
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ActualizarCondicionTransporte', cuerpo, { headers: this.cabeceras });
  }
  // .CondicionTransporte

  // EvaluacionGeneral
  ListadoEvaluacionGeneral( numMuestra: number ) {
    const cuerpo = {
      'ListadoEvaluacionGeneralrequest':
      {
          'cod_planta': COD_PLANTA,
          'cod_temp': COD_TEMPORADA,
          'Num_Muestra': numMuestra
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ListadoEvaluacionGeneral', cuerpo, { headers: this.cabeceras });
  }
  actualizarEvaluacionGeneral( data: any[], NewValue: number, Porc: number ) {
    const cuerpo = {
      'ActualizarEvaluacionGeneralrequest':
      {
          'cod_planta': data['cod_planta'],
          'cod_temp': data['cod_temp'],
          'Num_Muestra': data['num_muestra'],
          'cod_destin': data['cod_destin'],
          'cantidad': NewValue,
          'porc': Porc
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ActualizarEvaluacionGeneral', cuerpo, { headers: this.cabeceras });
  }
  // .EvaluacionGeneral

  // DistribucionCalibre
  ListadoDistribucionCalibre( numMuestra: number ) {
    const cuerpo = {
      'ListadoDistribucionCalibrerequest':
      {
          'cod_planta': COD_PLANTA,
          'cod_temp': COD_TEMPORADA,
          'Num_Muestra': numMuestra
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ListadoDistribucionCalibre', cuerpo, { headers: this.cabeceras });
  }
  actualizarDistribucionCalibre( data: any[], NewValue: number, Porc: number ) {
    const cuerpo = {
      'ActualizarDistribucionCalibrerequest':
      {
          'cod_planta': data['cod_planta'],
          'cod_temp': data['cod_temp'],
          'Num_Muestra': data['num_muestra'],
          'pk_serie': data['pk_serie'],
          'cantidad': NewValue,
          'porc': Porc
      }
    };
    // console.log('ENVIO ' + JSON.stringify(cuerpo));
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ActualizarDistribucionCalibre', cuerpo, { headers: this.cabeceras });
  }
  // .DistribucionCalibre

  // EvaluacionColor
  ListadoEvaluacionColor( numMuestra: number ) {
    const cuerpo = {
      'ListadoEvaluacionColorrequest':
      {
          'cod_planta': COD_PLANTA,
          'cod_temp': COD_TEMPORADA,
          'Num_Muestra': numMuestra
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ListadoEvaluacionColor', cuerpo, { headers: this.cabeceras });
  }
  actualizarEvaluacionColor( data: any[], NewValue: number, Porc: number ) {
    const cuerpo = {
      'ActualizarEvaluacionColorrequest':
      {
        'cod_planta': data['cod_planta'],
        'cod_temp': data['cod_temp'],
        'Num_Muestra': data['num_muestra'],
        'cod_eva_color': data['cod_eva_color'],
        'cantidad': NewValue,
        'porc': Porc
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ActualizarEvaluacionColor', cuerpo, { headers: this.cabeceras });
  }
  // .EvaluacionColor

  // SolidosSolubles
  ListadoMuestraSolidosSolubles( numMuestra: number ) {
    const cuerpo = {
      'ListadoMuestraSolidosSolublesrequest':
      {
          'cod_planta': COD_PLANTA,
          'cod_temp': COD_TEMPORADA,
          'Num_Muestra': numMuestra
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ListadoMuestraSolidosSolubles', cuerpo, { headers: this.cabeceras });
  }
  actualizarMuestraSolidosSolubles( data: any[], NewValue: number, duroFel: number ) {
    const cuerpo = {
      'ActualizarSolidosSolublesrequest':
      {
        'cod_planta': data['cod_planta'],
        'cod_temp': data['cod_temp'],
        'Num_Muestra': data['num_muestra'],
        'id_durofel': duroFel,
        'cod_eva_color': data['cod_eva_color'],
        'cantidad': NewValue
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ActualizarSolidosSolubles', cuerpo, { headers: this.cabeceras });
  }
  // .SolidosSolubles

  // DefectosCalidad
  ListadoDefectosCalidad( numMuestra: number, Categoria: string ) {
    const cuerpo = {
      'ListadoMuestraDefectosrequest':
      {
          'cod_planta': COD_PLANTA,
          'cod_temp': COD_TEMPORADA,
          'Num_Muestra': numMuestra,
          'Categoria': 'CA'
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ListadoMuestraDefectos', cuerpo, { headers: this.cabeceras });
  }
  actualizarDefectosCalidad( data: any[], NewValue: number, porc: number ) {
    const cuerpo = {
      'ActualizarDefectosrequest':
      {
        'cod_planta': data['cod_planta'],
        'cod_temp': data['cod_temp'],
        'Num_Muestra': data['num_muestra'],
        'cod_punto': data['cod_punto'],
        'cod_control': data['cod_control'],
        'cantidad': NewValue,
        'porc': porc
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ActualizarDefectos', cuerpo, { headers: this.cabeceras });
  }
  // .DefectosCalidad

  // DefectosCondicion
  ListadoDefectosCondicion( numMuestra: number, Categoria: string ) {
    const cuerpo = {
      'ListadoMuestraDefectosrequest':
      {
          'cod_planta': COD_PLANTA,
          'cod_temp': COD_TEMPORADA,
          'Num_Muestra': numMuestra,
          'Categoria': 'CO'
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ListadoMuestraDefectos', cuerpo, { headers: this.cabeceras });
  }
  actualizarDefectosCondicion( data: any[], NewValue: number, porc: number ) {
    const cuerpo = {
      'ActualizarDefectosrequest':
      {
        'cod_planta': data['cod_planta'],
        'cod_temp': data['cod_temp'],
        'Num_Muestra': data['num_muestra'],
        'cod_punto': data['cod_punto'],
        'cod_control': data['cod_control'],
        'cantidad': NewValue,
        'porc': porc
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ActualizarDefectos', cuerpo, { headers: this.cabeceras });
  }
  // .DefectosCondicion

  // Firmeza
  ListadoFirmeza( numMuestra: number ) {
    const cuerpo = {
      'ListadoFirmezarequest':
      {
          'cod_planta': COD_PLANTA,
          'cod_temp': COD_TEMPORADA,
          'Num_Muestra': numMuestra
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ListadoFirmeza', cuerpo, { headers: this.cabeceras });
  }
  actualizarFirmeza( data: any[], NewValue: number ) {
    const cuerpo = {
      'ActualizarFirmezaequest':
      {
        'cod_planta': data['cod_planta'],
        'cod_temp': data['cod_temp'],
        'Num_Muestra': data['num_muestra'],
        'pk_serieNro': data['pk_serieNro'],
        'cantidad': NewValue
      }
    };
    return this.http.post<any>(RUTA_WS +
      '/Servicios/RecepcionGranel.svc/rest/ActualizarFirmeza', cuerpo, { headers: this.cabeceras });
  }
  // .Firmeza

}
