

export class EncMuestraGranModel {
  constructor(
    public num_lote: number,
    public cod_planta: string,
    public cod_temp: string,
    public num_planilla: number,
    public num_muestra: number,
    public fecha: string,
    public can_muestra: number,
    public glosa: string,
    public cod_per: string,
    public cod_aptitudguarda: number,
    public  Id_Segregacion: number,
    public Total_Defecto: number,
    public PorExport: number,
    public tem_pulpa: number,
    public COD_TPROC: string
  ) {}
}
