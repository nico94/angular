
export interface IPalletizajeTarja {
  Folio: string;
  Productor: string;
  Variedad: string;
  Fecha: string;
  embalaje: string;
  calibre: string;
  asCondicion: string;
  Tcajas: number;
  Completo: number;
  codcat: number;
  codesp: number;
  codeti: string;
  des: string;
  descat: string;
  desenv: string;
  desesp: string;
  numproc: number;
}
