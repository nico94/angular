
export interface IZPLPalletizaje {
  calibrerotu: string;
  especie: string;
  variedadrotu: string;
  catrotu: string;
  numcajaspallet: string;
  idpallet: string;
}

