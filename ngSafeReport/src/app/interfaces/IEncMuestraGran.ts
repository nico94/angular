

export interface IEncMuestraGran {
  num_lote: number;
  cod_planta: string;
  cod_temp: string;
  num_planilla: number;
  num_muestra: number;
  fecha: string;
  can_muestra: number;
  glosa: string;
  cod_per: string;
  cod_aptitudguarda: number;
  Total_Defecto: number;
  PorExport: number;
}
