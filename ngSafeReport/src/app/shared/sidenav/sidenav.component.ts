import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styles: []
})
export class SidenavComponent implements OnInit {

  menuItem: any[] = [];
  titulo = 'Bienvenido/a';

  constructor(
    private router: Router) {
  }

  ngOnInit() {
  }


  favoritos() {
    this.router.navigate(['favoritos']);
  }

  abrirPantalla(item: any) {
    this.router.navigate([item.path]);
  }


  logout() {
    localStorage.removeItem('sesion');
    this.router.navigate(['login']);
  }

}
