import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuServiceService } from '../../services/menu-service.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {

  menuItem: any[] = [];
  titulo = 'Bienvenido/a';
  urlLogoEmpresa = 'assets/img/unifrutti.png';
  urlLogoPall = 'assets/img/logopallb.png';
  menuSuperior: any[] = [];

  constructor(
    private router: Router,
    private menuService: MenuServiceService
  ) {
    this.cargarMenu();
  }

  ngOnInit() {
    this.menuItem = [];
  }

  onNavigate() {
    window.open('http://www.unifrutti.com/', '_blank');
  }

  verificarRuta(menu: any) {
    this.menuItem = [];
    console.log(JSON.stringify(menu));
    if (menu) {
      this.router.navigate([menu.path]).then((e) => {
        this.menuItem = menu.menus;
        this.titulo = menu.nombre;
      });
      // this.menuService.navegarRuta(menu);
      // this.router.navigate([menu.path]);
    }
  }

  cargarMenu() {
    this.menuSuperior = this.menuService.cargarMenus();
  }

  abrirPantalla(item: any) {
    this.router.navigate([item.path]);
  }


  logout() {
    localStorage.removeItem('sesion');
    this.router.navigate(['login']);
  }

}
