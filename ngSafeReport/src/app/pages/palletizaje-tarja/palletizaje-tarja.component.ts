import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { RUTA_REPORTE, COD_ZPL } from './../../config/configuracion';

// Servicios
import { WsPalletizajeService } from './../../services/ws-palletizaje.service';
import { WsImpresoraService } from './../../services/ws-impresora.service';

// Interfaces
import { IPalletizajeTarja } from './../../interfaces/IPalletizajeTarja';
import { IZPLPalletizaje } from './../../interfaces/IZPLPalletizaje';

// Angular Material
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-palletizaje-tarja',
  templateUrl: './palletizaje-tarja.component.html',
  styles: []
})
export class PalletizajeTarjaComponent implements OnInit, OnDestroy {

  codReport: number;
  numFolio: string;
  IP = '0';

  p = 1;

  StringZPL: string;

  private _Subscription: Subscription;

  DataSourceLista: IPalletizajeTarja[] = [];
  DataSourceCompleto: any[] = [];
  ZPL: IZPLPalletizaje[] = [];
  ListaImpresoras: any[] = [];

  // Filter
  public searchString: string;

  // Validaciones
  errorWS: boolean;
  textoWebService: string;
  cargando: boolean;

  constructor(
    private wsPalletizaje: WsPalletizajeService,
    private wsImpresora: WsImpresoraService,
    public snackBar: MatSnackBar ) {

    this.cargarLista();
    this.cargarImpresoras();
  }

  key = 'name';
  reverse = false;
  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this._Subscription.unsubscribe();
  }

  setCod( cod: number, folio: string) {
    this.codReport = cod;
    this.numFolio = folio;
  }

  showReport(download: number, format: string) {

    switch (this.codReport) {

      case 1:
        // window.location.href = RUTA_REPORTE + 'ReportPalletizajeTarja?reporte=' + this.codReport + '&download='
        //   + download + '&formato=' + format + '&numFolio=' + this.numFolio;
        window.open(RUTA_REPORTE + 'ReportPalletizajeTarja?reporte=' + this.codReport + '&download='
          + download + '&formato=' + format + '&numFolio=' + this.numFolio, '_blank');
        break;


      default:
        break;

    }

  }// .showReport

  cargarLista() {

    this.cargando = true;

    this._Subscription = this.wsPalletizaje.getLista()
    .subscribe((dato: any) => {

      // console.log(dato['ListadoPalletTarjaResult']);
      this.cargando = false;

      if (dato['ListadoPalletTarjaResult']['realizado']) {
        this.DataSourceLista = dato['ListadoPalletTarjaResult']['lista'];
        if (this.DataSourceLista.length === 0) {
          this.textoWebService = 'Sin datos para mostrar';
        }
        this.errorWS = false;
      } else {
        this.errorWS = true;
        this.textoWebService = 'Error con el Web Service';
      }

    }, (error) => {
      this.cargando = false;
    });

  }

  ZPLnuevo( folio: string ) {

    if (this.IP === '0') {
      this.openSnackBar('Error...Debe seleccionar impresora', '');
      // console.log('error');
      return;
    }

    this.wsPalletizaje.ZPLNuevo( folio )
      .subscribe((dato: any) => {

        // console.log(dato['palletventanaResult']['lista']);

        // Resultado
        let _calibre = dato['palletventanaResult']['lista'][0].calibre;
        // console.log('CALIBRE: ' + _calibre);
        const _desesp = dato['palletventanaResult']['lista'][0].desesp;
        // console.log('ESPECIE: ' + _desesp);
        const _Variedad = dato['palletventanaResult']['lista'][0].Variedad;
        // console.log('VARIEDAD: ' + _Variedad);
        const _descat = dato['palletventanaResult']['lista'][0].descat;
        // console.log('CATEGORIA: ' + _descat);
        let _Tcajas = dato['palletventanaResult']['lista'][0].Tcajas;
        // console.log('CAJAS: ' + _Tcajas);
        const _Folio = dato['palletventanaResult']['lista'][0].Folio;
        // console.log('FOLIO: ' + _Folio);


        let num_caja = 0;
        let counter = 0;
        let vcod_cal = '';
        // Cambios pedidos por Don Marcelo
        dato['palletventanaResult']['lista'].forEach(element => {
          // console.log(element);
          // console.log('cajas1: ' + element.Tcajas);
          num_caja = num_caja + element.Tcajas;
          // console.log('cajas2: ' + num_caja);
          // console.log('num-caja ' + num_caja);
          // console.log('vcod_cal ' + element.calibre);
          if (vcod_cal !== element.calibre) {
            counter += 1;
            vcod_cal = element.calibre;
          }
        });

        _Tcajas = num_caja;
        // console.log('cajas3: ' + _Tcajas);

        // console.log('COUNTER: ' + counter);
        if (counter > 1) {
          _calibre = 'MIX';
          // console.log('Calibre: MIX');
        }


        this.prepararZPL(
          _calibre,
          _desesp,
          _Variedad,
          _descat,
          _Tcajas,
          _Folio
        );

    });


  }


  // prepararZPL( itemDato: IPalletizajeTarja ) {
  prepararZPL(
    calibre: string,
    desesp: string,
    Variedad: string,
    descat: string,
    Tcajas: string,
    Folio: string
    ) {

    if (this.IP === '0') {
      this.openSnackBar('Error...Debe seleccionar impresora', '');
      // console.log('error');
      return;
    }

    this._Subscription = this.wsPalletizaje.getZPL(COD_ZPL)
    .subscribe((dato: any) => {

      this.StringZPL = dato['CodigoZPLResult']['lista'][0].des_zpl;
      // console.log(this.StringZPL);

      // console.log(itemDato);

      this.ZPL = [{
        calibrerotu: calibre,
        especie: desesp,
        variedadrotu: Variedad,
        catrotu: descat,
        numcajaspallet: Tcajas,
        idpallet: Folio
      }];
      // this.ZPL = [{
      //   calibrerotu: itemDato.calibre,
      //   especie: itemDato.desesp,
      //   variedadrotu: itemDato.Variedad,
      //   catrotu: itemDato.descat,
      //   numcajaspallet: itemDato.Tcajas.toString(),
      //   idpallet: itemDato.Folio
      // }];

      // console.log(this.ZPL);

      this._Subscription = this.wsPalletizaje.sendZPL({...this.ZPL}, this.StringZPL, this.IP).subscribe((data: any) => {
        this.openSnackBar('Impresión correcta ---- FOLIO: ' + this.numFolio, '');
      }, (error) => {

      });

    }, (error) => {

    });

  }


  cargarImpresoras() {
    this._Subscription = this.wsImpresora.getListaImpresoras().subscribe((data: any) => {
      this.ListaImpresoras = data['ListarImpresorasResult']['lista'];
      // console.log('impresoras: ' + this.ListaImpresoras);
    }, (error) => {

    });
  }





  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }


}
