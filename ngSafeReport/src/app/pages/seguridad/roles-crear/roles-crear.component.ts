import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSnackBar } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription } from 'rxjs';

// Servicios
import { RolesService } from '../../../services/roles.service';

// Interfaz
import { IListadoFuncionalidad } from '../../../interfaces/IListadoFuncionalidad';

@Component({
  selector: 'app-roles-crear',
  templateUrl: './roles-crear.component.html',
  styleUrls: ['./roles-crear.component.css']
})
export class RolesCrearComponent implements OnInit, OnDestroy {

  displayedColumns: string[] = ['ch1', 'dependencia', 'funcionalidad', 'ch2', 'ch3', 'ch4'];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  private _Subscription: Subscription;

  // DataSources
  DataSourceListaMenu: any[] = [];
  ListFuncionalidad: IListadoFuncionalidad[] = [];
  DataSourceListaFuncionalidadMenu = new MatTableDataSource<IListadoFuncionalidad>();

  // Flags
  cargando = true;
  textoMensaje: string;
  error = false;

  // Bits
  bitGuardar: boolean;
  bitEditar: boolean;
  bitOK: boolean;

  // Variables
  _TipoMenu: string;

  constructor( public snackBar: MatSnackBar, private wsRoles: RolesService ) { }

  ngOnInit() {
    this.DataSourceListaFuncionalidadMenu.paginator = this.paginator;
    this.paginator.pageSize = 50;
    this.cargarMenu();
  }
  ngOnDestroy() {
    this._Subscription.unsubscribe();
  }

  guardar() {
    this.snackBar.open(' ✔ \xa0\xa0 Datos guardados correctamente', null, { duration: 2000 });
  }

  cargarMenu() {
    this._Subscription = this.wsRoles.getListadoMenu()
    .subscribe((data: any) => {
      if ( data['ListadoMenuResult']['lista'].length === 0 ) {
        this.textoMensaje = 'Sin datos para mostrar';
        this.error = true;
      } else {
        // Correcto
        this.DataSourceListaMenu = data['ListadoMenuResult']['lista'];
      }
    }, (error) => {

    });
  }

  doSomething(value: any) {
    this.DataSourceListaFuncionalidadMenu.data = [];
    this.ListFuncionalidad = [];
    // ROL : ADMIN -> 1
    this._Subscription = this.wsRoles.getListadoFuncionalidadMenu('1', value)
      .subscribe((data: any) => {
        console.log(JSON.stringify(data));
        if (data['ListadoFuncionalidadMenuRolResult']['lista'] === null) {
          this.textoMensaje = 'Sin datos para mostrar';
          this.error = true;
          return;
        }
        if ( data['ListadoFuncionalidadMenuRolResult']['lista'].length === 0 ) {
          this.textoMensaje = 'Sin datos para mostrar';
          this.error = true;
        } else {
          // Correcto
          data['ListadoFuncionalidadMenuRolResult']['lista'].forEach(element => {
            // tslint:disable-next-line:prefer-const
            let registro = {
              ch1: element.Asignado,
              dependencia: element.des_funcionalidad_funcionalidadDepende,
              funcionalidad: element.des_funcionalidad_funcionalidad,
              ch2: element.bit_Add_snapFR,
              ch3: element.bit_Edit_snapFR,
              ch4: element.bit_Del_snapFR
            };
            this.ListFuncionalidad.push(registro);
          });
          this.DataSourceListaFuncionalidadMenu.data = this.ListFuncionalidad;
        }
      }, (error) => {

      });
  }

}
