import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';

import { Subscription } from 'rxjs';
import { RolesService } from './../../../services/roles.service';

// Interfaz
import { IListadoRol } from '../../../interfaces/IListadoRol';

@Component({
  selector: 'app-roles-listado',
  templateUrl: './roles-listado.component.html',
  styleUrls: ['./roles-listado.component.css']
})
export class RolesListadoComponent implements OnInit, OnDestroy {

  displayedColumns: string[] = ['codigo', 'rol', 'icons'];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  // DataSources
  ListRol: IListadoRol[] = [];
  DataSourceLista: any[] = [];
  dataSource = new MatTableDataSource<IListadoRol>();

  private _Subscription: Subscription;

  cargando = true;
  textoMensaje: string;
  error = false;

  constructor( public dialog: MatDialog, private wsRoles: RolesService ) {}

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.paginator.pageSize = 50;
    this.cargarLista();
  }
  ngOnDestroy() {
    this._Subscription.unsubscribe();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(null, {
      width: '250px',
    });
  }

  cargarLista() {
    this.cargando = true;
    this._Subscription = this.wsRoles.getLista()
    .subscribe((dato: any) => {

      this.cargando = false;
      this.DataSourceLista = dato['ListadoRolResult']['lista'];

      if ( this.DataSourceLista.length === 0 ) {
        this.textoMensaje = 'Sin datos para mostrar';
        this.error = true;
      } else {
        // Correcto

        dato['ListadoRolResult']['lista'].forEach(element => {

          // tslint:disable-next-line:prefer-const
          let registro = {
            codigo: element.cod_rol_rol,
            rol: element.des_rol_rol,
            icons: ''
          };
          this.ListRol.push(registro);
        });

        this.dataSource.data = this.ListRol;
        this.error = false;
      }

    }, (error) => {
      // console.log(error.message);
      this.textoMensaje = 'Error con el Web Service';
      this.error = true;
      this.cargando = false;
    });

  }

}
