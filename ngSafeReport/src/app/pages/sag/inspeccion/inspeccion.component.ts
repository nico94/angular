import { Component, OnInit, OnDestroy } from '@angular/core';

import { RUTA_REPORTE } from './../../../config/configuracion';

import { Subscription } from 'rxjs';
import { WsSagService } from './../../../services/ws-sag.service';

@Component({
  selector: 'app-inspeccion',
  templateUrl: './inspeccion.component.html',
  styles: []
})
export class InspeccionComponent implements OnInit, OnDestroy {

  codReport: number;
  numFolio: string;

  private _Subscription: Subscription;

  DataSourceLista: any[] = [];

  cargando = true;
  textoMensaje: string;
  error = false;
  // Filter, Paginacion
  p = 1;
  public searchString: string;

  constructor(
    private wsSagService: WsSagService
  ) {
    this.cargarLista();
  }

  ngOnInit() {
  }
  ngOnDestroy() {
    this._Subscription.unsubscribe();
  }


  printReport1( cod: number ) {
    window.open(RUTA_REPORTE + 'ReportPalletizajeTarja?reporte=3&download=0&formato=pdf&parametro=' + cod, '_blank');
    // parametro = numInspeccion
    // this.printReport2( cod );
  }
  async printReport2( cod: number) {
    // await delay(500);
    // ReportPalletizajeTarja?reporte=3&download=0&formato=pdf&parametro=3
    window.open(RUTA_REPORTE + 'ReportPalletizajeTarja?reporte=4&download=0&formato=pdf&parametro=' + cod, '_blank');
    // parametro = numInspeccion
  }

  cargarLista() {
    this.cargando = true;
    this._Subscription = this.wsSagService.getLista()
    .subscribe((dato: any) => {
      this.cargando = false;
      if ( dato['ListidoInspeccionSagResult']['realizado'] ) {
        this.DataSourceLista = dato['ListidoInspeccionSagResult']['lista'];
        if ( this.DataSourceLista.length === 0 ) {
          this.textoMensaje = 'Sin datos para mostrar';
          this.error = true;
        } else {
          this.DataSourceLista = dato['ListidoInspeccionSagResult']['lista'];
          this.error = false;
        }
      } else {
        this.textoMensaje = 'Sin datos para mostrar';
        this.error = true;
      }

    }, (error) => {
      this.textoMensaje = 'Error con el Web Service';
      this.error = true;
      this.cargando = false;
    });

  }


}
