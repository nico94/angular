import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { RolesListadoComponent } from './seguridad/roles-listado/roles-listado.component';
import { RolesCrearComponent } from './seguridad/roles-crear/roles-crear.component';

const routes: Routes = [
    // {
    //   path: '',
    //   redirectTo: 'palletizajetarja',
    //   pathMatch: 'full'
    // },
    {
      path: 'roleslistado',
      component: RolesListadoComponent
    },
    {
      path: 'rolescrear',
      component: RolesCrearComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PagesRoutingModule { }
