import { Component, OnInit, OnDestroy } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

// Params Router
import { Subscription } from 'rxjs';
import { WsQCPlanillaService } from './../../../services/ws-qcplanilla.service';
import { EncMuestraGranModel } from './../../../model/EncMuestraGran';

// Angular Material
import { MatSnackBar } from '@angular/material';

// Configuraciones
import { COD_PLANTA, COD_TEMPORADA, RUTA_REPORTE } from './../../../config/configuracion';

@Component({
  selector: 'app-planillaqc',
  templateUrl: './planillaqc.component.html',
  styles: []
})
export class PlanillaqcComponent implements OnInit, OnDestroy {

  private _Subscription: Subscription;

  selectedId: number;
  EncMuestra: EncMuestraGranModel;

  esNuevo: string;
  spinner = false;
  spinner2 = false;
  bitGuardar: boolean;
  bitEditar: boolean;
  bitOK: boolean;

  // Validaciones
  textoEvaluacionGeneral: string;
  cargandoEvaluacionGeneral: boolean;
  textoDistribucionCalibre: string;
  cargandoDistribucionCalibre: boolean;
  textoEvaluacionFirmeza: string;
  cargandoEvaluacionfirmeza: boolean;
  textoEvaluacionColor: string;
  cargandoEvaluacionColor: boolean;
  textoSolidosSolubles: string;
  cargandoSolidosSolubles: boolean;
  textoDefectosCalidad: string;
  cargandoDefectosCalidad: boolean;
  textoDefectosCondicion: string;
  cargandoDefectosCondicion: boolean;

  // DataSources
  _DataSourceTipoProceso: any[] = [];
  _DataSourceCondicionTransporte: any[] = [];
  _DataSourceDistribucionCalibre: any[] = [];
  _DataSourceEvaluacionColor: any[] = [];
  _DataSourceEvaluacionGeneral: any[] = [];
  _DataSourceSolidosSolubles: any[] = [];
  _DataSourceDefectosCalidad: any[] = [];
  _DataSourceDefectosCondicion: any[] = [];
  _DataSourceFirmeza: any[] = [];
  _DataSourceAptitudGuarda: any[] = [];
  _DataSourceSegregacion: any[] = [];
  // .DataSources

  // Variables
  btnPrimary: string;

  _Planta: string;
  _Nlote: string;
  _FechaRecepcion: string;
  _NBandejas: number;
  _NombreProductor: string;
  _TotalKilos: number;
  _CodigoProductor: string;
  _HoraLLegada: string;
  _Variedad: string;
  _NumPlanilla: number;
  _NumMuestra: number;
  _TipoOrden: string;
  _TPulpa: number;
  _CantidadFrutos: number;
  _TipoAG: string;
  _TipoSeg: string;
  _Glosa = '';

  DistribucionCalibre_Value: number;
  EvaluacionGeneral_Value: number;
  EvaluacionColor_Value: number;
  SolidosSolubles_Value: number;
  DefectosCalidad_Value: number;
  DefectosCondicion_Value: number;
  Firmeza_Value: number;

  FirmezaProm: number;
  Total1: number;
  Total2: number;
  TotalPorc1: number;
  TotalPorc2: number;

  PorcDistribucionCalibre = 0;
  PorcDistribucionCalibre_Valid: boolean;
  PorcEvaluacionColor = 0;
  PorcEvaluacionColor_Valid: boolean;
  // .Variables


  constructor( private _router: Router,
               private route: ActivatedRoute,
               private _wsQCPlanilla: WsQCPlanillaService,
               public snackBar: MatSnackBar ) {

  this.Total1 = 0;
  this.Total2 = 0;
  this.TotalPorc1 = 0;
  this.TotalPorc2 = 0;
  }

  ngOnDestroy() {
    this._Subscription.unsubscribe();
  }

  ngOnInit() {

    this.FirmezaProm = 0;

    this._Subscription = this.route
      .queryParams
      .subscribe(params => {

        this._FechaRecepcion = params['FechaRecepcionString'];
        this._HoraLLegada = params['HoraIngresoString'];
        this._Nlote = params['N_lote'];
        this._CodigoProductor = params['codigo_productor'];
        this._TotalKilos = params['kilos'];
        this._NombreProductor = params['nombre_productor'];
        this._NBandejas = params['numero_bandeja'];
        this._Planta = params['planta'];
        this._Variedad = params['variedad'];
        this._NumPlanilla = params['Num_Planilla'];
        this._NumMuestra = params['Num_Muestra'];

        this.esNuevo = params['nuevo'];

        if ( this.esNuevo === 'true') {
          this.bitGuardar = true;
          this.bitEditar = false;

          this.btnPrimary = 'GUARDAR';

          // Metodo que carga al crear
          this.metodoNuevo();
        } else {
          this.bitEditar = true;
          this.bitGuardar = false;
          this.bitOK = true;

          // Metodo que carga al editar
          this.metodoEditar();
        }

      }, (error) => {
      });

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }



  guardar() {
    if (this._TipoOrden == null || this._TPulpa == null || this._CantidadFrutos == null) {
      this.openSnackBar('Error.. faltan datos por completar', '');
      return;
    }

    if ( this.esNuevo === 'true' ) {
      // Nuevo registro, llamar metodo de sergio para crear
      this.spinner = true;
      this.btnPrimary = 'Cargando...';

      this.EncMuestra = {
        num_lote: Number(this._Nlote),
        cod_planta: COD_PLANTA,
        cod_temp: COD_TEMPORADA,
        num_planilla: this._NumPlanilla,
        num_muestra: 0,
        fecha: '/Date(1483142400000)/',
        can_muestra: this._CantidadFrutos,
        glosa: '',
        cod_per: '1',
        cod_aptitudguarda: Number(this._TipoAG),
        Id_Segregacion: Number(this._TipoSeg),
        Total_Defecto: 0,
        PorExport: 0,
        tem_pulpa: this._TPulpa,
        COD_TPROC: this._TipoOrden
      };


      this._Subscription = this._wsQCPlanilla.nuevoRecepcionCerezas(this.EncMuestra)
        .subscribe((dato: any) => {
          // console.log(JSON.stringify(dato['GuardarEncMuestraGranResult'].NumMuestra));
          // Obtener Numero de Muestra creado
          this._NumMuestra = dato['GuardarEncMuestraGranResult'].NumMuestra;

          this.bitGuardar = false;
          this.bitEditar = true;
          this.bitOK = true;
          this.spinner = false;

          this.btnPrimary = 'GUARDAR';
          this.metodoNuevo();
        }, (error) => {

          this.bitGuardar = true;
          this.bitEditar = false;
          this.bitOK = false;
          this.spinner = false;

          this.btnPrimary = 'GUARDAR';
      });
    }

  }

  editar() {

    // Validaciones
    if ( !this.PorcDistribucionCalibre_Valid ) {
      this.openSnackBar('Error... Porcentaje calibre debe sumar 100%', '');
      return;
    }
    if ( !this.PorcEvaluacionColor_Valid ) {
      this.openSnackBar('Error... Porcentaje evaluacion color debe sumar 100%', '');
      return;
    }

    this.spinner2 = true;
    // Finalizar proceso
    const defecto = (this.TotalPorc1 + this.TotalPorc2);
    const exportacion = 100 - (this.TotalPorc1 + this.TotalPorc2);

    this._Subscription =
      this._wsQCPlanilla.actualizarEnc_MuestraGran(
        this._NumMuestra,
        this._Glosa,
        defecto,
        exportacion,
        Number(this._TipoAG),
        Number(this._TipoSeg))
      .subscribe( (dato: any) => {
        this.spinner2 = false;
        if (dato['ActualizarEncabezadoMuestraResult']['realizado']) {
          // Ok
          this.openSnackBar('Finalizado Correctamente', '');
          this.cargarEnc_MuestraGran();
        } else {
          // No realizado
          this.openSnackBar('Error respuesta del Web Service', '');
        }
      }, (error) => {
        // Sin respuesta WS
        this.spinner2 = false;
        this.openSnackBar('Error con el Web Service', '');
    });
  }

  // *********
  // Funciones
  // *********
  changeCondicionTransporte( value: any[] ) {
    let flag: boolean;
    flag = Boolean( value['ok']);
    this._Subscription = this._wsQCPlanilla.actualizarCondicionTransporte( value, !flag)
      .subscribe(( data: any) => {
        this.cargarCondicionTransporte(this._NumMuestra.toString());
      }, (error) => {
    });
  }

  // ** DISTRIBUCION CALIBRE
  setDistribucionCalibre( value) {
    this.DistribucionCalibre_Value = value;
  }
  focusOutDistribucionCalibre( item: any[] ) {
    const porc = (this.DistribucionCalibre_Value / this._CantidadFrutos) * 100;
    // console.log('CANT '+this._CantidadFrutos, 'DIS '+this.DistribucionCalibre_Value);

    this._Subscription =
    this._wsQCPlanilla.actualizarDistribucionCalibre( item, this.DistribucionCalibre_Value, porc)
      .subscribe(( data: any ) => {
        this.cargarDistribucionCalibre(this._NumMuestra);
      }, (error) => {
    });
  }
  // ** .DISTRIBUCION CALIBRE

  // ** EVALUACION GENERAL
  setEvaluacionGeneral( value) {
    this.EvaluacionGeneral_Value = value;
  }
  focusOutEvaluacionGeneral( item: any[] ) {

    const porc = (this.EvaluacionGeneral_Value / this._CantidadFrutos) * 100;

    this._Subscription =
    this._wsQCPlanilla.actualizarEvaluacionGeneral( item, this.EvaluacionGeneral_Value, porc)
      .subscribe(( data: any ) => {
        this.cargarEvaluacionGeneral(this._NumMuestra);
      }, (error) => {
    });

  }
  // ** .EVALUACION GENERAL

  // ** EVALUACION COLOR
  setEvaluacionColor( value) {
    this.EvaluacionColor_Value = value;
  }
  focusOutEvaluacionColor( item: any[] ) {

    const porc = (this.EvaluacionColor_Value / this._CantidadFrutos) * 100;

    this._Subscription =
    this._wsQCPlanilla.actualizarEvaluacionColor( item, this.EvaluacionColor_Value, porc)
      .subscribe(( data: any ) => {
        this.cargarEvaluacionColor(this._NumMuestra);
      }, (error) => {

    });
  }
  // ** .EVALUACION COLOR

  // ** EVALUACION SOLIDOS SOLUBLES
  setSolidosSolubles( value) {
    this.SolidosSolubles_Value = value;
  }
  focusOutSolidosSolubles( item: any[], FEL: number ) {
    this._Subscription =
    this._wsQCPlanilla.actualizarMuestraSolidosSolubles( item, this.SolidosSolubles_Value, FEL)
      .subscribe(( data: any ) => {
        this.cargarSolidosSolubles(this._NumMuestra);
      }, (error) => {

    });
  }
  // ** .EVALUACION SOLIDOS SOLUBLES

  // ** EVALUACION DEFECTOS CALIDAD
  setDefectosCalidad( value) {
    this.DefectosCalidad_Value = value;
  }
  focusOutDefectosCalidad( item: any[] ) {

    const porc = (this.DefectosCalidad_Value / this._CantidadFrutos) * 100;

    this._Subscription =
    this._wsQCPlanilla.actualizarDefectosCalidad( item, this.DefectosCalidad_Value, porc)
      .subscribe(( data: any ) => {
        this.cargarDefectosCalidad(this._NumMuestra);
      }, (error) => {
    });
  }
  // ** .EVALUACION DEFECTOS CALIDAD

  // ** EVALUACION DEFECTOS CONDICION
  setDefectosCondicion( value) {
    this.DefectosCondicion_Value = value;
  }
  focusOutDefectosCondicion( item: any[] ) {

    const porc = (this.DefectosCondicion_Value / this._CantidadFrutos) * 100;

    this._Subscription =
    this._wsQCPlanilla.actualizarDefectosCondicion( item, this.DefectosCondicion_Value, porc)
      .subscribe(( data: any ) => {
        this.cargarDefectosCondicion(this._NumMuestra);
      }, (error) => {
    });
  }
  // ** .EVALUACION DEFECTOS CONDICION

  // ** EVALUACION FIRMEZA
  setFirmeza( value) {
    this.Firmeza_Value = value;
  }
  focusOutFirmeza( item: any[] ) {
    this._Subscription =
    this._wsQCPlanilla.actualizarFirmeza( item, this.Firmeza_Value)
      .subscribe(( data: any ) => {
        this.cargarFirmeza(this._NumMuestra);
      }, (error) => {
    });
  }
  // ** .EVALUACION FIRMEZA

  reset( value: number, tipo: number ) {
    switch (tipo) {
      case 1:
        this.EvaluacionGeneral_Value = value;
        break;
      case 2:
        this.DistribucionCalibre_Value = value;
        break;
      case 3:
        this.Firmeza_Value = value;
        break;
      case 4:
        this.EvaluacionColor_Value = value;
        break;
      case 5:
        this.SolidosSolubles_Value = value;
        break;
      case 6:
        this.DefectosCalidad_Value = value;
        break;
      case 7:
        this.DefectosCondicion_Value = value;
        break;
      default:
        break;
    }
  }
  // *********
  // .Funciones
  // *********



  metodoNuevo() {
    this.cargarComboTipoProceso('');
    this.cargarComboAptitudGuarda('-1');
    this.cargarComboSegregacion('-1');
    this.cargarCondicionTransporte(this._NumMuestra.toString());
    this.cargarDistribucionCalibre(this._NumMuestra);
    this.cargarEvaluacionColor(this._NumMuestra);
    this.cargarEvaluacionGeneral(this._NumMuestra);
    this.cargarSolidosSolubles(this._NumMuestra);
    this.cargarDefectosCalidad(this._NumMuestra);
    this.cargarDefectosCondicion(this._NumMuestra);
    this.cargarFirmeza(this._NumMuestra);
  }
  metodoEditar() {
    this.cargarEnc_MuestraGran();
    // this.cargarComboTipoProceso('');
    this.cargarCondicionTransporte(this._NumMuestra.toString());
    this.cargarDistribucionCalibre(this._NumMuestra);
    this.cargarEvaluacionColor(this._NumMuestra);
    this.cargarEvaluacionGeneral(this._NumMuestra);
    this.cargarSolidosSolubles(this._NumMuestra);
    this.cargarDefectosCalidad(this._NumMuestra);
    this.cargarDefectosCondicion(this._NumMuestra);
    this.cargarFirmeza(this._NumMuestra);
  }



  // ***********
  // Cargar DataSources
  // ***********
  cargarEnc_MuestraGran() {
    this._Subscription = this._wsQCPlanilla.listadoEnc_MuestraGran(this._NumMuestra)
      .subscribe(( dato: any ) => {
        this.cargarComboTipoProceso(dato['DatosEncabezadoMuestraResult'].Cod_TProc);
        this.cargarComboAptitudGuarda('-1');
        this.cargarComboSegregacion('-1');
        this._TipoOrden = dato['DatosEncabezadoMuestraResult'].Cod_TProc;
        this._TipoAG = dato['DatosEncabezadoMuestraResult'].cod_aptitudguarda;
        this._TipoSeg = dato['DatosEncabezadoMuestraResult'].Id_Segregacion;
        this._TPulpa = dato['DatosEncabezadoMuestraResult'].tem_pulpa;
        this._CantidadFrutos = dato['DatosEncabezadoMuestraResult'].can_muestra;
        this._Glosa = dato['DatosEncabezadoMuestraResult'].Glosa;
      }, (error) => {
    });
  }


  cargarComboTipoProceso( codTP: string ) {
    this._Subscription = this._wsQCPlanilla.listadoTipoProceso(codTP)
    .subscribe((dato: any) => {
      this._DataSourceTipoProceso = dato['ListadoTipoProcesoResult']['lista'];
    }, (error) => {
    });
  }
  cargarComboAptitudGuarda( codAG: string ) {
    this._Subscription = this._wsQCPlanilla.listadoAptitudGuarda(codAG)
    .subscribe((dato: any) => {
      this._DataSourceAptitudGuarda = dato['ListadoAptitudGuardaResult']['lista'];
    }, (error) => {
    });
  }
  cargarComboSegregacion( codS: string ) {
    this._Subscription = this._wsQCPlanilla.listadoSegregacion(codS)
    .subscribe((dato: any) => {
      this._DataSourceSegregacion = dato['ListadosegregacionResult']['lista'];
    }, (error) => {
    });
  }
  cargarCondicionTransporte( numMuestra: string ) {
    this._Subscription = this._wsQCPlanilla.listadoCondicionTransporte( numMuestra )
      .subscribe((dato: any) => {
        this._DataSourceCondicionTransporte = dato['ListadoCondicionTransporteResult']['lista'];
      }, (error) => {
    });
  }
  cargarDistribucionCalibre( numMuestra: number ) {
    this.cargandoDistribucionCalibre = true;
    this._Subscription = this._wsQCPlanilla.ListadoDistribucionCalibre( numMuestra )
      .subscribe((dato: any) => {
        this.cargandoDistribucionCalibre = false;
        if (dato['ListadoDistribucionCalibreResult']['realizado']) {
          this._DataSourceDistribucionCalibre = dato['ListadoDistribucionCalibreResult']['lista'];
          if (this._DataSourceDistribucionCalibre.length === 0) {
            this.textoDistribucionCalibre = 'Sin datos para mostrar';
          } else {
            // Foreach
            let sumPorc = 0;
            dato['ListadoDistribucionCalibreResult']['lista'].forEach(function(item) {
              sumPorc = sumPorc + item.porc;
            });
            this.PorcDistribucionCalibre = sumPorc;
            if ( this.PorcDistribucionCalibre < 100 ) {
              this.PorcDistribucionCalibre_Valid = false;
            } else if ( this.PorcDistribucionCalibre > 100 ) {
              this.PorcDistribucionCalibre_Valid = false;
            } else {
              this.PorcDistribucionCalibre_Valid = true;
            }
          }
        } else {
          this.textoDistribucionCalibre = 'Error respuesta del Web Service';
        }
      }, (error) => {
        this.cargandoDistribucionCalibre = false;
        this.textoDistribucionCalibre = 'Error con el Web Service';
    });
  }
  cargarEvaluacionColor( numMuestra: number ) {
    this.cargandoEvaluacionColor = true;
    this._Subscription = this._wsQCPlanilla.ListadoEvaluacionColor( numMuestra )
      .subscribe((dato: any) => {
        this.cargandoEvaluacionColor = false;
        if (dato['ListadoEvaluacionColorResult']['realizado']) {
          this._DataSourceEvaluacionColor = dato['ListadoEvaluacionColorResult']['lista'];
          if (this._DataSourceEvaluacionColor.length === 0) {
            this.textoEvaluacionColor = 'Sin datos para mostrar';
          } else {
            // Foreach
            let sumPorc = 0;
            dato['ListadoEvaluacionColorResult']['lista'].forEach(function(item) {
              sumPorc = sumPorc + item.porc;
            });
            this.PorcEvaluacionColor = sumPorc;
            if ( this.PorcEvaluacionColor < 100 ) {
              this.PorcEvaluacionColor_Valid = false;
            } else if ( this.PorcEvaluacionColor > 100 ) {
              this.PorcEvaluacionColor_Valid = false;
            } else {
              this.PorcEvaluacionColor_Valid = true;
            }
          }
        } else {
          this.textoEvaluacionColor = 'Error respuesta del Web Service';
        }
      }, (error) => {
        this.cargandoEvaluacionColor = false;
        this.textoEvaluacionColor = 'Error con el Web Service';
    });
  }
  cargarEvaluacionGeneral( numMuestra: number ) {
    this.cargandoEvaluacionGeneral = true;
    this._Subscription = this._wsQCPlanilla.ListadoEvaluacionGeneral( numMuestra )
      .subscribe((dato: any) => {
        this.cargandoEvaluacionGeneral = false;
        if (dato['ListadoEvaluacionGeneralResult']['realizado']) {
          this._DataSourceEvaluacionGeneral = dato['ListadoEvaluacionGeneralResult']['lista'];
          if (this._DataSourceEvaluacionGeneral.length === 0) {
            this.textoEvaluacionGeneral = 'Sin datos para mostrar';
          }
        } else {
          this.textoEvaluacionGeneral = 'Error respuesta del Web Service';
        }
      }, (error) => {
        this.cargandoEvaluacionGeneral = false;
        this.textoEvaluacionGeneral = 'Error con el Web Service';
    });
  }
  cargarSolidosSolubles( numMuestra: number ) {
    this.cargandoSolidosSolubles = true;
    this._Subscription = this._wsQCPlanilla.ListadoMuestraSolidosSolubles( numMuestra )
      .subscribe((dato: any) => {
        this.cargandoSolidosSolubles = false;
        if (dato['ListadoMuestraSolidosSolublesResult']['realizado']) {
          this._DataSourceSolidosSolubles = dato['ListadoMuestraSolidosSolublesResult']['lista'];
          if (this._DataSourceSolidosSolubles.length === 0) {
            this.textoSolidosSolubles = 'Sin datos para mostrar';
          }
        } else {
          this.textoSolidosSolubles = 'Error respuesta del Web Service';
        }
      }, (error) => {
        this.cargandoSolidosSolubles = false;
        this.textoSolidosSolubles = 'Error con el Web Service';
    });
  }
  cargarDefectosCalidad( numMuestra: number ) {
    this.cargandoDefectosCalidad = true;
    this._Subscription = this._wsQCPlanilla.ListadoDefectosCalidad( numMuestra, '' )
      .subscribe((dato: any) => {
        this.cargandoDefectosCalidad = false;
        if (dato['ListadoMuestraDefectosResult']['realizado']) {
          this._DataSourceDefectosCalidad = dato['ListadoMuestraDefectosResult']['lista'];
          if (this._DataSourceDefectosCalidad.length === 0) {
            this.textoDefectosCalidad = 'Sin datos para mostrar';
          } else {
            // Calculos
            let suma = 0;
            let porc = 0;
            dato['ListadoMuestraDefectosResult']['lista'].forEach(function(item) {
              suma = suma + item.cantidad;
              porc = porc + item.porc;
            });
            this.Total1 = suma;
            this.TotalPorc1 = porc;
          }
        } else {
          this.textoDefectosCalidad = 'Error respuesta del Web Service';
        }
      }, (error) => {
        this.cargandoDefectosCalidad = false;
        this.textoDefectosCalidad = 'Error con el Web Service';
    });
  }
  cargarDefectosCondicion( numMuestra: number ) {
    this.cargandoDefectosCondicion = true;
    this._Subscription = this._wsQCPlanilla.ListadoDefectosCondicion( numMuestra, '' )
      .subscribe((dato: any) => {
        this.cargandoDefectosCondicion = false;
        if (dato['ListadoMuestraDefectosResult']['realizado']) {
          this._DataSourceDefectosCondicion = dato['ListadoMuestraDefectosResult']['lista'];
          if (this._DataSourceDefectosCondicion.length === 0) {
            this.textoDefectosCondicion = 'Sin datos para mostrar';
          } else {
            // Calculos
            let suma = 0;
            let porc = 0;
            dato['ListadoMuestraDefectosResult']['lista'].forEach(function(item) {
              suma = suma + item.cantidad;
              porc = porc + item.porc;
            });
            this.Total2 = suma;
            this.TotalPorc2 = porc;
          }
        } else {
          this.textoDefectosCondicion = 'Error respuesta del Web Service';
        }
      }, (error) => {
        this.cargandoDefectosCondicion = false;
        this.textoDefectosCondicion = 'Error con el Web Service';
    });
  }
  cargarFirmeza( numMuestra: number ) {
    this.cargandoEvaluacionfirmeza = true;
    this._Subscription = this._wsQCPlanilla.ListadoFirmeza( numMuestra )
      .subscribe((dato: any) => {
        this.cargandoEvaluacionfirmeza = false;
        if (dato['ListadoFirmezaResult']['realizado']) {
          this._DataSourceFirmeza = dato['ListadoFirmezaResult']['lista'];
          if (this._DataSourceFirmeza.length === 0) {
            this.textoEvaluacionFirmeza = 'Sin datos para mostrar';
          } else {
            // Calculos
            let suma = 0;
            let contador = 0;
            if ( this._DataSourceFirmeza.length > 0 ) {
              dato['ListadoFirmezaResult']['lista'].forEach(function(item, index) {
                if ( item.cantidad > 0) {
                  contador++;
                  suma = suma + item.cantidad;
                }
              });
              this.FirmezaProm = suma / contador;
            }
          }
        } else {
          this.textoEvaluacionFirmeza = 'Error respuesta del Web Service';
        }
      }, (error) => {
        this.cargandoEvaluacionfirmeza = false;
        this.textoEvaluacionFirmeza = 'Error con el Web Service';
    });
  }
  // ***********
  // .Cargar DataSources
  // ***********



  // **********************
  // Print report
  // **********************
  print() {
    window.open(RUTA_REPORTE +
      'ReportPalletizajeTarja?reporte=6' +
      '&download=0' +
      '&formato=pdf' +
      '&muestreo=' + this._NumMuestra +
      '&planta=' + COD_PLANTA +
      '&temporada=' + COD_TEMPORADA,
      '_blank');
  }
  // **********************
  // .Print report
  // **********************

}
