import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Router } from '@angular/router';

// Servicios
import { WsQCPlanillaService } from './../../../services/ws-qcplanilla.service';

@Component({
  selector: 'app-listadoqc',
  templateUrl: './listadoqc.component.html',
  styles: []
})
export class ListadoqcComponent implements OnInit, OnDestroy {

  lista: any[] = [];
  private _Subscription: Subscription;
  DataSource: any[] = [];

  cargando = true;
  textoMensaje: string;
  error = false;
  pendientes = false;
  tipoListado = 'Pendientes';

  // Filter, Paginacion
  p = 1;
  public searchString: string;

  constructor( private _router: Router,
               private wsQCPlanilla: WsQCPlanillaService ) { }

  onChange(value) {
    this.DataSource = [];
    this.pendientes = value.checked;
    this.pendientes ? this.tipoListado = 'Realizadas' : this.tipoListado = 'Pendientes';
    // console.log(this.pendientes);
    this.cargarLista();
  }

  ngOnInit() {
    this.cargarLista();
  }
  ngOnDestroy() {
    this._Subscription.unsubscribe();
  }

  cargarLista() {
    this.cargando = true;
    this._Subscription = this.wsQCPlanilla.getListaQC( this.pendientes )
    .subscribe((data: any) => {
      this.cargando = false;

      if ( data['ListadoQcResult']['realizado'] ) {
        // console.log(data['ListadoQcResult']['lista']);
        this.DataSource = data['ListadoQcResult']['lista'];
        // console.log('this.DataSource.length ' + this.DataSource.length);
        if ( this.DataSource.length === 0 ) {
          this.textoMensaje = 'Sin datos para mostrar';
          this.error = true;
        } else {
          this.DataSource = data['ListadoQcResult']['lista'];
          this.error = false;
        }
      } else {
        this.textoMensaje = 'Sin datos para mostrar';
        this.error = true;
      }

      // console.log(data['ListadoQcResult']['lista']);
      // console.log(data['ListadoQcResult']['realizado']);
    }, (error) => {
      // console.error('Error WS');
      this.textoMensaje = 'Error con el Web Service';
      this.error = true;
      this.cargando = false;
    });
  }

  openPlanilla( data: any) {
    // console.log('Listado: ' + JSON.stringify(data));

    this._router.navigate(['/qcrecepcion/plantillaqc'], {
      queryParams: {
        FechaRecepcionString: data['FechaRecepcionString'],
        HoraIngresoString: data['HoraIngresoString'],
        N_lote: data['N_lote'],
        codigo_productor: data['codigo_productor'],
        kilos: data['kilos'],
        nombre_productor: data['nombre_productor'],
        numero_bandeja: data['numero_bandeja'],
        planta: data['planta'],
        variedad: data['variedad'],
        Num_Planilla: data['Num_Planilla'],
        Num_Muestra: data['Num_Muestra'],
        nuevo: !this.pendientes
      }}
    );
  }

}
