import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';


// ANGULAR MATERIAL
import { FormsModule } from '@angular/forms';
import {
  MatToolbarModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatTableModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatIconModule,
  MatOptionModule,
  MatSelectModule,
  MatRadioModule,
  MatSlideToggleModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatMenuModule,
  MatSnackBarModule,
  MatCardModule
} from '@angular/material';

import { RolesListadoComponent } from './seguridad/roles-listado/roles-listado.component';
import { RolesCrearComponent } from './seguridad/roles-crear/roles-crear.component';


@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    FormsModule,
    MatIconModule,
    MatOptionModule,
    MatSelectModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatMenuModule,
    MatSnackBarModule,
    MatCardModule
  ],
  declarations: [
    RolesListadoComponent,
    RolesCrearComponent
  ]
})
export class PagesModule { }
