import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { WsNegocioService } from './../../../services/ws-negocio.service';

import { RUTA_REPORTE } from './../../../config/configuracion';

@Component({
  selector: 'app-listadonegocio',
  templateUrl: './listadonegocio.component.html',
  styles: []
})
export class ListadonegocioComponent implements OnInit {

  DataSourceLista: any[] = [];
  _Subscription: Subscription;

  cargando = true;
  textoMensaje: string;
  error = false;
  // Filter, Paginacion
  p = 1;
  public searchString: string;

  constructor( private wsNegocio: WsNegocioService ) { }

  ngOnInit() {
    this.cargarLista();
  }


  cargarLista() {
    this.cargando = true;
    this._Subscription = this.wsNegocio.getListaNegocio(-1)
    .subscribe((dato: any) => {
      this.cargando = false;
      if ( dato['ListadoNegocioResult']['realizado'] ) {
        this.DataSourceLista = dato['ListadoNegocioResult']['lista'];
        if ( this.DataSourceLista.length === 0 ) {
          this.textoMensaje = 'Sin datos para mostrar';
          this.error = true;
        } else {
          this.DataSourceLista = dato['ListadoNegocioResult']['lista'];
          this.error = false;
        }
      } else {
        this.textoMensaje = 'Error con el Web Service';
        this.error = true;
      }
    }, (error) => {
      this.textoMensaje = 'Error comunicacion con el Web Service';
      this.error = true;
      this.cargando = false;
    });
  }


  // Reportes
  showReportPackingList() {
    window.open(RUTA_REPORTE +
      'ReportPalletizajeTarja?reporte=0&download=0&formato=pdf&parametro=0',
    '_blank');
  }
  showReportDespacho() {
    window.open(RUTA_REPORTE +
      'ReportPalletizajeTarja?reporte=0&download=0&formato=pdf&parametro=0',
    '_blank');
  }
  showReportInvoice() {
    window.open(RUTA_REPORTE +
      'ReportPalletizajeTarja?reporte=0&download=0&formato=pdf&parametro=0',
    '_blank');
  }
  showReportGuias() {
    window.open(RUTA_REPORTE +
      'ReportPalletizajeTarja?reporte=0&download=0&formato=pdf&parametro=0',
    '_blank');
  }

}
