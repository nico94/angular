import { Component, OnInit } from '@angular/core';

//Firebase
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { MatSnackBar } from '@angular/material';

export interface IUsuario {
  id: string;
  nombre: string;
}
export interface IFecha {
  id: number;
  hora: string;
}
export interface IHora {
  id: number;
  latitud: number;
  longitud: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  titulo = 'People Tracking';
  lat: number = -34.9853329;
  lng: number = -71.2394072;
  zoom: number = 15;
  estiloMapa = [{
    elementType: "labels.text",
    stylers: [{
      visibility: "off"
    }]
  }];

  fechaUbi2:any[] = [];

  usuarioSeleccionado: string;

  labelOptions = {
    color: '#CC0000',
    fontFamily: '',
    fontSize: '15px',
    fontWeight: 'bold',
  };
  ubicaciones: any[];
  fechaUbi: any = [] = [];
  usuarios: any[];
  fechas: any[];
  arregloCoords: any[] = [];
  mostrarLineas:boolean = false;

  public polyline: Array<any>;
  public polylines: Array<any>;

  constructor(
    private db: AngularFireDatabase,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.obtenerUbicaciones();
  }

  obtenerUbicaciones(): void {
    this.db.list<any>('/').valueChanges()
      .subscribe(data => {
        if (data.length > 0) {
          this.arregloCoords = [];
          this.usuarios = data;
          this.usuarios.forEach(usuario => {
            let foto = usuario['photoUrl'];
            this.fechaUbi.push(usuario['ubicaciones']);
            let cor = { 'coor': usuario['actual'], 'foto': foto };
            this.arregloCoords.push(cor);
          });
        } else {
          console.log('Sin datos');
          this.snackBar.open('No hay datos registrados', null, { duration: 2000 });
        }
      });
  }

  seleccionaUsuario(usu: any) {
    this.arregloCoords = [];
    this.fechaUbi = [];
    this.fechas = [];
    // let ub: any[] = [];
    let ubicaciones: any;
    // console.log('Usuario : '+JSON.stringify(usu));

    ubicaciones = usu['ubicaciones'];
    if(ubicaciones){
      this.mostrarLineas = true;
      for (var i in ubicaciones) {
        this.fechaUbi.push(ubicaciones[i]);
        this.fechas.push(ubicaciones[i]);
        // ub.push(ubicaciones[i]);
      }
      console.log(JSON.stringify(this.fechaUbi2));
    }else{
      this.mostrarLineas = false;
      this.snackBar.open('Sin ubicaciones asociadas al usuario', null, { duration: 2000 });
    }
  }

  seleccionaFecha(fecha: any) {
    this.arregloCoords = [];
    let cor = {};
    for (var i in fecha) {
      // console.log(fecha[i]);
      cor = { 'coor': fecha[i], 'foto': '' };
      this.arregloCoords.push(cor);
    }
  }
}