// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAejXPfaJ3TmdNrj4sIrFwYMmBrtgcK6g4',
    authDomain: 'peopletrack-e33e6.firebaseapp.com',
    databaseURL: 'https://peopletrack-e33e6.firebaseio.com',
    projectId: 'peopletrack-e33e6',
    storageBucket: 'peopletrack-e33e6.appspot.com',
    messagingSenderId: '400863412971'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
